import { __decorate } from 'tslib';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { ɵɵdefineInjectable, ɵɵinject, Injectable, EventEmitter, ChangeDetectorRef, Input, ViewChild, Output, Component, forwardRef, NgModule } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import { OrderPipe, OrderModule } from 'ngx-order-pipe';
import { TimeagoDefaultFormatter, TimeagoModule } from 'ngx-timeago';
import { QuillEditorComponent, QuillModule } from 'ngx-quill';
import { finalize } from 'rxjs/operators';
import 'quill-mention';
import { CommonModule } from '@angular/common';

class Note {
    constructor() {
        this.updateMode = false;
        this.confirmDelete = false;
    }
}

let CommentsService = class CommentsService {
    constructor(handler) {
        this.http = new HttpClient(handler);
    }
    getNotes({ config, noteType, entityId, clientId, status }) {
        let headers = this.getHttpOptions();
        if (status == null)
            return this.http.get(config.urlPrefix + 'note/get-note?noteType=' + noteType + '&entityId=' + entityId + '&clientId=' + clientId, headers);
        else
            return this.http.get(config.urlPrefix + 'note/get-note?noteType=' + noteType + '&entityId=' + entityId + '&clientId=' + clientId + '&status=' + status, headers);
    }
    addComment({ config, comment, noteType, rootNote, entityId, clientId }) {
        const commentObject = new Note();
        let headers = this.getHttpOptions();
        commentObject.content = comment;
        commentObject.rootNote = rootNote;
        commentObject.entityId = entityId;
        return this.http.post(config.urlPrefix + 'note/add?noteType=' + noteType + '&clientId=' + clientId, commentObject, headers);
    }
    updateNote({ config, comment, commentUUID }) {
        let headers = this.getHttpOptions();
        const commentObject = new Note();
        commentObject.content = comment;
        commentObject.uuid = commentUUID;
        return this.http.post(config.urlPrefix + 'note/update', commentObject, headers);
    }
    deleteNote({ config, commentUUID }) {
        let headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'note/delete?uuid=' + commentUUID, headers);
    }
    getUserStats({ config, noteType, entityId }) {
        let headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'note/comment-user-stats'
            + '?noteType=' + noteType + '&entityId=' + entityId, headers);
    }
    updateLastRead({ config, noteTypeId, entityId, commentUUID }) {
        let headers = this.getHttpOptions();
        return this.http.post(config.urlPrefix + 'note/last-read?noteUuid=' + commentUUID + '&noteTypeId=' + noteTypeId + '&entityId=' + entityId, {}, headers);
    }
    // createCommentSection({config}: CommentParam): Observable<AccessControl<CommentSection>> {
    //   return this.access.httpPost<CommentSection>(config.urlPrefix + 'section/create');
    // }
    resolveComment({ config, noteTypeId, entityId, noteUUID }) {
        let headers = this.getHttpOptions();
        return this.http.post(config.urlPrefix + 'note/resolve?noteTypeId=' + noteTypeId + '&entityId=' + entityId + '&noteUUID=' + noteUUID, {}, headers);
    }
    getNoteType({ config, noteType, clientId }) {
        let headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'noteType/get?noteType=' + noteType + '&clientId=' + clientId, headers);
    }
    getCookie(name) {
        let ca = document.cookie.split(';');
        let caLen = ca.length;
        let cookieName = `${name}=`;
        let c;
        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    }
    getHttpOptions() {
        let authorization = this.getCookie('access_token');
        let contentType = "application/json";
        let httpHeaderOptions = {
            'Authorization': 'Bearer ' + authorization,
            'Content-Type': contentType
        };
        const httpOptions = {
            headers: new HttpHeaders(httpHeaderOptions)
        };
        return httpOptions;
    }
};
CommentsService.ctorParameters = () => [
    { type: HttpBackend }
];
CommentsService.ɵprov = ɵɵdefineInjectable({ factory: function CommentsService_Factory() { return new CommentsService(ɵɵinject(HttpBackend)); }, token: CommentsService, providedIn: "root" });
CommentsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], CommentsService);

var CommentSectionComponent_1;
let CommentSectionComponent = CommentSectionComponent_1 = class CommentSectionComponent {
    constructor(commentsService, orderService, cdr) {
        this.commentsService = commentsService;
        this.orderService = orderService;
        this.cdr = cdr;
        this.commentContent = '';
        this.frozen = false;
        this.readOnly = false;
        this.loading = true;
        this.noteTypeLoading = true;
        this.commentList = [];
        this.isLastReadUpdated = false;
        this.notes = [];
        this.commentsTree = [];
        this.mentionDenotation = [];
        this.show = false;
        this.readNote = true;
        this.loadCommentUserStats = true;
        this.commentMap = {};
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.mentionClick = new EventEmitter();
        // cbk = debounce(() => this.inview, 300);
        // cbk =  () => this.inview();
        this.showResolved = false;
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    ngOnInit() {
        // window.addEventListener('scroll',this.cbk);
        // this.getNoteType();
    }
    updateCommentContent(newVal) {
        this.writeValue(newVal);
    }
    ngAfterContentInit() {
        this.getNotes();
        this.getNoteType();
    }
    getNotes() {
        this.loading = true;
        this.commentsTree = [];
        let status = 'ACTIVE';
        if (this.showResolved) {
            status = 'RESOLVED';
        }
        this.commentsService.getNotes({ config: this.config, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId, status: status })
            .subscribe(accessObj => {
            this.notes = accessObj.data;
            const commentSection = accessObj.data;
            let sordBy = 'creationDate';
            if (status == 'RESOLVED') {
                sordBy = 'resolvedAt';
            }
            const comments = this.orderService.transform(commentSection, sordBy);
            this.commentList = comments;
            comments.forEach(comment => {
                this.commentMap[comment.uuid] = comment;
                comment.replies = [];
            });
            comments.forEach(comment => {
                if (comment.rootNote) {
                    this.commentMap[comment.rootNote.uuid].replies.push(comment);
                }
                else {
                    this.commentsTree.push(comment);
                }
            });
            this.loading = false;
            this.updateLastRead();
        }, error => {
            this.loading = false;
        });
    }
    getNoteType() {
        this.noteTypeLoading = true;
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(response => {
            var _a, _b;
            this.noteTypeData = response === null || response === void 0 ? void 0 : response.data;
            const characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            this.mentionDenotation = characters.split(',');
            this.noteTypeLoading = false;
        }, error => {
            this.noteTypeLoading = false;
        });
    }
    ngAfterViewChecked() {
        // console.log('mention', this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'));
        // Array.from(this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'))
        //   .forEach(elem => {
        //     const mentionChar = (<any>elem).getAttribute('data-denotation-char');
        //     const id = (<any>elem).getAttribute('data-id');
        //     const value = (<any>elem).getAttribute('data-value');
        //     const elemRef = elem;
        //     (<any>elem).onclick = (event) => {
        //       this.mentionClick.emit({mentionChar, id, value, elemRef, event});
        //   };
        //   console.log('listerner set on ', elem);
        // });
    }
    ngOnChange() {
    }
    setStatsOnView() {
        // this.loadCommentUserStats = true;
        // if()
        // this.commentsService.getUserStats({config: this.config, noteType : this.noteTypeData.uuid ,entityId : this.entityId})
        //   .subscribe(data => {
        //     this.commentUserStats = data.data;
        //     const commentList = this.commentList.filter(c => c.userId !== this.currentUser.id);
        //     const comment = commentList[commentList.length - this.commentUserStats.unreadCount];
        //     this.loadCommentUserStats = false;
        //     this.updateLastRead();
        //     if (comment) {
        //       comment.firstUnread = true;
        //     }
        //   });
    }
    setReplyTo(comment) {
        comment.replying = true;
        this.cdr.detectChanges();
        setTimeout(() => this.scroll('comment_quill_' + comment.uuid), 150);
    }
    resolve(comment) {
        this.commentsService.resolveComment({ config: this.config, noteTypeId: comment.noteType.id, entityId: this.entityId, noteUUID: comment.uuid }).subscribe(data => {
            this.commentsTree = this.commentsTree.filter((note) => note.uuid != comment.uuid);
        });
        this.cdr.detectChanges();
        setTimeout(() => this.scroll('comment_quill_' + comment.uuid), 150);
    }
    pushToCommentList(addedComment, comment) {
        this.commentMap[addedComment.uuid] = addedComment;
        this.commentList.push(addedComment);
        addedComment.replies = [];
        if (comment) {
            comment.replying = false;
        }
        if (addedComment.rootNote) {
            this.commentMap[comment.uuid].replies.push(addedComment);
        }
        else {
            this.commentsTree.push(addedComment);
        }
        this.cdr.detectChanges();
        this.scrollToComment(addedComment);
    }
    pushWhenEdit(editNote, comment) {
        this.commentsTree.forEach(comment => {
            if (editNote.uuid == comment.uuid) {
                comment.content = editNote.content;
            }
            else {
                comment.replies.forEach(reply => {
                    if (editNote.uuid == reply.uuid)
                        reply.content = editNote.content;
                });
            }
        });
        this.cdr.detectChanges();
    }
    deleteToCommentList(deletedNote) {
        this.commentsTree = this.commentsTree.filter(note => note.uuid != deletedNote.uuid);
        this.commentsTree.forEach(comment => {
            comment.replies = comment.replies.filter(x => x.uuid != deletedNote.uuid);
        });
    }
    cancelReply(comment) {
        comment.replying = false;
    }
    scrollToComment(comment) {
        this.scroll(this.getClassName(comment), () => {
            const repliedComment = this.commentList.filter(commentObject => comment.uuid == commentObject.uuid)[0];
            if (repliedComment) {
                this.selectComment(repliedComment);
            }
        });
    }
    scroll(className, callback) {
        const classElement = document.getElementsByClassName(className);
        if (classElement.length > 0) {
            classElement[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
            if (callback) {
                callback();
            }
        }
    }
    getClassName(comment) {
        return 'note_' + comment.uuid;
    }
    selectComment(comment) {
        comment['selected'] = true;
        setTimeout(() => comment['selected'] = false, 3000);
    }
    get mainCommentBox() {
        return this.commentBox;
    }
    setFocus(editor, box) {
        box.editor.getQuill().focus();
    }
    formatDate(date_str) {
        const date = new Date(date_str);
        return date.toLocaleString();
    }
    onSuggest(event) {
        this.suggest.emit(event);
    }
    updateLastRead() {
        if (!this.isLastReadUpdated) {
            const comments = this.commentList.filter(c => c.userId !== this.currentUser.id);
            const lastComment = comments[comments.length - 1];
            if (lastComment) {
                this.commentsService.updateLastRead({ config: this.config, noteTypeId: this.noteTypeData.id, entityId: this.entityId, commentUUID: lastComment.uuid }).subscribe(data => {
                    this.isLastReadUpdated = true;
                });
            }
        }
    }
    //   isElementInViewport (el) {
    //     const rect = el.getBoundingClientRect();
    //     return (
    //         rect.top >= 0 &&
    //         rect.left >= 0 &&
    //         rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
    //         rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
    //     );
    // }
    get value() { return this.commentContent; }
    set value(v) {
        if (v !== this.commentContent) {
            this.commentContent = v;
            this.onChange(v);
        }
    }
    writeValue(value) {
        this.commentContent = value;
        this.onChange(value);
    }
    registerOnChange(fn) { this.onChange = fn; }
    registerOnTouched(fn) { this.onTouched = fn; }
    ngOnDestroy() {
        // window.removeEventListener('scroll', this.cbk);
    }
    showResolvedNote() {
        this.showResolved = !this.showResolved;
    }
    changeSwitch() {
        this.showResolved = !this.showResolved;
        this.getNotes();
    }
    checkQuill(replyQuill) {
    }
    strip(html) {
        // this.showLessContent(html);
        let doc = new DOMParser().parseFromString(html, 'text/html');
        return doc.body.textContent || "";
    }
    deleteNote(note) {
        this.commentsService.deleteNote({ config: this.config, commentUUID: note.uuid }).subscribe(response => {
            const comment = response.data;
            this.deleteToCommentList(comment);
        });
    }
    editNote(note) {
        note.updateMode = !note.updateMode;
    }
    confirmDelete(note) {
        note.confirmDelete = !note.confirmDelete;
    }
};
CommentSectionComponent.ctorParameters = () => [
    { type: CommentsService },
    { type: OrderPipe },
    { type: ChangeDetectorRef }
];
__decorate([
    Input()
], CommentSectionComponent.prototype, "frozen", void 0);
__decorate([
    ViewChild('mainCommentBox', { static: false })
], CommentSectionComponent.prototype, "commentBox", void 0);
__decorate([
    ViewChild('commentSectionWrapper', { static: false })
], CommentSectionComponent.prototype, "commentSectionWrapper", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "readOnly", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "currentUser", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "noteType", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "entityId", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "clientId", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "suggestions", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "config", void 0);
__decorate([
    Output()
], CommentSectionComponent.prototype, "suggest", void 0);
__decorate([
    Output()
], CommentSectionComponent.prototype, "mentionClick", void 0);
CommentSectionComponent = CommentSectionComponent_1 = __decorate([
    Component({
        selector: 'wrs-comment-section',
        template: "<div *ngIf = \"loading && noteTypeLoading\">\r\n  Loading notes\r\n</div>\r\n<div *ngIf = \"!loading && !noteTypeLoading\">\r\n<div class=\"resolve-switch text-right text-end d-flex align-items-center justify-content-end mb-2\">\r\n  <span class=\"d-inline-block mr-2\">Show resolved notes</span>\r\n  <label class=\"switch\" data-toggle=\"tooltip\" data-toggle=\"tooltip\" title=\"Show resolved notes\">\r\n    <input type=\"checkbox\" [(ngModel)]=\"showResolved\" (click)=\"changeSwitch()\">\r\n    <span class=\"slider round\"></span>\r\n  </label>\r\n</div>\r\n<div  class=\"comment-box-body mt-2\" *ngIf = \"!readOnly && !showResolved\">\r\n  <wrs-comment-box [config]=\"config\" #mainCommentBox  (sent)=\"pushToCommentList($event)\"\r\n  [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, mainCommentBox)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\" [(ngModel)]=\"commentContent\"></wrs-comment-box>\r\n</div>\r\n</div>\r\n\r\n<ng-template  ngFor let-note [ngForOf]=\"commentsTree\" let-i=\"index\" >\r\n  <div class=\"comment-row mb-2 comment-left\" [ngClass]=\"[\r\n  note?.selected ? 'selected-comment': '',\r\n  true ? 'note_' + note.uuid: ''\r\n]\">\r\n  <div  #commentSectionWrapper class=\"comment-column top-comment pt-2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"note.sender?.imageUrl\" title=\"{{note.sender?.name }} ({{note.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"d-inline-block\" *ngIf = \"!note.updateMode && !note.confirmDelete\">\r\n        <!-- <quill-editor  class=\"read-mode\"  name=\"content\" [(ngModel)]=\"note.content\" [trimOnValidation] = \"true\" [maxLength]=\"maxLength\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"note.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"note.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, note)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"note\" [commentContent] = \"note.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"note.confirmDelete\">\r\n        Deleting this note will permanently delete the entire note trail. Are you sure you want to continue?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(note)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\"btn btn-danger\" (click)= \"deleteNote(note)\">\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == note?.userUuid\">\r\n        <button *ngIf = \"!note.updateMode && note.status == 'ACTIVE' && !readOnly && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"editNote(note)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !note.updateMode && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"confirmDelete(note)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n        <!-- <div class=\"show-more-less\" (click)=\"note.show = !note.show\"><a href=\"#\" onclick=\"event.preventDefault();\"> {{ note.show ? 'Show less': 'Show More' }}<i *ngIf=\"!note.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"note.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\" >\r\n        <span>{{note.sender?.name}} <span class=\"comment-info\" timeago [date]=\"note.creationDate\" [live]=\"true\"></span></span>\r\n      </span>\r\n    </span>\r\n  </div>\r\n  <span class=\"comment-actions resolve\">\r\n    <button *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button button-resolve\"  (click)=\"resolve(note)\" data-toggle=\"tooltip\" title=\"Mark as Resolve\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n    <span class=\"d-flex-inline comment-user-info\">\r\n      <span class=\"mr-2\" *ngIf=\"note.status == 'RESOLVED'\">Resolved by: {{note.resolved?.name}} <span class=\"comment-info\" timeago [date]=\"note?.resolvedAt\"></span></span>\r\n    <button *ngIf=\"note.status == 'RESOLVED'\" style=\"color: green;\" type=\"button\" class=\"icon-button button-resolved\"   title=\"Resolved\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n  </span>\r\n  </span>\r\n  <span *ngIf = \"note.replies.length > 2 && !note.showAllReplie\" class=\"show-reply\" (click)=\"note.showAllReplie = !note.showAllReplie\"><a href=\"#\" onclick=\"event.preventDefault();\">{{'Show '+(note.replies.length - 1) +' replies' }}<i  class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i></a></span>\r\n  <div class=\"comment-column reply-comment\" *ngFor = \"let replyNote of note?.replies;let replyIndex=index;\" >\r\n    <span  *ngIf = \"replyIndex == note?.replies.length - 1 || note.showAllReplie || note.replies.length <= 2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"replyNote.sender?.imageUrl\" title=\"{{replyNote.sender?.name }} ({{replyNote.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"container-show-more d-inline-block\" *ngIf = \"!replyNote.updateMode && !replyNote.confirmDelete\">\r\n        <!-- <quill-editor #replyQuill class=\"read-mode\"  name=\"content\" [(ngModel)]=\"replyNote.content\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"replyNote.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"replyNote.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, replyNote)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"replyNote\" [commentContent] = \"replyNote.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"replyNote.confirmDelete\">\r\n        Are you sure you want to delete this note?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(replyNote)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\" btn btn-danger \" (click)= \"deleteNote(replyNote)\" >\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == replyNote?.userUuid\">\r\n        <button *ngIf = \"!replyNote.updateMode && note.status == 'ACTIVE' && !readOnly && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"editNote(replyNote)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !replyNote.updateMode && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"confirmDelete(replyNote)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n      <!-- <div *ngIf = \"strip(replyNote.content).length > 505\" class=\"show-more-less\" (click)=\"replyNote.show = !replyNote.show\"> <a href=\"#\" onclick=\"event.preventDefault();\">  {{ replyNote.show ? 'Show less': 'Show More' }}<i *ngIf=\"!replyNote.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"replyNote.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\">\r\n          <span>{{replyNote.sender?.name}} <span class=\"comment-info\" timeago [date]=\"replyNote.creationDate\" [live]=\"true\"></span></span>\r\n        </span>\r\n    </span>\r\n  </span>\r\n  </div>\r\n  <span class=\"comment-actions\">\r\n    <button  *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button\"   (click)=\"setReplyTo(note)\" title=\"Reply\">\r\n      <i class=\"fa fa-reply\"></i>\r\n    </button>\r\n  </span>\r\n  <div class=\"comment-text\">\r\n    <div>\r\n      <div>\r\n        <wrs-comment-box [config]=\"config\" #box  *ngIf=\"note.replying\" (sent)=\"pushToCommentList($event, note)\" [rootNote]=\"note\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, box)\" (cancel)=\"cancelReply(note)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n</ng-template>",
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => CommentSectionComponent_1),
                multi: true
            }, TimeagoDefaultFormatter],
        styles: [".comment-row{display:flex;position:relative}.comment-row:before{content:\"\";left:17px;height:100%;width:1px;background-color:#ccc;position:absolute}.comment-row .comment-row:before{height:1px;width:26px;top:20px;left:-17px}.comment-row .comment-row .comment-row:before{height:1px;width:70px;top:20px;left:-50px}.comment-left{justify-content:flex-start}.selected-comment>.comment-text>.comment{-webkit-animation-name:highlighted-comment;animation-name:highlighted-comment;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-duration:4s;animation-duration:4s}@-webkit-keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}@keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}.comment{width:100%;min-width:150px;border-radius:2px;display:inline-block;overflow-x:hidden;padding:2px;position:relative;overflow:visible}.comment-user-info{display:flex;text-overflow:ellipsis;font-size:.8rem;justify-content:flex-end}.comment-user-info.show-resolve{justify-content:flex-start}.comment-right,.comment-right .comment-user-info{flex-direction:row-reverse}.comment-content{padding:5px 0;border-radius:2px}.comment-text{width:100%}.comment-actions{color:#6b778c;font-size:.7em;display:flex}.comment-actions>i{cursor:pointer;margin-right:5px}:host ::ng-deep .ui-dialog{display:flex;flex-direction:column;max-height:100vh;max-width:100%;min-width:150px;min-height:150px}:host ::ng-deep .ui-dialog-content{flex-grow:1}:host ::ng-deep .comment-content img{max-width:500px;height:auto;display:block;background-color:#fff}:host ::ng-deep .comment-content p{margin:5px 0}:host ::ng-deep .comment-section-body{max-height:500px;overflow-y:scroll;padding:5px 10px;border-radius:2px;position:relative}.comment-box-body{margin:1em 0;position:relative}.comment-info{color:grey}:host ::ng-deep quill-editor{display:block}:host ::ng-deep .ql-editor:focus{border:1px solid #80bdff!important}:host ::ng-deep .ng-invalid{border:1px dashed red}:host ::ng-deep .mention{cursor:pointer;color:#fff;border-radius:700px;padding:3px 5px;margin:0 5px}:host ::ng-deep .user-image{border:1px solid #cbcbcb;border-radius:50%;width:30px;background-color:#fff;position:relative;height:30px}:host ::ng-deep [data-denotation-char=\"#\"]{background-color:rgba(140,195,75,.8)}:host ::ng-deep [data-denotation-char=\"@\"]{background-color:rgba(0,122,217,.8)}:host ::ng-deep .ql-mention-list [data-denotation-char=\"@\"]{background-color:#fff}:host ::ng-deep .ql-mention-list [data-denotation-char=\"#\"]{background-color:#fff}.unread-row{display:flex;justify-content:center;font-style:italic}.unread-row span{border-radius:700px;border:1px solid #000;padding:3px 10px;font-size:.8em}.rounded-btn{border:1px solid #007ad9;color:#007ad9;background:0 0;border-radius:30px;margin:0 5px;font-size:13px;padding:5px 10px}.rounded-btn:hover{background-color:#177dfb!important;color:#fff}:host ::ng-deep .button-style{display:inline-block;position:relative;text-decoration:none!important;cursor:pointer;text-align:center;overflow:visible}:host ::ng-deep .ql-container.ql-snow .ql-editor{padding:5px 40px 5px 10px;word-break:break-word}:focus{outline:0!important}wrs-comment-section{display:block;background:#eee;padding:10px}.ql-editor{padding:10px}.comment-box-body{display:flex;flex-direction:row}.comment-box-body wrs-comment-box{flex:1}.comment-box-body .more-option{padding-top:7px;width:30px;display:flex;justify-content:center;font-size:18px}.comment-box-body .more-option i{cursor:pointer}.comment-content .comment-box-body{width:calc(100% - 25px)!important;margin-top:5px;padding-top:0!important}.resolved-comments{padding:0 10px 10px}wrs-comment-box{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;display:block}wrs-comment-box .first-comment-box{flex:1}.comment-row wrs-comment-box{display:block}.comment-row{display:flex;flex-direction:column;border-bottom:0;background-color:rgba(255,255,255,.5);padding:10px;box-shadow:0 0 3px rgba(0,0,0,.1);margin-bottom:15px!important;border-radius:5px}.comment-row:before{display:none}.comment-row .comment-column{display:flex;flex-direction:row;border-top:1px solid #eee}.comment-row .comment-column:last-of-type{padding-bottom:10px}.comment-row .comment-column.reply-comment>span{display:flex;flex:1}.comment-row .comment-column.top-comment{border-top-color:transparent!important}.comment-row .comment-column span.comment-user-info{padding:0 5px 15px}.comment-row .comment-column span.user-icon{margin:0 5px;padding-top:10px}.ql-container.ql-snow{border:1px solid #eee!important;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;width:100%}.comment-box-body .ql-container.ql-snow{box-shadow:none;border-radius:7px!important}.ql-container.ql-snow .ql-editor{padding:5px 10px}.comment-actions{position:absolute;right:0;bottom:8px}.comment-actions.resolve{bottom:unset;top:10px;right:0}.send-button{background:#177dfb!important;padding:5px 10px;border:none;color:#fff}.icon-button{color:#177dfb;margin:0 5px;width:27px;height:27px}.icon-button.edit-delete{color:#1c1d1f}.icon-button:hover{background:#dcecfe;color:#177dfb;border-radius:50%}.icon-button.button-resolve{height:27px;width:27px;font-size:15px;border-radius:50%}.edit-delete-button{display:flex;justify-content:flex-end}.resolve-switch .switch{position:relative;display:inline-block;width:40px;height:20px;margin-bottom:0}.resolve-switch .switch input{opacity:0;width:0;height:0}.resolve-switch .slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;transition:.4s}.resolve-switch .slider:before{position:absolute;content:\"\";height:13px;width:13px;left:4px;bottom:4px;background-color:#fff;transition:.4s}.resolve-switch input:checked+.slider{background-color:#177dfb}.resolve-switch input:focus+.slider{box-shadow:0 0 1px #177dfb}.resolve-switch input:checked+.slider:before{transform:translateX(20px)}.resolve-switch .d-inline-block{font-weight:500;vertical-align:middle}.resolve-switch .slider.round{border-radius:34px}.resolve-switch .slider.round:before{border-radius:50%}:host ::ng-deep .ql-editor.ql-blank::before{left:10px;top:7px}:host ::ng-deep wrs-comment-box .ql-container.ql-snow{box-shadow:none}:host ::ng-deep wrs-comment-box .ql-editor{border-radius:0}:host ::ng-deep .read-mode .ql-container.ql-snow{border:0!important;background:0 0;box-shadow:none;padding:0!important;width:100%}:host ::ng-deep .read-mode .ql-container .ql-editor{padding-left:5px}.comment-user-info span{font-size:11px;font-weight:400}.comment-user-info span .comment-info{color:grey;font-style:italic}.icon-button.button-resolved{height:27px;width:27px;font-size:15px;border-radius:50%;margin-top:-5px}.show-reply{padding-left:43px;font-size:13px;font-weight:400}.show-more-less{font-size:13px;font-weight:400}.delete-confirm{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;width:calc(100% - 25px)!important;margin-top:5px}.btn.btn-danger{background-color:#bd2130!important;padding:.3rem 1.4rem;height:31px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.btn-text.btn-light{width:unset;border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}"]
    })
], CommentSectionComponent);

var CommentBoxComponent_1;
let CommentBoxComponent = CommentBoxComponent_1 = class CommentBoxComponent {
    constructor(commentsService) {
        this.commentsService = commentsService;
        this.commentContent = '';
        this.mentionDenotation = [];
        this.sent = new EventEmitter();
        this.cancel = new EventEmitter();
        this.init = new EventEmitter();
        this.onContentChange = new EventEmitter();
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.modules = null;
        this.user = JSON.parse(localStorage.getItem('user'));
        this.formAction = false;
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    ngOnInit() {
        // this.getNoteType();
        this.modules = {
            mention: {
                allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
                // TODO: User has to provide the denotation characters
                mentionDenotationChars: this.mentionDenotation,
                onSelect: (item, insertItem) => {
                    const editor = this.editor.quillEditor;
                    insertItem(item);
                    // necessary because quill-mention triggers changes as 'api' instead of 'user'
                    editor.insertText(editor.getLength() - 1, '', 'user');
                },
                source: (searchTerm, renderList, mentionChar) => {
                    const event = {
                        query: searchTerm,
                        mentionChar: mentionChar,
                        renderList: renderList
                    };
                    // TODO: Document it for users
                    this.suggest.emit(event);
                },
                // TODO: Provide a map of denotation characters and functions
                renderItem: (item, searchTerm) => {
                    return item.value;
                }
            },
            toolbar: []
        };
    }
    ngAfterViewInit() {
        // this.user['name'] = this.user['full_name'];
    }
    getNoteType() {
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(response => {
            var _a, _b;
            const characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            this.mentionDenotation = characters.split(',');
        });
    }
    updateContent() {
        this.writeValue(this.commentContent);
    }
    doComment() {
        if (!this.commentContent.trim()) {
            return false;
        }
        this.formAction = true;
        if (this.rootNote) {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, rootNote: { uuid: this.rootNote.uuid }, clientId: this.clientId })
                .pipe(finalize(() => this.resetState()))
                .subscribe(accessObj => {
                const comment = accessObj.data;
                this.sent.emit(comment);
            });
        }
        else {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId })
                .pipe(finalize(() => this.resetState()))
                .subscribe(accessObj => {
                const comment = accessObj.data;
                comment['user'] = this.user;
                this.sent.emit(comment);
            });
        }
    }
    cancelComment() {
        this.cancel.emit(true);
    }
    resetState() {
        this.commentContent = null;
        this.formAction = false;
    }
    inititalized(editor) {
        this.init.emit(editor);
    }
    get value() {
        return this.commentContent;
    }
    set value(v) {
        if (v !== this.commentContent) {
            this.commentContent = v;
            this.onChange(v);
        }
    }
    writeValue(value) {
        this.commentContent = value;
        this.onContentChange.emit(this.commentContent);
        this.onChange(value);
    }
    updateNote(note) {
        this.commentsService.updateNote({ config: this.config, comment: this.commentContent, commentUUID: note.uuid }).subscribe(response => {
            const comment = response.data;
            note.updateMode = !note.updateMode;
            this.sent.emit(comment);
        });
    }
    editNote(note) {
        note.updateMode = !note.updateMode;
    }
    registerOnChange(fn) { this.onChange = fn; }
    registerOnTouched(fn) { this.onTouched = fn; }
};
CommentBoxComponent.ctorParameters = () => [
    { type: CommentsService }
];
__decorate([
    Input()
], CommentBoxComponent.prototype, "commentContent", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "note", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "rootNote", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "noteType", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "entityId", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "clientId", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "mentionDenotation", void 0);
__decorate([
    ViewChild(QuillEditorComponent, { static: false })
], CommentBoxComponent.prototype, "editor", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "sent", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "cancel", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "init", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "onContentChange", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "suggestions", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "config", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "suggest", void 0);
CommentBoxComponent = CommentBoxComponent_1 = __decorate([
    Component({
        selector: 'wrs-comment-box',
        template: "<div class=\"first-comment-box\">\r\n  <quill-editor  #editor\r\n    [(ngModel)]=\"commentContent\" name=\"commentContent\" [style]=\"{'height':'150px'}\" [disabled]=\"formAction\"\r\n    placeholder=\"Enter note\" (onInit)=\"inititalized($event)\" [modules]=\"modules\"\r\n    (onContentChanged)=\"updateContent()\"></quill-editor>\r\n</div>\r\n<div class=\"text-right text-end mt-2\">\r\n  <button  type=\"button\" class=\"icon-button btn-light\" *ngIf=\"rootNote\"\r\n    (click)=\"cancelComment()\" [disabled]=\"formAction\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"rootNote\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"!rootNote && !note?.updateMode\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>\r\n<div class=\"text-right text-end mt-2\" *ngIf = \"note?.updateMode\">\r\n  <button  type=\"button\" (click) = \"editNote(note)\" class=\"icon-button btn-light\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" (click) = \"updateNote(note)\">\r\n    save\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>",
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => CommentBoxComponent_1),
                multi: true
            }],
        styles: [":host ::ng-deep .ql-toolbar.ql-snow{display:none}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-top:1px solid #c8c8c8}:host ::ng-deep .ql-editor{min-height:50px}:host ::ng-deep .ui-send-comment{background:#177dfb!important;border-color:#177dfb!important;border-radius:26px;font-size:14px;margin:5px;transition:.3s;color:#fff;padding:5px 10px}:host ::ng-deep .ui-send-comment:hover{background:0 0!important;border-color:#177dfb!important;color:#177dfb!important}:host ::ng-deep .ui-send-comment-cancel{background:#177dfb!important}:host ::ng-deep .ql-mention-list-container{width:270px;border:1px solid #f0f0f0;border-radius:4px;background-color:#fff;box-shadow:0 2px 12px 0 rgba(30,30,30,.08);z-index:9001;max-height:150px;overflow-y:scroll}:host ::ng-deep .ql-mention-list{list-style:none;margin:0;padding:0;overflow:hidden}:host ::ng-deep .ql-mention-list-item{cursor:pointer;height:40px;line-height:40px;font-size:14px;padding:0 20px;border-bottom:1px solid #e4e4e4;vertical-align:middle}:host ::ng-deep .ql-mention-list-item.selected{background-color:#d3e1eb;text-decoration:none}:host ::ng-deep .mention{height:24px;width:65px;border-radius:6px;background-color:#177dfb;padding:3px 0;margin-right:2px}:host ::ng-deep .mention>span{margin:0 3px}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-radius:5px}.send-button{background:#177dfb!important;padding:7px 10px;border:none;color:#fff;border-radius:3px;margin-left:10px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.icon-button.button-resolve{background:#177dfb;color:#fff;position:absolute;bottom:30px;right:12px;border-radius:0 14px 14px 0;height:40px}"]
    })
], CommentBoxComponent);

let CommentSectionModule = class CommentSectionModule {
};
CommentSectionModule = __decorate([
    NgModule({
        declarations: [CommentSectionComponent, CommentBoxComponent],
        imports: [
            CommonModule,
            FormsModule,
            TimeagoModule.forRoot(),
            QuillModule.forRoot(),
            OrderModule
            // NgInviewModule
        ],
        exports: [CommentSectionComponent],
    })
], CommentSectionModule);

/*
 * Public API Surface of comment-section
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CommentSectionComponent, CommentSectionModule, CommentsService, CommentBoxComponent as ɵa };
//# sourceMappingURL=comment-section.js.map
