var CommentSectionComponent_1;
import { __decorate } from "tslib";
import { AfterContentInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommentsService } from './comment-section.service';
import { OrderPipe } from 'ngx-order-pipe';
import { TimeagoDefaultFormatter } from 'ngx-timeago';
let CommentSectionComponent = CommentSectionComponent_1 = class CommentSectionComponent {
    constructor(commentsService, orderService, cdr) {
        this.commentsService = commentsService;
        this.orderService = orderService;
        this.cdr = cdr;
        this.commentContent = '';
        this.frozen = false;
        this.readOnly = false;
        this.loading = true;
        this.noteTypeLoading = true;
        this.commentList = [];
        this.isLastReadUpdated = false;
        this.notes = [];
        this.commentsTree = [];
        this.mentionDenotation = [];
        this.show = false;
        this.readNote = true;
        this.loadCommentUserStats = true;
        this.commentMap = {};
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.mentionClick = new EventEmitter();
        // cbk = debounce(() => this.inview, 300);
        // cbk =  () => this.inview();
        this.showResolved = false;
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    ngOnInit() {
        // window.addEventListener('scroll',this.cbk);
        // this.getNoteType();
    }
    updateCommentContent(newVal) {
        this.writeValue(newVal);
    }
    ngAfterContentInit() {
        this.getNotes();
        this.getNoteType();
    }
    getNotes() {
        this.loading = true;
        this.commentsTree = [];
        let status = 'ACTIVE';
        if (this.showResolved) {
            status = 'RESOLVED';
        }
        this.commentsService.getNotes({ config: this.config, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId, status: status })
            .subscribe(accessObj => {
            this.notes = accessObj.data;
            const commentSection = accessObj.data;
            let sordBy = 'creationDate';
            if (status == 'RESOLVED') {
                sordBy = 'resolvedAt';
            }
            const comments = this.orderService.transform(commentSection, sordBy);
            this.commentList = comments;
            comments.forEach(comment => {
                this.commentMap[comment.uuid] = comment;
                comment.replies = [];
            });
            comments.forEach(comment => {
                if (comment.rootNote) {
                    this.commentMap[comment.rootNote.uuid].replies.push(comment);
                }
                else {
                    this.commentsTree.push(comment);
                }
            });
            this.loading = false;
            this.updateLastRead();
        }, error => {
            this.loading = false;
        });
    }
    getNoteType() {
        this.noteTypeLoading = true;
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(response => {
            var _a, _b;
            this.noteTypeData = response === null || response === void 0 ? void 0 : response.data;
            const characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            this.mentionDenotation = characters.split(',');
            this.noteTypeLoading = false;
        }, error => {
            this.noteTypeLoading = false;
        });
    }
    ngAfterViewChecked() {
        // console.log('mention', this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'));
        // Array.from(this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'))
        //   .forEach(elem => {
        //     const mentionChar = (<any>elem).getAttribute('data-denotation-char');
        //     const id = (<any>elem).getAttribute('data-id');
        //     const value = (<any>elem).getAttribute('data-value');
        //     const elemRef = elem;
        //     (<any>elem).onclick = (event) => {
        //       this.mentionClick.emit({mentionChar, id, value, elemRef, event});
        //   };
        //   console.log('listerner set on ', elem);
        // });
    }
    ngOnChange() {
    }
    setStatsOnView() {
        // this.loadCommentUserStats = true;
        // if()
        // this.commentsService.getUserStats({config: this.config, noteType : this.noteTypeData.uuid ,entityId : this.entityId})
        //   .subscribe(data => {
        //     this.commentUserStats = data.data;
        //     const commentList = this.commentList.filter(c => c.userId !== this.currentUser.id);
        //     const comment = commentList[commentList.length - this.commentUserStats.unreadCount];
        //     this.loadCommentUserStats = false;
        //     this.updateLastRead();
        //     if (comment) {
        //       comment.firstUnread = true;
        //     }
        //   });
    }
    setReplyTo(comment) {
        comment.replying = true;
        this.cdr.detectChanges();
        setTimeout(() => this.scroll('comment_quill_' + comment.uuid), 150);
    }
    resolve(comment) {
        this.commentsService.resolveComment({ config: this.config, noteTypeId: comment.noteType.id, entityId: this.entityId, noteUUID: comment.uuid }).subscribe(data => {
            this.commentsTree = this.commentsTree.filter((note) => note.uuid != comment.uuid);
        });
        this.cdr.detectChanges();
        setTimeout(() => this.scroll('comment_quill_' + comment.uuid), 150);
    }
    pushToCommentList(addedComment, comment) {
        this.commentMap[addedComment.uuid] = addedComment;
        this.commentList.push(addedComment);
        addedComment.replies = [];
        if (comment) {
            comment.replying = false;
        }
        if (addedComment.rootNote) {
            this.commentMap[comment.uuid].replies.push(addedComment);
        }
        else {
            this.commentsTree.push(addedComment);
        }
        this.cdr.detectChanges();
        this.scrollToComment(addedComment);
    }
    pushWhenEdit(editNote, comment) {
        this.commentsTree.forEach(comment => {
            if (editNote.uuid == comment.uuid) {
                comment.content = editNote.content;
            }
            else {
                comment.replies.forEach(reply => {
                    if (editNote.uuid == reply.uuid)
                        reply.content = editNote.content;
                });
            }
        });
        this.cdr.detectChanges();
    }
    deleteToCommentList(deletedNote) {
        this.commentsTree = this.commentsTree.filter(note => note.uuid != deletedNote.uuid);
        this.commentsTree.forEach(comment => {
            comment.replies = comment.replies.filter(x => x.uuid != deletedNote.uuid);
        });
    }
    cancelReply(comment) {
        comment.replying = false;
    }
    scrollToComment(comment) {
        this.scroll(this.getClassName(comment), () => {
            const repliedComment = this.commentList.filter(commentObject => comment.uuid == commentObject.uuid)[0];
            if (repliedComment) {
                this.selectComment(repliedComment);
            }
        });
    }
    scroll(className, callback) {
        const classElement = document.getElementsByClassName(className);
        if (classElement.length > 0) {
            classElement[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
            if (callback) {
                callback();
            }
        }
    }
    getClassName(comment) {
        return 'note_' + comment.uuid;
    }
    selectComment(comment) {
        comment['selected'] = true;
        setTimeout(() => comment['selected'] = false, 3000);
    }
    get mainCommentBox() {
        return this.commentBox;
    }
    setFocus(editor, box) {
        box.editor.getQuill().focus();
    }
    formatDate(date_str) {
        const date = new Date(date_str);
        return date.toLocaleString();
    }
    onSuggest(event) {
        this.suggest.emit(event);
    }
    updateLastRead() {
        if (!this.isLastReadUpdated) {
            const comments = this.commentList.filter(c => c.userId !== this.currentUser.id);
            const lastComment = comments[comments.length - 1];
            if (lastComment) {
                this.commentsService.updateLastRead({ config: this.config, noteTypeId: this.noteTypeData.id, entityId: this.entityId, commentUUID: lastComment.uuid }).subscribe(data => {
                    this.isLastReadUpdated = true;
                });
            }
        }
    }
    //   isElementInViewport (el) {
    //     const rect = el.getBoundingClientRect();
    //     return (
    //         rect.top >= 0 &&
    //         rect.left >= 0 &&
    //         rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
    //         rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
    //     );
    // }
    get value() { return this.commentContent; }
    set value(v) {
        if (v !== this.commentContent) {
            this.commentContent = v;
            this.onChange(v);
        }
    }
    writeValue(value) {
        this.commentContent = value;
        this.onChange(value);
    }
    registerOnChange(fn) { this.onChange = fn; }
    registerOnTouched(fn) { this.onTouched = fn; }
    ngOnDestroy() {
        // window.removeEventListener('scroll', this.cbk);
    }
    showResolvedNote() {
        this.showResolved = !this.showResolved;
    }
    changeSwitch() {
        this.showResolved = !this.showResolved;
        this.getNotes();
    }
    checkQuill(replyQuill) {
    }
    strip(html) {
        // this.showLessContent(html);
        let doc = new DOMParser().parseFromString(html, 'text/html');
        return doc.body.textContent || "";
    }
    deleteNote(note) {
        this.commentsService.deleteNote({ config: this.config, commentUUID: note.uuid }).subscribe(response => {
            const comment = response.data;
            this.deleteToCommentList(comment);
        });
    }
    editNote(note) {
        note.updateMode = !note.updateMode;
    }
    confirmDelete(note) {
        note.confirmDelete = !note.confirmDelete;
    }
};
CommentSectionComponent.ctorParameters = () => [
    { type: CommentsService },
    { type: OrderPipe },
    { type: ChangeDetectorRef }
];
__decorate([
    Input()
], CommentSectionComponent.prototype, "frozen", void 0);
__decorate([
    ViewChild('mainCommentBox', { static: false })
], CommentSectionComponent.prototype, "commentBox", void 0);
__decorate([
    ViewChild('commentSectionWrapper', { static: false })
], CommentSectionComponent.prototype, "commentSectionWrapper", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "readOnly", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "currentUser", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "noteType", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "entityId", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "clientId", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "suggestions", void 0);
__decorate([
    Input()
], CommentSectionComponent.prototype, "config", void 0);
__decorate([
    Output()
], CommentSectionComponent.prototype, "suggest", void 0);
__decorate([
    Output()
], CommentSectionComponent.prototype, "mentionClick", void 0);
CommentSectionComponent = CommentSectionComponent_1 = __decorate([
    Component({
        selector: 'wrs-comment-section',
        template: "<div *ngIf = \"loading && noteTypeLoading\">\r\n  Loading notes\r\n</div>\r\n<div *ngIf = \"!loading && !noteTypeLoading\">\r\n<div class=\"resolve-switch text-right text-end d-flex align-items-center justify-content-end mb-2\">\r\n  <span class=\"d-inline-block mr-2\">Show resolved notes</span>\r\n  <label class=\"switch\" data-toggle=\"tooltip\" data-toggle=\"tooltip\" title=\"Show resolved notes\">\r\n    <input type=\"checkbox\" [(ngModel)]=\"showResolved\" (click)=\"changeSwitch()\">\r\n    <span class=\"slider round\"></span>\r\n  </label>\r\n</div>\r\n<div  class=\"comment-box-body mt-2\" *ngIf = \"!readOnly && !showResolved\">\r\n  <wrs-comment-box [config]=\"config\" #mainCommentBox  (sent)=\"pushToCommentList($event)\"\r\n  [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, mainCommentBox)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\" [(ngModel)]=\"commentContent\"></wrs-comment-box>\r\n</div>\r\n</div>\r\n\r\n<ng-template  ngFor let-note [ngForOf]=\"commentsTree\" let-i=\"index\" >\r\n  <div class=\"comment-row mb-2 comment-left\" [ngClass]=\"[\r\n  note?.selected ? 'selected-comment': '',\r\n  true ? 'note_' + note.uuid: ''\r\n]\">\r\n  <div  #commentSectionWrapper class=\"comment-column top-comment pt-2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"note.sender?.imageUrl\" title=\"{{note.sender?.name }} ({{note.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"d-inline-block\" *ngIf = \"!note.updateMode && !note.confirmDelete\">\r\n        <!-- <quill-editor  class=\"read-mode\"  name=\"content\" [(ngModel)]=\"note.content\" [trimOnValidation] = \"true\" [maxLength]=\"maxLength\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"note.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"note.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, note)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"note\" [commentContent] = \"note.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"note.confirmDelete\">\r\n        Deleting this note will permanently delete the entire note trail. Are you sure you want to continue?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(note)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\"btn btn-danger\" (click)= \"deleteNote(note)\">\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == note?.userUuid\">\r\n        <button *ngIf = \"!note.updateMode && note.status == 'ACTIVE' && !readOnly && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"editNote(note)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !note.updateMode && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"confirmDelete(note)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n        <!-- <div class=\"show-more-less\" (click)=\"note.show = !note.show\"><a href=\"#\" onclick=\"event.preventDefault();\"> {{ note.show ? 'Show less': 'Show More' }}<i *ngIf=\"!note.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"note.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\" >\r\n        <span>{{note.sender?.name}} <span class=\"comment-info\" timeago [date]=\"note.creationDate\" [live]=\"true\"></span></span>\r\n      </span>\r\n    </span>\r\n  </div>\r\n  <span class=\"comment-actions resolve\">\r\n    <button *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button button-resolve\"  (click)=\"resolve(note)\" data-toggle=\"tooltip\" title=\"Mark as Resolve\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n    <span class=\"d-flex-inline comment-user-info\">\r\n      <span class=\"mr-2\" *ngIf=\"note.status == 'RESOLVED'\">Resolved by: {{note.resolved?.name}} <span class=\"comment-info\" timeago [date]=\"note?.resolvedAt\"></span></span>\r\n    <button *ngIf=\"note.status == 'RESOLVED'\" style=\"color: green;\" type=\"button\" class=\"icon-button button-resolved\"   title=\"Resolved\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n  </span>\r\n  </span>\r\n  <span *ngIf = \"note.replies.length > 2 && !note.showAllReplie\" class=\"show-reply\" (click)=\"note.showAllReplie = !note.showAllReplie\"><a href=\"#\" onclick=\"event.preventDefault();\">{{'Show '+(note.replies.length - 1) +' replies' }}<i  class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i></a></span>\r\n  <div class=\"comment-column reply-comment\" *ngFor = \"let replyNote of note?.replies;let replyIndex=index;\" >\r\n    <span  *ngIf = \"replyIndex == note?.replies.length - 1 || note.showAllReplie || note.replies.length <= 2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"replyNote.sender?.imageUrl\" title=\"{{replyNote.sender?.name }} ({{replyNote.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"container-show-more d-inline-block\" *ngIf = \"!replyNote.updateMode && !replyNote.confirmDelete\">\r\n        <!-- <quill-editor #replyQuill class=\"read-mode\"  name=\"content\" [(ngModel)]=\"replyNote.content\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"replyNote.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"replyNote.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, replyNote)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"replyNote\" [commentContent] = \"replyNote.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"replyNote.confirmDelete\">\r\n        Are you sure you want to delete this note?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(replyNote)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\" btn btn-danger \" (click)= \"deleteNote(replyNote)\" >\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == replyNote?.userUuid\">\r\n        <button *ngIf = \"!replyNote.updateMode && note.status == 'ACTIVE' && !readOnly && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"editNote(replyNote)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !replyNote.updateMode && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"confirmDelete(replyNote)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n      <!-- <div *ngIf = \"strip(replyNote.content).length > 505\" class=\"show-more-less\" (click)=\"replyNote.show = !replyNote.show\"> <a href=\"#\" onclick=\"event.preventDefault();\">  {{ replyNote.show ? 'Show less': 'Show More' }}<i *ngIf=\"!replyNote.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"replyNote.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\">\r\n          <span>{{replyNote.sender?.name}} <span class=\"comment-info\" timeago [date]=\"replyNote.creationDate\" [live]=\"true\"></span></span>\r\n        </span>\r\n    </span>\r\n  </span>\r\n  </div>\r\n  <span class=\"comment-actions\">\r\n    <button  *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button\"   (click)=\"setReplyTo(note)\" title=\"Reply\">\r\n      <i class=\"fa fa-reply\"></i>\r\n    </button>\r\n  </span>\r\n  <div class=\"comment-text\">\r\n    <div>\r\n      <div>\r\n        <wrs-comment-box [config]=\"config\" #box  *ngIf=\"note.replying\" (sent)=\"pushToCommentList($event, note)\" [rootNote]=\"note\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, box)\" (cancel)=\"cancelReply(note)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n</ng-template>",
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => CommentSectionComponent_1),
                multi: true
            }, TimeagoDefaultFormatter],
        styles: [".comment-row{display:flex;position:relative}.comment-row:before{content:\"\";left:17px;height:100%;width:1px;background-color:#ccc;position:absolute}.comment-row .comment-row:before{height:1px;width:26px;top:20px;left:-17px}.comment-row .comment-row .comment-row:before{height:1px;width:70px;top:20px;left:-50px}.comment-left{justify-content:flex-start}.selected-comment>.comment-text>.comment{-webkit-animation-name:highlighted-comment;animation-name:highlighted-comment;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-duration:4s;animation-duration:4s}@-webkit-keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}@keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}.comment{width:100%;min-width:150px;border-radius:2px;display:inline-block;overflow-x:hidden;padding:2px;position:relative;overflow:visible}.comment-user-info{display:flex;text-overflow:ellipsis;font-size:.8rem;justify-content:flex-end}.comment-user-info.show-resolve{justify-content:flex-start}.comment-right,.comment-right .comment-user-info{flex-direction:row-reverse}.comment-content{padding:5px 0;border-radius:2px}.comment-text{width:100%}.comment-actions{color:#6b778c;font-size:.7em;display:flex}.comment-actions>i{cursor:pointer;margin-right:5px}:host ::ng-deep .ui-dialog{display:flex;flex-direction:column;max-height:100vh;max-width:100%;min-width:150px;min-height:150px}:host ::ng-deep .ui-dialog-content{flex-grow:1}:host ::ng-deep .comment-content img{max-width:500px;height:auto;display:block;background-color:#fff}:host ::ng-deep .comment-content p{margin:5px 0}:host ::ng-deep .comment-section-body{max-height:500px;overflow-y:scroll;padding:5px 10px;border-radius:2px;position:relative}.comment-box-body{margin:1em 0;position:relative}.comment-info{color:grey}:host ::ng-deep quill-editor{display:block}:host ::ng-deep .ql-editor:focus{border:1px solid #80bdff!important}:host ::ng-deep .ng-invalid{border:1px dashed red}:host ::ng-deep .mention{cursor:pointer;color:#fff;border-radius:700px;padding:3px 5px;margin:0 5px}:host ::ng-deep .user-image{border:1px solid #cbcbcb;border-radius:50%;width:30px;background-color:#fff;position:relative;height:30px}:host ::ng-deep [data-denotation-char=\"#\"]{background-color:rgba(140,195,75,.8)}:host ::ng-deep [data-denotation-char=\"@\"]{background-color:rgba(0,122,217,.8)}:host ::ng-deep .ql-mention-list [data-denotation-char=\"@\"]{background-color:#fff}:host ::ng-deep .ql-mention-list [data-denotation-char=\"#\"]{background-color:#fff}.unread-row{display:flex;justify-content:center;font-style:italic}.unread-row span{border-radius:700px;border:1px solid #000;padding:3px 10px;font-size:.8em}.rounded-btn{border:1px solid #007ad9;color:#007ad9;background:0 0;border-radius:30px;margin:0 5px;font-size:13px;padding:5px 10px}.rounded-btn:hover{background-color:#177dfb!important;color:#fff}:host ::ng-deep .button-style{display:inline-block;position:relative;text-decoration:none!important;cursor:pointer;text-align:center;overflow:visible}:host ::ng-deep .ql-container.ql-snow .ql-editor{padding:5px 40px 5px 10px;word-break:break-word}:focus{outline:0!important}wrs-comment-section{display:block;background:#eee;padding:10px}.ql-editor{padding:10px}.comment-box-body{display:flex;flex-direction:row}.comment-box-body wrs-comment-box{flex:1}.comment-box-body .more-option{padding-top:7px;width:30px;display:flex;justify-content:center;font-size:18px}.comment-box-body .more-option i{cursor:pointer}.comment-content .comment-box-body{width:calc(100% - 25px)!important;margin-top:5px;padding-top:0!important}.resolved-comments{padding:0 10px 10px}wrs-comment-box{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;display:block}wrs-comment-box .first-comment-box{flex:1}.comment-row wrs-comment-box{display:block}.comment-row{display:flex;flex-direction:column;border-bottom:0;background-color:rgba(255,255,255,.5);padding:10px;box-shadow:0 0 3px rgba(0,0,0,.1);margin-bottom:15px!important;border-radius:5px}.comment-row:before{display:none}.comment-row .comment-column{display:flex;flex-direction:row;border-top:1px solid #eee}.comment-row .comment-column:last-of-type{padding-bottom:10px}.comment-row .comment-column.reply-comment>span{display:flex;flex:1}.comment-row .comment-column.top-comment{border-top-color:transparent!important}.comment-row .comment-column span.comment-user-info{padding:0 5px 15px}.comment-row .comment-column span.user-icon{margin:0 5px;padding-top:10px}.ql-container.ql-snow{border:1px solid #eee!important;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;width:100%}.comment-box-body .ql-container.ql-snow{box-shadow:none;border-radius:7px!important}.ql-container.ql-snow .ql-editor{padding:5px 10px}.comment-actions{position:absolute;right:0;bottom:8px}.comment-actions.resolve{bottom:unset;top:10px;right:0}.send-button{background:#177dfb!important;padding:5px 10px;border:none;color:#fff}.icon-button{color:#177dfb;margin:0 5px;width:27px;height:27px}.icon-button.edit-delete{color:#1c1d1f}.icon-button:hover{background:#dcecfe;color:#177dfb;border-radius:50%}.icon-button.button-resolve{height:27px;width:27px;font-size:15px;border-radius:50%}.edit-delete-button{display:flex;justify-content:flex-end}.resolve-switch .switch{position:relative;display:inline-block;width:40px;height:20px;margin-bottom:0}.resolve-switch .switch input{opacity:0;width:0;height:0}.resolve-switch .slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;transition:.4s}.resolve-switch .slider:before{position:absolute;content:\"\";height:13px;width:13px;left:4px;bottom:4px;background-color:#fff;transition:.4s}.resolve-switch input:checked+.slider{background-color:#177dfb}.resolve-switch input:focus+.slider{box-shadow:0 0 1px #177dfb}.resolve-switch input:checked+.slider:before{transform:translateX(20px)}.resolve-switch .d-inline-block{font-weight:500;vertical-align:middle}.resolve-switch .slider.round{border-radius:34px}.resolve-switch .slider.round:before{border-radius:50%}:host ::ng-deep .ql-editor.ql-blank::before{left:10px;top:7px}:host ::ng-deep wrs-comment-box .ql-container.ql-snow{box-shadow:none}:host ::ng-deep wrs-comment-box .ql-editor{border-radius:0}:host ::ng-deep .read-mode .ql-container.ql-snow{border:0!important;background:0 0;box-shadow:none;padding:0!important;width:100%}:host ::ng-deep .read-mode .ql-container .ql-editor{padding-left:5px}.comment-user-info span{font-size:11px;font-weight:400}.comment-user-info span .comment-info{color:grey;font-style:italic}.icon-button.button-resolved{height:27px;width:27px;font-size:15px;border-radius:50%;margin-top:-5px}.show-reply{padding-left:43px;font-size:13px;font-weight:400}.show-more-less{font-size:13px;font-weight:400}.delete-confirm{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;width:calc(100% - 25px)!important;margin-top:5px}.btn.btn-danger{background-color:#bd2130!important;padding:.3rem 1.4rem;height:31px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.btn-text.btn-light{width:unset;border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}"]
    })
], CommentSectionComponent);
export { CommentSectionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1zZWN0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1lbnQtc2VjdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50LXNlY3Rpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkosT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUU1RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFJM0MsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sYUFBYSxDQUFDO0FBZ0J0RCxJQUFhLHVCQUF1QiwrQkFBcEMsTUFBYSx1QkFBdUI7SUFvRGxDLFlBQ1UsZUFBZ0MsRUFDaEMsWUFBdUIsRUFDdkIsR0FBc0I7UUFGdEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLGlCQUFZLEdBQVosWUFBWSxDQUFXO1FBQ3ZCLFFBQUcsR0FBSCxHQUFHLENBQW1CO1FBdERoQyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNYLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFNZixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBRzFCLFlBQU8sR0FBWSxJQUFJLENBQUM7UUFDeEIsb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsc0JBQWlCLEdBQVUsRUFBRSxDQUFDO1FBQzlCLFNBQUksR0FBWSxLQUFLLENBQUM7UUFDdEIsYUFBUSxHQUFZLElBQUksQ0FBQztRQUV6Qix5QkFBb0IsR0FBWSxJQUFJLENBQUM7UUFXckMsZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUdoQixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQVFqQixZQUFPLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFHaEQsaUJBQVksR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyRCwwQ0FBMEM7UUFDMUMsOEJBQThCO1FBQzlCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBcVA5QixhQUFRLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFFLENBQUMsQ0FBQztRQUNyQixjQUFTLEdBQUcsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO0lBbFBlLENBQUM7SUFFckMsUUFBUTtRQUNOLDhDQUE4QztRQUM5QyxzQkFBc0I7SUFDeEIsQ0FBQztJQUVELG9CQUFvQixDQUFDLE1BQWM7UUFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQsa0JBQWtCO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUV2QixJQUFJLE1BQU0sR0FBRyxRQUFRLENBQUM7UUFDdEIsSUFBRyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3BCLE1BQU0sR0FBRyxVQUFVLENBQUM7U0FDckI7UUFDRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRyxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUMsQ0FBQzthQUM3SSxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQzVCLE1BQU0sY0FBYyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFDdEMsSUFBSSxNQUFNLEdBQUcsY0FBYyxDQUFDO1lBQzVCLElBQUcsTUFBTSxJQUFJLFVBQVUsRUFBRTtnQkFDdkIsTUFBTSxHQUFHLFlBQVksQ0FBQzthQUN2QjtZQUNELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVyRSxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQztZQUM1QixRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7Z0JBQ3hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBRUgsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDekIsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO29CQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDOUQ7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2pDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUVyQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ1IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTs7WUFDakksSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLGFBQVIsUUFBUSx1QkFBUixRQUFRLENBQUUsSUFBSSxDQUFDO1lBQ25DLE1BQU0sVUFBVSxlQUFHLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxJQUFJLDBDQUFFLGNBQWMsMENBQUUsaUJBQWlCLENBQUM7WUFDckUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxFQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFDTixDQUFDO0lBQ0Qsa0JBQWtCO1FBQ2hCLHNHQUFzRztRQUN0Ryx5RkFBeUY7UUFDekYsdUJBQXVCO1FBQ3ZCLDRFQUE0RTtRQUM1RSxzREFBc0Q7UUFDdEQsNERBQTREO1FBQzVELDRCQUE0QjtRQUM1Qix5Q0FBeUM7UUFDekMsMEVBQTBFO1FBQzFFLE9BQU87UUFDUCw0Q0FBNEM7UUFDNUMsTUFBTTtJQUNSLENBQUM7SUFFRCxVQUFVO0lBRVYsQ0FBQztJQUVELGNBQWM7UUFDWixvQ0FBb0M7UUFDcEMsT0FBTztRQUNQLHdIQUF3SDtRQUN4SCx5QkFBeUI7UUFDekIseUNBQXlDO1FBQ3pDLDBGQUEwRjtRQUMxRiwyRkFBMkY7UUFDM0YseUNBQXlDO1FBQ3pDLDZCQUE2QjtRQUM3QixxQkFBcUI7UUFDckIsb0NBQW9DO1FBQ3BDLFFBQVE7UUFDUixRQUFRO0lBQ1YsQ0FBQztJQUVELFVBQVUsQ0FBQyxPQUFhO1FBQ3RCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCxPQUFPLENBQUMsT0FBTztRQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLEVBQUUsTUFBTSxFQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRyxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUUsSUFBSSxDQUFDLEVBQUU7WUFDcEssSUFBSSxDQUFDLFlBQVksR0FBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEYsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hCLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBRUQsaUJBQWlCLENBQUMsWUFBa0IsRUFBRSxPQUFjO1FBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLFlBQVksQ0FBQztRQUNsRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNwQyxZQUFZLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLE9BQU8sRUFBRTtZQUNYLE9BQU8sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxZQUFZLENBQUMsUUFBUSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDMUQ7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3RDO1FBRUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxZQUFZLENBQUMsUUFBYyxFQUFDLE9BQWM7UUFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDbEMsSUFBRyxRQUFRLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUU7Z0JBQ2hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQzthQUNwQztpQkFBTTtnQkFDUCxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDOUIsSUFBRyxRQUFRLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxJQUFJO3dCQUM5QixLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7Z0JBQ25DLENBQUMsQ0FBQyxDQUFDO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVELG1CQUFtQixDQUFDLFdBQWlCO1FBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNqQyxPQUFPLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0UsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQU87UUFDakIsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFhO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLEVBQUU7WUFDM0MsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RyxJQUFJLGNBQWMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUNwQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELE1BQU0sQ0FBQyxTQUFpQixFQUFFLFFBQVM7UUFDakMsTUFBTSxZQUFZLEdBQUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hFLElBQUksWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0IsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUcsS0FBSyxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQUM7WUFDeEUsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLENBQUM7YUFDWjtTQUNGO0lBQ0gsQ0FBQztJQUVELFlBQVksQ0FBQyxPQUFhO1FBQ3hCLE9BQU8sT0FBTyxHQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDL0IsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUFhO1FBQ3pCLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDM0IsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELElBQUksY0FBYztRQUNoQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQztJQUVELFFBQVEsQ0FBQyxNQUFNLEVBQUUsR0FBRztRQUNsQixHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxVQUFVLENBQUMsUUFBZ0I7UUFDekIsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEMsT0FBTyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUFVO1FBQ2xCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMzQixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNoRixNQUFNLFdBQVcsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLFdBQVcsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsRUFBRSxNQUFNLEVBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDekssSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDL0IsQ0FBQyxDQUFDLENBQUM7YUFDSDtTQUNGO0lBQ0gsQ0FBQztJQUVILCtCQUErQjtJQUMvQiwrQ0FBK0M7SUFFL0MsZUFBZTtJQUNmLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0hBQXNIO0lBQ3RILCtHQUErRztJQUMvRyxTQUFTO0lBQ1QsSUFBSTtJQUdGLElBQUksS0FBSyxLQUFhLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFFbkQsSUFBSSxLQUFLLENBQUMsQ0FBUztRQUNqQixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQzdCLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbEI7SUFDSCxDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQWE7UUFDdEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBSUQsZ0JBQWdCLENBQUMsRUFBb0IsSUFBVSxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEUsaUJBQWlCLENBQUMsRUFBYyxJQUFVLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUVoRSxXQUFXO1FBQ1Qsa0RBQWtEO0lBQ3BELENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsWUFBWSxHQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMxQyxDQUFDO0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsVUFBVSxDQUFDLFVBQVU7SUFDckIsQ0FBQztJQUVELEtBQUssQ0FBQyxJQUFJO1FBQ1IsOEJBQThCO1FBQzlCLElBQUksR0FBRyxHQUFHLElBQUksU0FBUyxFQUFFLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztRQUM3RCxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQztJQUVyQyxDQUFDO0lBR0QsVUFBVSxDQUFDLElBQVU7UUFDcEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2pHLE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxhQUFhLENBQUMsSUFBVTtRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUMzQyxDQUFDO0NBRUEsQ0FBQTs7WUEvUjRCLGVBQWU7WUFDbEIsU0FBUztZQUNsQixpQkFBaUI7O0FBckR2QjtJQUFSLEtBQUssRUFBRTt1REFBZ0I7QUFHeEI7SUFEQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFDLENBQUM7MkRBQ2I7QUFFcUI7SUFBcEQsU0FBUyxDQUFDLHVCQUF1QixFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQyxDQUFDO3NFQUFtQztBQUM5RTtJQUFSLEtBQUssRUFBRTt5REFBa0I7QUFDakI7SUFBUixLQUFLLEVBQUU7NERBQVk7QUFnQnBCO0lBREMsS0FBSyxFQUFFO3lEQUNTO0FBR2pCO0lBREMsS0FBSyxFQUFFO3lEQUNTO0FBR2pCO0lBREMsS0FBSyxFQUFFO3lEQUNTO0FBS2pCO0lBREMsS0FBSyxFQUFFOzREQUNTO0FBR2pCO0lBREMsS0FBSyxFQUFFO3VEQUNjO0FBS3RCO0lBREMsTUFBTSxFQUFFO3dEQUN1QztBQUdoRDtJQURDLE1BQU0sRUFBRTs2REFDNEM7QUEvQzFDLHVCQUF1QjtJQVZuQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUscUJBQXFCO1FBQy9CLCt3U0FBK0M7UUFFL0MsU0FBUyxFQUFFLENBQUM7Z0JBQ1YsT0FBTyxFQUFFLGlCQUFpQjtnQkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx5QkFBdUIsQ0FBQztnQkFDdEQsS0FBSyxFQUFFLElBQUk7YUFDWixFQUFDLHVCQUF1QixDQUFDOztLQUMzQixDQUFDO0dBQ1csdUJBQXVCLENBb1ZuQztTQXBWWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlckNvbnRlbnRJbml0LCBDaGFuZ2VEZXRlY3RvclJlZiwgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQ29tbWVudEJveENvbXBvbmVudCB9IGZyb20gJy4vY29tbWVudC1ib3gvY29tbWVudC1ib3guY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29tbWVudHNTZXJ2aWNlIH0gZnJvbSAnLi9jb21tZW50LXNlY3Rpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IENvbW1lbnRVc2VyU3RhdHMgfSBmcm9tICcuL21vZGVscy9jb21tZW50LXVzZXItc3RhdHMnO1xyXG5pbXBvcnQgeyBPcmRlclBpcGUgfSBmcm9tICduZ3gtb3JkZXItcGlwZSc7XHJcbmltcG9ydCB7IGNvbmZpZyB9IGZyb20gJ3Byb2Nlc3MnO1xyXG5pbXBvcnQgIGRlYm91bmNlICBmcm9tICdsb2Rhc2gtZXMvZGVib3VuY2UnO1xyXG5pbXBvcnQgeyBOb3RlIH0gZnJvbSAnLi9tb2RlbHMvbm90ZSc7XHJcbmltcG9ydCB7IFRpbWVhZ29EZWZhdWx0Rm9ybWF0dGVyIH0gZnJvbSAnbmd4LXRpbWVhZ28nO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBDb21tZW50Q29uZmlnIHtcclxuICB1cmxQcmVmaXg6IHN0cmluZzsgICAgLy8gdGhlIHByZWZpeCB3aWxsIGJlIGFwcGVuZGVkIGJlZm9yZSBhbGwgdGhlIGNvbW1lbnQgZW5kcG9pbnRzLlxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3dycy1jb21tZW50LXNlY3Rpb24nLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50LXNlY3Rpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1lbnQtc2VjdGlvbi5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW3tcclxuICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gQ29tbWVudFNlY3Rpb25Db21wb25lbnQpLFxyXG4gICAgbXVsdGk6IHRydWVcclxuICB9LFRpbWVhZ29EZWZhdWx0Rm9ybWF0dGVyXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudFNlY3Rpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyQ29udGVudEluaXQsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuICBjb21tZW50Q29udGVudCA9ICcnO1xyXG4gIEBJbnB1dCgpIGZyb3plbiA9IGZhbHNlO1xyXG5cclxuICBAVmlld0NoaWxkKCdtYWluQ29tbWVudEJveCcsIHtzdGF0aWM6IGZhbHNlfSlcclxuICBjb21tZW50Qm94OiBDb21tZW50Qm94Q29tcG9uZW50O1xyXG5cclxuICBAVmlld0NoaWxkKCdjb21tZW50U2VjdGlvbldyYXBwZXInLCB7c3RhdGljOiBmYWxzZX0pIGNvbW1lbnRTZWN0aW9uV3JhcHBlcjogRWxlbWVudFJlZjtcclxuICBASW5wdXQoKSByZWFkT25seSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGN1cnJlbnRVc2VyXHJcbiAgZGlzcGxheUNvbW1lbnQ6IE5vdGU7XHJcbiAgbG9hZGluZzogYm9vbGVhbiA9IHRydWU7XHJcbiAgbm90ZVR5cGVMb2FkaW5nOiBib29sZWFuID0gdHJ1ZTtcclxuICBjb21tZW50VXNlclN0YXRzOiBDb21tZW50VXNlclN0YXRzO1xyXG4gIGNvbW1lbnRMaXN0OiBOb3RlW10gPSBbXTtcclxuICBpc0xhc3RSZWFkVXBkYXRlZCA9IGZhbHNlO1xyXG4gIG5vdGVzOiBOb3RlW10gPSBbXTtcclxuICBjb21tZW50c1RyZWU6IE5vdGVbXSA9IFtdO1xyXG4gIG1lbnRpb25EZW5vdGF0aW9uOiBhbnlbXSA9IFtdO1xyXG4gIHNob3c6IGJvb2xlYW4gPSBmYWxzZTtcclxuICByZWFkTm90ZTogYm9vbGVhbiA9IHRydWU7XHJcbiAgbm90ZVR5cGVEYXRhOiBhbnk7XHJcbiAgbG9hZENvbW1lbnRVc2VyU3RhdHM6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIG5vdGVUeXBlOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgZW50aXR5SWQ6IHN0cmluZztcclxuXHJcbiAgQElucHV0KClcclxuICBjbGllbnRJZDogc3RyaW5nO1xyXG5cclxuICBjb21tZW50TWFwID0ge307XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc3VnZ2VzdGlvbnMgPSBbXTtcclxuXHJcbiAgQElucHV0KClcclxuICBjb25maWc6IENvbW1lbnRDb25maWc7ICAvLyBDb21tZW50IGNvbmZpZy4gUmVxdWlyZWQuXHJcblxyXG5cclxuXHJcbiAgQE91dHB1dCgpXHJcbiAgc3VnZ2VzdDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIEBPdXRwdXQoKVxyXG4gIG1lbnRpb25DbGljazogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIC8vIGNiayA9IGRlYm91bmNlKCgpID0+IHRoaXMuaW52aWV3LCAzMDApO1xyXG4gIC8vIGNiayA9ICAoKSA9PiB0aGlzLmludmlldygpO1xyXG4gIHNob3dSZXNvbHZlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBjb21tZW50c1NlcnZpY2U6IENvbW1lbnRzU2VydmljZSxcclxuICAgIHByaXZhdGUgb3JkZXJTZXJ2aWNlOiBPcmRlclBpcGUsXHJcbiAgICBwcml2YXRlIGNkcjogQ2hhbmdlRGV0ZWN0b3JSZWYpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIC8vIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLHRoaXMuY2JrKTtcclxuICAgIC8vIHRoaXMuZ2V0Tm90ZVR5cGUoKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZUNvbW1lbnRDb250ZW50KG5ld1ZhbDogc3RyaW5nKSB7XHJcbiAgICB0aGlzLndyaXRlVmFsdWUobmV3VmFsKTtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcclxuICAgIHRoaXMuZ2V0Tm90ZXMoKTtcclxuICAgIHRoaXMuZ2V0Tm90ZVR5cGUoKTtcclxuICB9XHJcblxyXG4gIGdldE5vdGVzKCkge1xyXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuICAgIHRoaXMuY29tbWVudHNUcmVlID0gW107XHJcblxyXG4gICAgbGV0IHN0YXR1cyA9ICdBQ1RJVkUnO1xyXG4gICAgaWYodGhpcy5zaG93UmVzb2x2ZWQpIHtcclxuICAgICAgc3RhdHVzID0gJ1JFU09MVkVEJztcclxuICAgIH1cclxuICAgICAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5nZXROb3Rlcyh7Y29uZmlnOiB0aGlzLmNvbmZpZywgbm90ZVR5cGU6IHRoaXMubm90ZVR5cGUsIGVudGl0eUlkOiB0aGlzLmVudGl0eUlkLCBjbGllbnRJZCA6IHRoaXMuY2xpZW50SWQsIHN0YXR1czogc3RhdHVzfSlcclxuICAgICAgICAgIC5zdWJzY3JpYmUoYWNjZXNzT2JqID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ub3RlcyA9IGFjY2Vzc09iai5kYXRhO1xyXG4gICAgICAgICAgICBjb25zdCBjb21tZW50U2VjdGlvbiA9IGFjY2Vzc09iai5kYXRhO1xyXG4gICAgICAgICAgICBsZXQgc29yZEJ5ID0gJ2NyZWF0aW9uRGF0ZSc7XHJcbiAgICAgICAgICAgIGlmKHN0YXR1cyA9PSAnUkVTT0xWRUQnKSB7XHJcbiAgICAgICAgICAgICAgc29yZEJ5ID0gJ3Jlc29sdmVkQXQnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbW1lbnRzID0gdGhpcy5vcmRlclNlcnZpY2UudHJhbnNmb3JtKGNvbW1lbnRTZWN0aW9uLCBzb3JkQnkpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5jb21tZW50TGlzdCA9IGNvbW1lbnRzO1xyXG4gICAgICAgICAgICBjb21tZW50cy5mb3JFYWNoKGNvbW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuY29tbWVudE1hcFtjb21tZW50LnV1aWRdID0gY29tbWVudDtcclxuICAgICAgICAgICAgICBjb21tZW50LnJlcGxpZXMgPSBbXTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb21tZW50cy5mb3JFYWNoKGNvbW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChjb21tZW50LnJvb3ROb3RlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRNYXBbY29tbWVudC5yb290Tm90ZS51dWlkXS5yZXBsaWVzLnB1c2goY29tbWVudCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29tbWVudHNUcmVlLnB1c2goY29tbWVudCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMYXN0UmVhZCgpO1xyXG4gICAgICAgICAgfSxlcnJvciA9PntcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldE5vdGVUeXBlKCkge1xyXG4gICAgdGhpcy5ub3RlVHlwZUxvYWRpbmcgPSB0cnVlO1xyXG4gICAgdGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0Tm90ZVR5cGUoeyBjb25maWcgOiB0aGlzLmNvbmZpZyAsbm90ZVR5cGUgOiB0aGlzLm5vdGVUeXBlLCBjbGllbnRJZCA6IHRoaXMuY2xpZW50SWR9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLm5vdGVUeXBlRGF0YSA9IHJlc3BvbnNlPy5kYXRhO1xyXG4gICAgICBjb25zdCBjaGFyYWN0ZXJzID0gcmVzcG9uc2U/LmRhdGE/Lm5vdGVUeXBlQ29uZmlnPy5tZW50aW9uQ2hhcmFjdGVycztcclxuICAgICAgdGhpcy5tZW50aW9uRGVub3RhdGlvbiA9IGNoYXJhY3RlcnMuc3BsaXQoJywnKTtcclxuICAgICAgdGhpcy5ub3RlVHlwZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICB9LGVycm9yID0+e1xyXG4gICAgICB0aGlzLm5vdGVUeXBlTG9hZGluZyA9IGZhbHNlO1xyXG4gICAgIH0pO1xyXG4gIH1cclxuICBuZ0FmdGVyVmlld0NoZWNrZWQoKSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZygnbWVudGlvbicsIHRoaXMuY29tbWVudFNlY3Rpb25XcmFwcGVyLm5hdGl2ZUVsZW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnbWVudGlvbicpKTtcclxuICAgIC8vIEFycmF5LmZyb20odGhpcy5jb21tZW50U2VjdGlvbldyYXBwZXIubmF0aXZlRWxlbWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdtZW50aW9uJykpXHJcbiAgICAvLyAgIC5mb3JFYWNoKGVsZW0gPT4ge1xyXG4gICAgLy8gICAgIGNvbnN0IG1lbnRpb25DaGFyID0gKDxhbnk+ZWxlbSkuZ2V0QXR0cmlidXRlKCdkYXRhLWRlbm90YXRpb24tY2hhcicpO1xyXG4gICAgLy8gICAgIGNvbnN0IGlkID0gKDxhbnk+ZWxlbSkuZ2V0QXR0cmlidXRlKCdkYXRhLWlkJyk7XHJcbiAgICAvLyAgICAgY29uc3QgdmFsdWUgPSAoPGFueT5lbGVtKS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdmFsdWUnKTtcclxuICAgIC8vICAgICBjb25zdCBlbGVtUmVmID0gZWxlbTtcclxuICAgIC8vICAgICAoPGFueT5lbGVtKS5vbmNsaWNrID0gKGV2ZW50KSA9PiB7XHJcbiAgICAvLyAgICAgICB0aGlzLm1lbnRpb25DbGljay5lbWl0KHttZW50aW9uQ2hhciwgaWQsIHZhbHVlLCBlbGVtUmVmLCBldmVudH0pO1xyXG4gICAgLy8gICB9O1xyXG4gICAgLy8gICBjb25zb2xlLmxvZygnbGlzdGVybmVyIHNldCBvbiAnLCBlbGVtKTtcclxuICAgIC8vIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZSgpIHtcclxuXHJcbiAgfVxyXG5cclxuICBzZXRTdGF0c09uVmlldygpIHtcclxuICAgIC8vIHRoaXMubG9hZENvbW1lbnRVc2VyU3RhdHMgPSB0cnVlO1xyXG4gICAgLy8gaWYoKVxyXG4gICAgLy8gdGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0VXNlclN0YXRzKHtjb25maWc6IHRoaXMuY29uZmlnLCBub3RlVHlwZSA6IHRoaXMubm90ZVR5cGVEYXRhLnV1aWQgLGVudGl0eUlkIDogdGhpcy5lbnRpdHlJZH0pXHJcbiAgICAvLyAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAvLyAgICAgdGhpcy5jb21tZW50VXNlclN0YXRzID0gZGF0YS5kYXRhO1xyXG4gICAgLy8gICAgIGNvbnN0IGNvbW1lbnRMaXN0ID0gdGhpcy5jb21tZW50TGlzdC5maWx0ZXIoYyA9PiBjLnVzZXJJZCAhPT0gdGhpcy5jdXJyZW50VXNlci5pZCk7XHJcbiAgICAvLyAgICAgY29uc3QgY29tbWVudCA9IGNvbW1lbnRMaXN0W2NvbW1lbnRMaXN0Lmxlbmd0aCAtIHRoaXMuY29tbWVudFVzZXJTdGF0cy51bnJlYWRDb3VudF07XHJcbiAgICAvLyAgICAgdGhpcy5sb2FkQ29tbWVudFVzZXJTdGF0cyA9IGZhbHNlO1xyXG4gICAgLy8gICAgIHRoaXMudXBkYXRlTGFzdFJlYWQoKTtcclxuICAgIC8vICAgICBpZiAoY29tbWVudCkge1xyXG4gICAgLy8gICAgICAgY29tbWVudC5maXJzdFVucmVhZCA9IHRydWU7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICB9KTtcclxuICB9XHJcblxyXG4gIHNldFJlcGx5VG8oY29tbWVudDogTm90ZSkge1xyXG4gICAgY29tbWVudC5yZXBseWluZyA9IHRydWU7XHJcbiAgICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuc2Nyb2xsKCdjb21tZW50X3F1aWxsXycgKyBjb21tZW50LnV1aWQpLCAxNTApO1xyXG4gIH1cclxuXHJcbiAgcmVzb2x2ZShjb21tZW50KSB7XHJcbiAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLnJlc29sdmVDb21tZW50KHsgY29uZmlnIDogdGhpcy5jb25maWcsIG5vdGVUeXBlSWQgOiBjb21tZW50Lm5vdGVUeXBlLmlkICxlbnRpdHlJZCA6IHRoaXMuZW50aXR5SWQsIG5vdGVVVUlEIDogY29tbWVudC51dWlkIH0pLnN1YnNjcmliZSggZGF0YSA9PiB7XHJcbiAgICB0aGlzLmNvbW1lbnRzVHJlZSA9ICB0aGlzLmNvbW1lbnRzVHJlZS5maWx0ZXIoKG5vdGUpID0+IG5vdGUudXVpZCAhPSBjb21tZW50LnV1aWQpO1xyXG4gICB9KTtcclxuICAgdGhpcy5jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnNjcm9sbCgnY29tbWVudF9xdWlsbF8nICsgY29tbWVudC51dWlkKSwgMTUwKTtcclxuICB9XHJcblxyXG4gIHB1c2hUb0NvbW1lbnRMaXN0KGFkZGVkQ29tbWVudDogTm90ZSwgY29tbWVudD86IE5vdGUpIHtcclxuICAgIHRoaXMuY29tbWVudE1hcFthZGRlZENvbW1lbnQudXVpZF0gPSBhZGRlZENvbW1lbnQ7XHJcbiAgICB0aGlzLmNvbW1lbnRMaXN0LnB1c2goYWRkZWRDb21tZW50KTtcclxuICAgIGFkZGVkQ29tbWVudC5yZXBsaWVzID0gW107XHJcbiAgICBpZiAoY29tbWVudCkge1xyXG4gICAgICBjb21tZW50LnJlcGx5aW5nID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoYWRkZWRDb21tZW50LnJvb3ROb3RlKSB7XHJcbiAgICAgIHRoaXMuY29tbWVudE1hcFtjb21tZW50LnV1aWRdLnJlcGxpZXMucHVzaChhZGRlZENvbW1lbnQpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jb21tZW50c1RyZWUucHVzaChhZGRlZENvbW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuY2RyLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMuc2Nyb2xsVG9Db21tZW50KGFkZGVkQ29tbWVudCk7XHJcbiAgfVxyXG5cclxuICBwdXNoV2hlbkVkaXQoZWRpdE5vdGU6IE5vdGUsY29tbWVudD86IE5vdGUpIHtcclxuICAgIHRoaXMuY29tbWVudHNUcmVlLmZvckVhY2goY29tbWVudCA9PntcclxuICAgICAgaWYoZWRpdE5vdGUudXVpZCA9PSBjb21tZW50LnV1aWQpIHtcclxuICAgICAgICBjb21tZW50LmNvbnRlbnQgPSBlZGl0Tm90ZS5jb250ZW50O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICBjb21tZW50LnJlcGxpZXMuZm9yRWFjaChyZXBseSA9PntcclxuICAgICAgICBpZihlZGl0Tm90ZS51dWlkID09IHJlcGx5LnV1aWQpXHJcbiAgICAgICAgcmVwbHkuY29udGVudCA9IGVkaXROb3RlLmNvbnRlbnQ7XHJcbiAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVUb0NvbW1lbnRMaXN0KGRlbGV0ZWROb3RlOiBOb3RlKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzVHJlZSA9IHRoaXMuY29tbWVudHNUcmVlLmZpbHRlcihub3RlID0+IG5vdGUudXVpZCAhPSBkZWxldGVkTm90ZS51dWlkKTtcclxuICAgIHRoaXMuY29tbWVudHNUcmVlLmZvckVhY2goY29tbWVudCA9PntcclxuICAgICAgIGNvbW1lbnQucmVwbGllcyA9IGNvbW1lbnQucmVwbGllcy5maWx0ZXIoeCA9PiB4LnV1aWQgIT0gZGVsZXRlZE5vdGUudXVpZCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNhbmNlbFJlcGx5KGNvbW1lbnQpIHtcclxuICAgIGNvbW1lbnQucmVwbHlpbmcgPSBmYWxzZTtcclxuICB9XHJcblxyXG4gIHNjcm9sbFRvQ29tbWVudChjb21tZW50OiBOb3RlKSB7XHJcbiAgICB0aGlzLnNjcm9sbCh0aGlzLmdldENsYXNzTmFtZShjb21tZW50KSwgKCkgPT4ge1xyXG4gICAgICBjb25zdCByZXBsaWVkQ29tbWVudCA9IHRoaXMuY29tbWVudExpc3QuZmlsdGVyKGNvbW1lbnRPYmplY3QgPT4gY29tbWVudC51dWlkID09IGNvbW1lbnRPYmplY3QudXVpZClbMF07XHJcbiAgICAgIGlmIChyZXBsaWVkQ29tbWVudCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0Q29tbWVudChyZXBsaWVkQ29tbWVudCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2Nyb2xsKGNsYXNzTmFtZTogc3RyaW5nLCBjYWxsYmFjaz8pIHtcclxuICAgIGNvbnN0IGNsYXNzRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoY2xhc3NOYW1lKTtcclxuICAgIGlmIChjbGFzc0VsZW1lbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICBjbGFzc0VsZW1lbnRbMF0uc2Nyb2xsSW50b1ZpZXcoeyBiZWhhdmlvcjogJ3Ntb290aCcgLCBibG9jazogJ2NlbnRlcid9KTtcclxuICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0Q2xhc3NOYW1lKGNvbW1lbnQ6IE5vdGUpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuICdub3RlXycrIGNvbW1lbnQudXVpZDtcclxuICB9XHJcblxyXG4gIHNlbGVjdENvbW1lbnQoY29tbWVudDogTm90ZSkge1xyXG4gICAgY29tbWVudFsnc2VsZWN0ZWQnXSA9IHRydWU7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IGNvbW1lbnRbJ3NlbGVjdGVkJ10gPSBmYWxzZSwgMzAwMCk7XHJcbiAgfVxyXG5cclxuICBnZXQgbWFpbkNvbW1lbnRCb3goKSB7XHJcbiAgICByZXR1cm4gdGhpcy5jb21tZW50Qm94O1xyXG4gIH1cclxuXHJcbiAgc2V0Rm9jdXMoZWRpdG9yLCBib3gpIHtcclxuICAgIGJveC5lZGl0b3IuZ2V0UXVpbGwoKS5mb2N1cygpO1xyXG4gIH1cclxuXHJcbiAgZm9ybWF0RGF0ZShkYXRlX3N0cjogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZShkYXRlX3N0cik7XHJcbiAgICByZXR1cm4gZGF0ZS50b0xvY2FsZVN0cmluZygpO1xyXG4gIH1cclxuXHJcbiAgb25TdWdnZXN0KGV2ZW50OiBhbnkpIHtcclxuICAgIHRoaXMuc3VnZ2VzdC5lbWl0KGV2ZW50KTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZUxhc3RSZWFkKCkge1xyXG4gICAgaWYgKCF0aGlzLmlzTGFzdFJlYWRVcGRhdGVkKSB7XHJcbiAgICAgIGNvbnN0IGNvbW1lbnRzID0gdGhpcy5jb21tZW50TGlzdC5maWx0ZXIoYyA9PiBjLnVzZXJJZCAhPT0gdGhpcy5jdXJyZW50VXNlci5pZCk7XHJcbiAgICAgIGNvbnN0IGxhc3RDb21tZW50ID0gY29tbWVudHNbY29tbWVudHMubGVuZ3RoIC0gMV07XHJcbiAgICAgIGlmIChsYXN0Q29tbWVudCkge1xyXG4gICAgICAgdGhpcy5jb21tZW50c1NlcnZpY2UudXBkYXRlTGFzdFJlYWQoeyBjb25maWcgOiB0aGlzLmNvbmZpZywgbm90ZVR5cGVJZCA6IHRoaXMubm90ZVR5cGVEYXRhLmlkICxlbnRpdHlJZCA6IHRoaXMuZW50aXR5SWQsIGNvbW1lbnRVVUlEOiBsYXN0Q29tbWVudC51dWlkfSkuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMuaXNMYXN0UmVhZFVwZGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4vLyAgIGlzRWxlbWVudEluVmlld3BvcnQgKGVsKSB7XHJcbi8vICAgICBjb25zdCByZWN0ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblxyXG4vLyAgICAgcmV0dXJuIChcclxuLy8gICAgICAgICByZWN0LnRvcCA+PSAwICYmXHJcbi8vICAgICAgICAgcmVjdC5sZWZ0ID49IDAgJiZcclxuLy8gICAgICAgICByZWN0LmJvdHRvbSA8PSAod2luZG93LmlubmVySGVpZ2h0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQpICYmIC8qIG9yICQod2luZG93KS5oZWlnaHQoKSAqL1xyXG4vLyAgICAgICAgIHJlY3QucmlnaHQgPD0gKHdpbmRvdy5pbm5lcldpZHRoIHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCkgLyogb3IgJCh3aW5kb3cpLndpZHRoKCkgKi9cclxuLy8gICAgICk7XHJcbi8vIH1cclxuXHJcblxyXG4gIGdldCB2YWx1ZSgpOiBzdHJpbmcgeyByZXR1cm4gdGhpcy5jb21tZW50Q29udGVudDsgfVxyXG5cclxuICBzZXQgdmFsdWUodjogc3RyaW5nKSB7XHJcbiAgICBpZiAodiAhPT0gdGhpcy5jb21tZW50Q29udGVudCkge1xyXG4gICAgICB0aGlzLmNvbW1lbnRDb250ZW50ID0gdjtcclxuICAgICAgdGhpcy5vbkNoYW5nZSh2KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5jb21tZW50Q29udGVudCA9IHZhbHVlO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZSA9IChfKSA9PiB7fTtcclxuICBvblRvdWNoZWQgPSAoKSA9PiB7fTtcclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiAoXzogYW55KSA9PiB2b2lkKTogdm9pZCB7IHRoaXMub25DaGFuZ2UgPSBmbjsgfVxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuOiAoKSA9PiB2b2lkKTogdm9pZCB7IHRoaXMub25Ub3VjaGVkID0gZm47IH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAvLyB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5jYmspO1xyXG4gIH1cclxuXHJcbiAgc2hvd1Jlc29sdmVkTm90ZSgpIHtcclxuICAgIHRoaXMuc2hvd1Jlc29sdmVkID0gICF0aGlzLnNob3dSZXNvbHZlZDtcclxuICB9XHJcblxyXG4gIGNoYW5nZVN3aXRjaCgpIHtcclxuICAgIHRoaXMuc2hvd1Jlc29sdmVkID0gIXRoaXMuc2hvd1Jlc29sdmVkO1xyXG4gICAgdGhpcy5nZXROb3RlcygpO1xyXG4gIH1cclxuXHJcbiAgY2hlY2tRdWlsbChyZXBseVF1aWxsKSB7XHJcbiAgfVxyXG5cclxuICBzdHJpcChodG1sKXtcclxuICAgIC8vIHRoaXMuc2hvd0xlc3NDb250ZW50KGh0bWwpO1xyXG4gICAgbGV0IGRvYyA9IG5ldyBET01QYXJzZXIoKS5wYXJzZUZyb21TdHJpbmcoaHRtbCwgJ3RleHQvaHRtbCcpO1xyXG4gICAgcmV0dXJuIGRvYy5ib2R5LnRleHRDb250ZW50IHx8IFwiXCI7XHJcblxyXG4gfVxyXG5cclxuXHJcbiBkZWxldGVOb3RlKG5vdGU6IE5vdGUpIHtcclxuICB0aGlzLmNvbW1lbnRzU2VydmljZS5kZWxldGVOb3RlKHtjb25maWc6IHRoaXMuY29uZmlnLGNvbW1lbnRVVUlEOiBub3RlLnV1aWR9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT57XHJcbiAgICBjb25zdCBjb21tZW50ID0gcmVzcG9uc2UuZGF0YTtcclxuICAgIHRoaXMuZGVsZXRlVG9Db21tZW50TGlzdChjb21tZW50KTtcclxuICB9KTtcclxuIH1cclxuXHJcbiBlZGl0Tm90ZShub3RlOiBOb3RlKSB7XHJcbiAgbm90ZS51cGRhdGVNb2RlID0gIW5vdGUudXBkYXRlTW9kZTtcclxufVxyXG5cclxuY29uZmlybURlbGV0ZShub3RlOiBOb3RlKSB7XHJcbiAgbm90ZS5jb25maXJtRGVsZXRlID0gIW5vdGUuY29uZmlybURlbGV0ZTtcclxufVxyXG5cclxufVxyXG4iXX0=