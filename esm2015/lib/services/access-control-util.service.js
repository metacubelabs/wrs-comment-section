import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AccessControl } from '../models/access-control';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
let AccessControlUtilService = class AccessControlUtilService {
    constructor(http) {
        this.http = http;
    }
    httpGet(url, params) {
        return this.http.get(url, { params })
            .pipe(map(obj => new AccessControl(obj)));
    }
    httpPost(url, params) {
        return this.http.post(url, params)
            .pipe(map(obj => new AccessControl(obj)));
    }
    httpPut(url, params) {
        return this.http.put(url, params)
            .pipe(map(obj => new AccessControl(obj)));
    }
    httpDelete(url, params) {
        return this.http.delete(url, { params })
            .pipe(map(obj => new AccessControl(obj)));
    }
};
AccessControlUtilService.ctorParameters = () => [
    { type: HttpClient }
];
AccessControlUtilService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AccessControlUtilService_Factory() { return new AccessControlUtilService(i0.ɵɵinject(i1.HttpClient)); }, token: AccessControlUtilService, providedIn: "root" });
AccessControlUtilService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AccessControlUtilService);
export { AccessControlUtilService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wtdXRpbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbWVudC1zZWN0aW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2FjY2Vzcy1jb250cm9sLXV0aWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXJDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7O0FBS3pELElBQWEsd0JBQXdCLEdBQXJDLE1BQWEsd0JBQXdCO0lBRW5DLFlBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFBSSxDQUFDO0lBRXpDLE9BQU8sQ0FBSSxHQUFXLEVBQUUsTUFBWTtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFtQixHQUFHLEVBQUUsRUFBQyxNQUFNLEVBQUMsQ0FBQzthQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxhQUFhLENBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxRQUFRLENBQUksR0FBVyxFQUFFLE1BQVk7UUFDbkMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBbUIsR0FBRyxFQUFFLE1BQU0sQ0FBQzthQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxhQUFhLENBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxPQUFPLENBQUksR0FBVyxFQUFFLE1BQVk7UUFDbEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBbUIsR0FBRyxFQUFFLE1BQU0sQ0FBQzthQUNoRCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxhQUFhLENBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxVQUFVLENBQUksR0FBVyxFQUFFLE1BQVk7UUFDckMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBbUIsR0FBRyxFQUFFLEVBQUMsTUFBTSxFQUFDLENBQUM7YUFDckQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksYUFBYSxDQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDO0NBQ0YsQ0FBQTs7WUFyQjJCLFVBQVU7OztBQUZ6Qix3QkFBd0I7SUFIcEMsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLHdCQUF3QixDQXVCcEM7U0F2Qlksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWNjZXNzQ29udHJvbCB9IGZyb20gJy4uL21vZGVscy9hY2Nlc3MtY29udHJvbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBY2Nlc3NDb250cm9sVXRpbFNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICBodHRwR2V0PFQ+KHVybDogc3RyaW5nLCBwYXJhbXM/OiBhbnkpOiBPYnNlcnZhYmxlPEFjY2Vzc0NvbnRyb2w8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PEFjY2Vzc0NvbnRyb2w8VD4+KHVybCwge3BhcmFtc30pXHJcbiAgICAgIC5waXBlKG1hcChvYmogPT4gbmV3IEFjY2Vzc0NvbnRyb2w8VD4ob2JqKSkpO1xyXG4gIH1cclxuXHJcbiAgaHR0cFBvc3Q8VD4odXJsOiBzdHJpbmcsIHBhcmFtcz86IGFueSk6IE9ic2VydmFibGU8QWNjZXNzQ29udHJvbDxUPj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PEFjY2Vzc0NvbnRyb2w8VD4+KHVybCwgcGFyYW1zKVxyXG4gICAgICAucGlwZShtYXAob2JqID0+IG5ldyBBY2Nlc3NDb250cm9sPFQ+KG9iaikpKTtcclxuICB9XHJcblxyXG4gIGh0dHBQdXQ8VD4odXJsOiBzdHJpbmcsIHBhcmFtcz86IGFueSk6IE9ic2VydmFibGU8QWNjZXNzQ29udHJvbDxUPj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQ8QWNjZXNzQ29udHJvbDxUPj4odXJsLCBwYXJhbXMpXHJcbiAgICAgIC5waXBlKG1hcChvYmogPT4gbmV3IEFjY2Vzc0NvbnRyb2w8VD4ob2JqKSkpO1xyXG4gIH1cclxuXHJcbiAgaHR0cERlbGV0ZTxUPih1cmw6IHN0cmluZywgcGFyYW1zPzogYW55KTogT2JzZXJ2YWJsZTxBY2Nlc3NDb250cm9sPFQ+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLmRlbGV0ZTxBY2Nlc3NDb250cm9sPFQ+Pih1cmwsIHtwYXJhbXN9KVxyXG4gICAgICAucGlwZShtYXAob2JqID0+IG5ldyBBY2Nlc3NDb250cm9sPFQ+KG9iaikpKTtcclxuICB9XHJcbn1cclxuIl19