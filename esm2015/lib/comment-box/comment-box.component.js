var CommentBoxComponent_1;
import { __decorate } from "tslib";
import { Component, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommentsService } from '../comment-section.service';
import { QuillEditorComponent } from 'ngx-quill';
import { finalize } from 'rxjs/operators';
import 'quill-mention';
let CommentBoxComponent = CommentBoxComponent_1 = class CommentBoxComponent {
    constructor(commentsService) {
        this.commentsService = commentsService;
        this.commentContent = '';
        this.mentionDenotation = [];
        this.sent = new EventEmitter();
        this.cancel = new EventEmitter();
        this.init = new EventEmitter();
        this.onContentChange = new EventEmitter();
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.modules = null;
        this.user = JSON.parse(localStorage.getItem('user'));
        this.formAction = false;
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    ngOnInit() {
        // this.getNoteType();
        this.modules = {
            mention: {
                allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
                // TODO: User has to provide the denotation characters
                mentionDenotationChars: this.mentionDenotation,
                onSelect: (item, insertItem) => {
                    const editor = this.editor.quillEditor;
                    insertItem(item);
                    // necessary because quill-mention triggers changes as 'api' instead of 'user'
                    editor.insertText(editor.getLength() - 1, '', 'user');
                },
                source: (searchTerm, renderList, mentionChar) => {
                    const event = {
                        query: searchTerm,
                        mentionChar: mentionChar,
                        renderList: renderList
                    };
                    // TODO: Document it for users
                    this.suggest.emit(event);
                },
                // TODO: Provide a map of denotation characters and functions
                renderItem: (item, searchTerm) => {
                    return item.value;
                }
            },
            toolbar: []
        };
    }
    ngAfterViewInit() {
        // this.user['name'] = this.user['full_name'];
    }
    getNoteType() {
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(response => {
            var _a, _b;
            const characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            this.mentionDenotation = characters.split(',');
        });
    }
    updateContent() {
        this.writeValue(this.commentContent);
    }
    doComment() {
        if (!this.commentContent.trim()) {
            return false;
        }
        this.formAction = true;
        if (this.rootNote) {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, rootNote: { uuid: this.rootNote.uuid }, clientId: this.clientId })
                .pipe(finalize(() => this.resetState()))
                .subscribe(accessObj => {
                const comment = accessObj.data;
                this.sent.emit(comment);
            });
        }
        else {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId })
                .pipe(finalize(() => this.resetState()))
                .subscribe(accessObj => {
                const comment = accessObj.data;
                comment['user'] = this.user;
                this.sent.emit(comment);
            });
        }
    }
    cancelComment() {
        this.cancel.emit(true);
    }
    resetState() {
        this.commentContent = null;
        this.formAction = false;
    }
    inititalized(editor) {
        this.init.emit(editor);
    }
    get value() {
        return this.commentContent;
    }
    set value(v) {
        if (v !== this.commentContent) {
            this.commentContent = v;
            this.onChange(v);
        }
    }
    writeValue(value) {
        this.commentContent = value;
        this.onContentChange.emit(this.commentContent);
        this.onChange(value);
    }
    updateNote(note) {
        this.commentsService.updateNote({ config: this.config, comment: this.commentContent, commentUUID: note.uuid }).subscribe(response => {
            const comment = response.data;
            note.updateMode = !note.updateMode;
            this.sent.emit(comment);
        });
    }
    editNote(note) {
        note.updateMode = !note.updateMode;
    }
    registerOnChange(fn) { this.onChange = fn; }
    registerOnTouched(fn) { this.onTouched = fn; }
};
CommentBoxComponent.ctorParameters = () => [
    { type: CommentsService }
];
__decorate([
    Input()
], CommentBoxComponent.prototype, "commentContent", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "note", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "rootNote", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "noteType", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "entityId", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "clientId", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "mentionDenotation", void 0);
__decorate([
    ViewChild(QuillEditorComponent, { static: false })
], CommentBoxComponent.prototype, "editor", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "sent", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "cancel", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "init", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "onContentChange", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "suggestions", void 0);
__decorate([
    Input()
], CommentBoxComponent.prototype, "config", void 0);
__decorate([
    Output()
], CommentBoxComponent.prototype, "suggest", void 0);
CommentBoxComponent = CommentBoxComponent_1 = __decorate([
    Component({
        selector: 'wrs-comment-box',
        template: "<div class=\"first-comment-box\">\r\n  <quill-editor  #editor\r\n    [(ngModel)]=\"commentContent\" name=\"commentContent\" [style]=\"{'height':'150px'}\" [disabled]=\"formAction\"\r\n    placeholder=\"Enter note\" (onInit)=\"inititalized($event)\" [modules]=\"modules\"\r\n    (onContentChanged)=\"updateContent()\"></quill-editor>\r\n</div>\r\n<div class=\"text-right text-end mt-2\">\r\n  <button  type=\"button\" class=\"icon-button btn-light\" *ngIf=\"rootNote\"\r\n    (click)=\"cancelComment()\" [disabled]=\"formAction\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"rootNote\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"!rootNote && !note?.updateMode\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>\r\n<div class=\"text-right text-end mt-2\" *ngIf = \"note?.updateMode\">\r\n  <button  type=\"button\" (click) = \"editNote(note)\" class=\"icon-button btn-light\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" (click) = \"updateNote(note)\">\r\n    save\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>",
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => CommentBoxComponent_1),
                multi: true
            }],
        styles: [":host ::ng-deep .ql-toolbar.ql-snow{display:none}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-top:1px solid #c8c8c8}:host ::ng-deep .ql-editor{min-height:50px}:host ::ng-deep .ui-send-comment{background:#177dfb!important;border-color:#177dfb!important;border-radius:26px;font-size:14px;margin:5px;transition:.3s;color:#fff;padding:5px 10px}:host ::ng-deep .ui-send-comment:hover{background:0 0!important;border-color:#177dfb!important;color:#177dfb!important}:host ::ng-deep .ui-send-comment-cancel{background:#177dfb!important}:host ::ng-deep .ql-mention-list-container{width:270px;border:1px solid #f0f0f0;border-radius:4px;background-color:#fff;box-shadow:0 2px 12px 0 rgba(30,30,30,.08);z-index:9001;max-height:150px;overflow-y:scroll}:host ::ng-deep .ql-mention-list{list-style:none;margin:0;padding:0;overflow:hidden}:host ::ng-deep .ql-mention-list-item{cursor:pointer;height:40px;line-height:40px;font-size:14px;padding:0 20px;border-bottom:1px solid #e4e4e4;vertical-align:middle}:host ::ng-deep .ql-mention-list-item.selected{background-color:#d3e1eb;text-decoration:none}:host ::ng-deep .mention{height:24px;width:65px;border-radius:6px;background-color:#177dfb;padding:3px 0;margin-right:2px}:host ::ng-deep .mention>span{margin:0 3px}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-radius:5px}.send-button{background:#177dfb!important;padding:7px 10px;border:none;color:#fff;border-radius:3px;margin-left:10px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.icon-button.button-resolve{background:#177dfb;color:#fff;position:absolute;bottom:30px;right:12px;border-radius:0 14px 14px 0;height:40px}"]
    })
], CommentBoxComponent);
export { CommentBoxComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1ib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbWVudC1zZWN0aW9uLyIsInNvdXJjZXMiOlsibGliL2NvbW1lbnQtYm94L2NvbW1lbnQtYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBaUIsU0FBUyxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckgsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDakQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTFDLE9BQU8sZUFBZSxDQUFDO0FBYXZCLElBQWEsbUJBQW1CLDJCQUFoQyxNQUFhLG1CQUFtQjtJQWlDOUIsWUFBb0IsZUFBZ0M7UUFBaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBL0IzQyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQU1wQixzQkFBaUIsR0FBVSxFQUFFLENBQUM7UUFJN0IsU0FBSSxHQUFHLElBQUksWUFBWSxFQUFRLENBQUM7UUFDaEMsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7UUFDckMsU0FBSSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDL0Isb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBR3BELGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBTWpCLFlBQU8sR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVoRCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBRWYsU0FBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBRWhELGVBQVUsR0FBRyxLQUFLLENBQUM7UUFvSG5CLGFBQVEsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBQ3JCLGNBQVMsR0FBRyxHQUFHLEVBQUUsR0FBRSxDQUFDLENBQUM7SUFsSG1DLENBQUM7SUFFekQsUUFBUTtRQUNOLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsT0FBTyxFQUFFO2dCQUNQLFlBQVksRUFBRSxxQkFBcUI7Z0JBQ25DLHNEQUFzRDtnQkFDdEQsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtnQkFDbEQsUUFBUSxFQUFFLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxFQUFFO29CQUM3QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQW9CLENBQUM7b0JBQ2hELFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDakIsOEVBQThFO29CQUM5RSxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN4RCxDQUFDO2dCQUNELE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLEVBQUU7b0JBQzlDLE1BQU0sS0FBSyxHQUFHO3dCQUNaLEtBQUssRUFBRSxVQUFVO3dCQUNqQixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsVUFBVSxFQUFFLFVBQVU7cUJBQ3ZCLENBQUM7b0JBQ0YsOEJBQThCO29CQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDM0IsQ0FBQztnQkFDRCw2REFBNkQ7Z0JBQzdELFVBQVUsRUFBRSxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsRUFBRTtvQkFDL0IsT0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUNyQixDQUFDO2FBQ0Y7WUFDRCxPQUFPLEVBQUUsRUFBRTtTQUNWLENBQUE7SUFDRCxDQUFDO0lBQ0QsZUFBZTtRQUNiLDhDQUE4QztJQUNoRCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUMsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTs7WUFDaEksTUFBTSxVQUFVLGVBQUcsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLElBQUksMENBQUUsY0FBYywwQ0FBRSxpQkFBaUIsQ0FBQztZQUNyRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELFNBQVM7UUFDUCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUMvQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFFdkIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUcsUUFBUSxFQUFHLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDcE0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztpQkFDdkMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUNyQixNQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQzVKLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7aUJBQ3ZDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDckIsTUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDL0IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsYUFBYTtRQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVELFlBQVksQ0FBQyxNQUFNO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQUksS0FBSyxDQUFDLENBQVM7UUFDakIsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFhO1FBQ3RCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBVTtRQUNuQixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0gsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxRQUFRLENBQUMsSUFBVTtRQUNoQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN0QyxDQUFDO0lBSUYsZ0JBQWdCLENBQUMsRUFBb0IsSUFBVSxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEUsaUJBQWlCLENBQUMsRUFBYyxJQUFVLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztDQUNqRSxDQUFBOztZQXJIc0MsZUFBZTs7QUEvQjNDO0lBQVIsS0FBSyxFQUFFOzJEQUFxQjtBQUNwQjtJQUFSLEtBQUssRUFBRTtpREFBWTtBQUNYO0lBQVIsS0FBSyxFQUFFO3FEQUFnQjtBQUNmO0lBQVIsS0FBSyxFQUFFO3FEQUFrQjtBQUNqQjtJQUFSLEtBQUssRUFBRTtxREFBa0I7QUFDakI7SUFBUixLQUFLLEVBQUU7cURBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFOzhEQUErQjtBQUNXO0lBQWpELFNBQVMsQ0FBQyxvQkFBb0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQzttREFBOEI7QUFHckU7SUFBVCxNQUFNLEVBQUU7aURBQWlDO0FBQ2hDO0lBQVQsTUFBTSxFQUFFO21EQUFzQztBQUNyQztJQUFULE1BQU0sRUFBRTtpREFBZ0M7QUFDL0I7SUFBVCxNQUFNLEVBQUU7NERBQTJDO0FBR3BEO0lBREMsS0FBSyxFQUFFO3dEQUNTO0FBR2pCO0lBREMsS0FBSyxFQUFFO21EQUNjO0FBR3RCO0lBREMsTUFBTSxFQUFFO29EQUN1QztBQXhCckMsbUJBQW1CO0lBVi9CLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxpQkFBaUI7UUFDM0IsaStDQUEyQztRQUUzQyxTQUFTLEVBQUUsQ0FBQztnQkFDVixPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHFCQUFtQixDQUFDO2dCQUNsRCxLQUFLLEVBQUUsSUFBSTthQUNaLENBQUM7O0tBQ0gsQ0FBQztHQUNXLG1CQUFtQixDQXNKL0I7U0F0SlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQ29tbWVudENvbmZpZyB9IGZyb20gJy4uL2NvbW1lbnQtc2VjdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50c1NlcnZpY2UgfSBmcm9tICcuLi9jb21tZW50LXNlY3Rpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFF1aWxsRWRpdG9yQ29tcG9uZW50IH0gZnJvbSAnbmd4LXF1aWxsJztcclxuaW1wb3J0IHsgZmluYWxpemUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCBRdWlsbCBmcm9tICdxdWlsbCc7XHJcbmltcG9ydCAncXVpbGwtbWVudGlvbic7XHJcbmltcG9ydCB7IE5vdGUgfSBmcm9tICcuLi9tb2RlbHMvbm90ZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3dycy1jb21tZW50LWJveCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnQtYm94LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50LWJveC5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW3tcclxuICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gQ29tbWVudEJveENvbXBvbmVudCksXHJcbiAgICBtdWx0aTogdHJ1ZVxyXG4gIH1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50Qm94Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcblxyXG4gIEBJbnB1dCgpIGNvbW1lbnRDb250ZW50ID0gJyc7XHJcbiAgQElucHV0KCkgbm90ZTogTm90ZTtcclxuICBASW5wdXQoKSByb290Tm90ZTogTm90ZTtcclxuICBASW5wdXQoKSBub3RlVHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGVudGl0eUlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgY2xpZW50SWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBtZW50aW9uRGVub3RhdGlvbjogYW55W10gPSBbXTtcclxuICBAVmlld0NoaWxkKFF1aWxsRWRpdG9yQ29tcG9uZW50LCB7c3RhdGljOiBmYWxzZX0pIGVkaXRvcjogUXVpbGxFZGl0b3JDb21wb25lbnQ7XHJcblxyXG5cclxuICBAT3V0cHV0KCkgc2VudCA9IG5ldyBFdmVudEVtaXR0ZXI8Tm90ZT4oKTtcclxuICBAT3V0cHV0KCkgY2FuY2VsID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG4gIEBPdXRwdXQoKSBpbml0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIG9uQ29udGVudENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIHN1Z2dlc3Rpb25zID0gW107XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgY29uZmlnOiBDb21tZW50Q29uZmlnO1xyXG5cclxuICBAT3V0cHV0KClcclxuICBzdWdnZXN0OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgbW9kdWxlcyA9IG51bGw7XHJcblxyXG4gIHVzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyJykpO1xyXG5cclxuICBmb3JtQWN0aW9uID0gZmFsc2U7XHJcbiAgbWVudGlvbkNoYXJzOiBhbnk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2UpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIC8vIHRoaXMuZ2V0Tm90ZVR5cGUoKTtcclxuICAgIHRoaXMubW9kdWxlcyA9IHtcclxuICAgICAgbWVudGlvbjoge1xyXG4gICAgICAgIGFsbG93ZWRDaGFyczogL15bQS1aYS16XFxzw4XDhMOWw6XDpMO2XSokLyxcclxuICAgICAgICAvLyBUT0RPOiBVc2VyIGhhcyB0byBwcm92aWRlIHRoZSBkZW5vdGF0aW9uIGNoYXJhY3RlcnNcclxuICAgICAgICBtZW50aW9uRGVub3RhdGlvbkNoYXJzOiB0aGlzLm1lbnRpb25EZW5vdGF0aW9uLFxyXG4gICAgb25TZWxlY3Q6IChpdGVtLCBpbnNlcnRJdGVtKSA9PiB7XHJcbiAgICAgIGNvbnN0IGVkaXRvciA9IHRoaXMuZWRpdG9yLnF1aWxsRWRpdG9yIGFzIFF1aWxsO1xyXG4gICAgICBpbnNlcnRJdGVtKGl0ZW0pO1xyXG4gICAgICAvLyBuZWNlc3NhcnkgYmVjYXVzZSBxdWlsbC1tZW50aW9uIHRyaWdnZXJzIGNoYW5nZXMgYXMgJ2FwaScgaW5zdGVhZCBvZiAndXNlcidcclxuICAgICAgZWRpdG9yLmluc2VydFRleHQoZWRpdG9yLmdldExlbmd0aCgpIC0gMSwgJycsICd1c2VyJyk7XHJcbiAgICB9LFxyXG4gICAgc291cmNlOiAoc2VhcmNoVGVybSwgcmVuZGVyTGlzdCwgbWVudGlvbkNoYXIpID0+IHtcclxuICAgICAgY29uc3QgZXZlbnQgPSB7XHJcbiAgICAgICAgcXVlcnk6IHNlYXJjaFRlcm0sXHJcbiAgICAgICAgbWVudGlvbkNoYXI6IG1lbnRpb25DaGFyLFxyXG4gICAgICAgIHJlbmRlckxpc3Q6IHJlbmRlckxpc3RcclxuICAgICAgfTtcclxuICAgICAgLy8gVE9ETzogRG9jdW1lbnQgaXQgZm9yIHVzZXJzXHJcbiAgICAgIHRoaXMuc3VnZ2VzdC5lbWl0KGV2ZW50KTtcclxuICAgIH0sXHJcbiAgICAvLyBUT0RPOiBQcm92aWRlIGEgbWFwIG9mIGRlbm90YXRpb24gY2hhcmFjdGVycyBhbmQgZnVuY3Rpb25zXHJcbiAgICByZW5kZXJJdGVtOiAoaXRlbSwgc2VhcmNoVGVybSkgPT4ge1xyXG4gICAgICByZXR1cm4gIGl0ZW0udmFsdWU7XHJcbiAgICB9XHJcbiAgfSxcclxuICB0b29sYmFyOiBbXVxyXG4gIH1cclxuICB9XHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgLy8gdGhpcy51c2VyWyduYW1lJ10gPSB0aGlzLnVzZXJbJ2Z1bGxfbmFtZSddO1xyXG4gIH1cclxuXHJcbiAgZ2V0Tm90ZVR5cGUoKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5nZXROb3RlVHlwZSh7IGNvbmZpZyA6IHRoaXMuY29uZmlnICxub3RlVHlwZSA6IHRoaXMubm90ZVR5cGUsY2xpZW50SWQgOiB0aGlzLmNsaWVudElkfSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgY29uc3QgY2hhcmFjdGVycyA9IHJlc3BvbnNlPy5kYXRhPy5ub3RlVHlwZUNvbmZpZz8ubWVudGlvbkNoYXJhY3RlcnM7XHJcbiAgICAgIHRoaXMubWVudGlvbkRlbm90YXRpb24gPSBjaGFyYWN0ZXJzLnNwbGl0KCcsJyk7XHJcbiAgICAgfSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVDb250ZW50KCkge1xyXG4gICAgdGhpcy53cml0ZVZhbHVlKHRoaXMuY29tbWVudENvbnRlbnQpO1xyXG4gIH1cclxuXHJcbiAgZG9Db21tZW50KCkge1xyXG4gICAgaWYgKCF0aGlzLmNvbW1lbnRDb250ZW50LnRyaW0oKSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB0aGlzLmZvcm1BY3Rpb24gPSB0cnVlO1xyXG5cclxuICAgIGlmICh0aGlzLnJvb3ROb3RlKSB7XHJcbiAgICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmFkZENvbW1lbnQoe2NvbmZpZzogdGhpcy5jb25maWcsY29tbWVudDogdGhpcy5jb21tZW50Q29udGVudCwgbm90ZVR5cGU6IHRoaXMubm90ZVR5cGUsIGVudGl0eUlkOiB0aGlzLmVudGl0eUlkICwgcm9vdE5vdGUgOiB7dXVpZDogdGhpcy5yb290Tm90ZS51dWlkfSwgY2xpZW50SWQ6IHRoaXMuY2xpZW50SWQgfSlcclxuICAgICAgICAucGlwZShmaW5hbGl6ZSgoKSA9PiB0aGlzLnJlc2V0U3RhdGUoKSkpXHJcbiAgICAgICAgLnN1YnNjcmliZShhY2Nlc3NPYmogPT4ge1xyXG4gICAgICAgICAgY29uc3QgY29tbWVudCA9IGFjY2Vzc09iai5kYXRhO1xyXG4gICAgICAgICAgdGhpcy5zZW50LmVtaXQoY29tbWVudCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5hZGRDb21tZW50KHtjb25maWc6IHRoaXMuY29uZmlnLGNvbW1lbnQ6IHRoaXMuY29tbWVudENvbnRlbnQsIG5vdGVUeXBlOiB0aGlzLm5vdGVUeXBlLCBlbnRpdHlJZDogdGhpcy5lbnRpdHlJZCwgY2xpZW50SWQ6IHRoaXMuY2xpZW50SWQgfSlcclxuICAgICAgICAucGlwZShmaW5hbGl6ZSgoKSA9PiB0aGlzLnJlc2V0U3RhdGUoKSkpXHJcbiAgICAgICAgLnN1YnNjcmliZShhY2Nlc3NPYmogPT4ge1xyXG4gICAgICAgICAgY29uc3QgY29tbWVudCA9IGFjY2Vzc09iai5kYXRhO1xyXG4gICAgICAgICAgY29tbWVudFsndXNlciddID0gdGhpcy51c2VyO1xyXG4gICAgICAgICAgdGhpcy5zZW50LmVtaXQoY29tbWVudCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjYW5jZWxDb21tZW50KCkge1xyXG4gICAgdGhpcy5jYW5jZWwuZW1pdCh0cnVlKTtcclxuICB9XHJcblxyXG4gIHJlc2V0U3RhdGUoKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRDb250ZW50ID0gbnVsbDtcclxuICAgIHRoaXMuZm9ybUFjdGlvbiA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgaW5pdGl0YWxpemVkKGVkaXRvcikge1xyXG4gICAgdGhpcy5pbml0LmVtaXQoZWRpdG9yKTtcclxuICB9XHJcblxyXG4gIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuY29tbWVudENvbnRlbnQ7XHJcbiAgfVxyXG5cclxuICBzZXQgdmFsdWUodjogc3RyaW5nKSB7XHJcbiAgICBpZiAodiAhPT0gdGhpcy5jb21tZW50Q29udGVudCkge1xyXG4gICAgICB0aGlzLmNvbW1lbnRDb250ZW50ID0gdjtcclxuICAgICAgdGhpcy5vbkNoYW5nZSh2KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5jb21tZW50Q29udGVudCA9IHZhbHVlO1xyXG4gICAgdGhpcy5vbkNvbnRlbnRDaGFuZ2UuZW1pdCh0aGlzLmNvbW1lbnRDb250ZW50KTtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlTm90ZShub3RlOiBOb3RlKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzU2VydmljZS51cGRhdGVOb3RlKHtjb25maWc6IHRoaXMuY29uZmlnLGNvbW1lbnQ6IHRoaXMuY29tbWVudENvbnRlbnQsIGNvbW1lbnRVVUlEOiBub3RlLnV1aWR9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT57XHJcbiAgICAgIGNvbnN0IGNvbW1lbnQgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICBub3RlLnVwZGF0ZU1vZGUgPSAhbm90ZS51cGRhdGVNb2RlO1xyXG4gICAgICB0aGlzLnNlbnQuZW1pdChjb21tZW50KTtcclxuICAgIH0pO1xyXG4gICB9XHJcblxyXG4gICBlZGl0Tm90ZShub3RlOiBOb3RlKSB7XHJcbiAgICAgIG5vdGUudXBkYXRlTW9kZSA9ICFub3RlLnVwZGF0ZU1vZGU7XHJcbiAgIH1cclxuXHJcbiAgb25DaGFuZ2UgPSAoXykgPT4ge307XHJcbiAgb25Ub3VjaGVkID0gKCkgPT4ge307XHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbjogKF86IGFueSkgPT4gdm9pZCk6IHZvaWQgeyB0aGlzLm9uQ2hhbmdlID0gZm47IH1cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbjogKCkgPT4gdm9pZCk6IHZvaWQgeyB0aGlzLm9uVG91Y2hlZCA9IGZuOyB9XHJcbn1cclxuIl19