/**
 * Update October 22, 2020: Removed the data service layer to avoid passing the config
 * at deeper levels. Ideal solution would have been to provide a configuration in the
 * context instead of manually passing that into the params.
 */
import { HttpBackend } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccessControl } from './models/access-control';
import { Note } from './models/note';
export interface CommentParam {
    config: {
        urlPrefix: string;
    };
    commentUUID?: string;
    comment?: string;
    rootNote?: Note;
    noteType?: string;
    entityId?: string;
    noteTypeId?: number;
    noteUUID?: string;
    status?: string;
    clientId?: string;
}
export declare class CommentsService {
    private http;
    constructor(handler: HttpBackend);
    getNotes({ config, noteType, entityId, clientId, status }: CommentParam): Observable<AccessControl<any>>;
    addComment({ config, comment, noteType, rootNote, entityId, clientId }: CommentParam): Observable<any>;
    updateNote({ config, comment, commentUUID }: {
        config: any;
        comment: any;
        commentUUID: any;
    }): Observable<any>;
    deleteNote({ config, commentUUID }: {
        config: any;
        commentUUID: any;
    }): Observable<any>;
    getUserStats({ config, noteType, entityId }: CommentParam): Observable<AccessControl<any>>;
    updateLastRead({ config, noteTypeId, entityId, commentUUID }: CommentParam): Observable<AccessControl<any>>;
    resolveComment({ config, noteTypeId, entityId, noteUUID }: CommentParam): Observable<AccessControl<any>>;
    getNoteType({ config, noteType, clientId }: CommentParam): Observable<AccessControl<any>>;
    getCookie(name: string): string;
    private getHttpOptions;
}
