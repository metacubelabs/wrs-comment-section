export declare class AccessControl<T> {
    data: T;
    permissions: string[];
    constructor(obj: any);
    get read(): boolean;
    get write(): boolean;
    get delete(): boolean;
    get edit(): boolean;
    hasPermissionFor(access: string): boolean;
}
