export declare class Note {
    uuid?: string;
    orgId?: number;
    orgUuid?: string;
    content?: string;
    allContent?: string;
    userId?: number;
    userUuid?: string;
    rootNote?: Note;
    entityId?: string;
    createdBy?: number;
    createdDate?: string;
    lastModifiedBy?: number;
    lastModifiedDate?: string;
    resolvedBy?: number;
    resolvedAt?: string;
    sender?: any;
    replies?: Note[];
    firstUnread?: boolean;
    replying?: boolean;
    show?: boolean;
    showAllReplie?: boolean;
    updateMode?: boolean;
    confirmDelete?: boolean;
}
