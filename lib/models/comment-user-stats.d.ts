export declare class CommentUserStats {
    userId: number;
    userUuid: string;
    lastRead: Date;
    unreadCount: number;
    commentSectionUUID: string;
}
