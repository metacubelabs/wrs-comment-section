import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccessControl } from '../models/access-control';
export declare class AccessControlUtilService {
    private http;
    constructor(http: HttpClient);
    httpGet<T>(url: string, params?: any): Observable<AccessControl<T>>;
    httpPost<T>(url: string, params?: any): Observable<AccessControl<T>>;
    httpPut<T>(url: string, params?: any): Observable<AccessControl<T>>;
    httpDelete<T>(url: string, params?: any): Observable<AccessControl<T>>;
}
