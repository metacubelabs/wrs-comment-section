export declare class BasicUtility {
    static objectToParams(paramObj: any): string;
    static propertyGetter(obj: any, prop: string): any;
    static swap(arr: any[], index1: number, index2: number): any[];
    static compare(arr1: any[], arr2: any[], prop: string, prop2?: string): any;
    static compareValue(a: any, b: any): 1 | 0 | -1;
    static isInIframe(): boolean;
    static isEmpty(val: any): boolean;
    static isEmptyOrEqual(val: any, compareWith: any): boolean;
    static isEmptyOrHas(val: any[] | any, compareWith: any): boolean;
    static unique<T>(val: T[]): T[];
}
