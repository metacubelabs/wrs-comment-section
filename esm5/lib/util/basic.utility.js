import { __read, __spread, __values } from "tslib";
var BasicUtility = /** @class */ (function () {
    function BasicUtility() {
    }
    BasicUtility.objectToParams = function (paramObj) {
        if (!paramObj) {
            return '';
        }
        var url = [];
        Object.keys(paramObj).forEach(function (key) {
            var val = paramObj[key];
            if (val != null) {
                if ((val.forEach && val.length > 0) || (!val.forEach)) {
                    url.push(key + '=' + val);
                }
            }
        });
        return url.filter(function (param) { return param.trim().length !== 0; }).join('&');
    };
    BasicUtility.propertyGetter = function (obj, prop) {
        var e_1, _a;
        if (!prop || prop === 'this') {
            return obj;
        }
        var props = prop.split('.');
        try {
            for (var props_1 = __values(props), props_1_1 = props_1.next(); !props_1_1.done; props_1_1 = props_1.next()) {
                var property = props_1_1.value;
                if (!obj) {
                    break;
                }
                obj = obj[property];
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (props_1_1 && !props_1_1.done && (_a = props_1.return)) _a.call(props_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return obj;
    };
    BasicUtility.swap = function (arr, index1, index2) {
        arr = __spread(arr);
        var temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
        return arr;
    };
    BasicUtility.compare = function (arr1, arr2, prop, prop2) {
        var _this = this;
        if (!(arr1 && arr2 && arr1.length == arr2.length)) {
            return false;
        }
        arr1.sort(function (a, b) { return _this.compareValue(_this.propertyGetter(a, prop), _this.propertyGetter(b, prop2 || prop)); });
        arr2.sort(function (a, b) { return _this.compareValue(_this.propertyGetter(a, prop), _this.propertyGetter(b, prop2 || prop)); });
        return arr1.reduce(function (a, c, i) { return a && _this.propertyGetter(c, prop) == _this.propertyGetter(arr2[i], prop2 || prop); }, true);
    };
    BasicUtility.compareValue = function (a, b) {
        return a == b ? 0 : (a > b ? 1 : -1);
    };
    BasicUtility.isInIframe = function () {
        try {
            return window.self !== window.top;
        }
        catch (e) {
            return true;
        }
    };
    BasicUtility.isEmpty = function (val) {
        if (!val) {
            return true;
        }
        if (val.length === 0) {
            return true;
        }
        return false;
    };
    BasicUtility.isEmptyOrEqual = function (val, compareWith) {
        return this.isEmpty(val) || (compareWith === val);
    };
    BasicUtility.isEmptyOrHas = function (val, compareWith) {
        return this.isEmpty(val) || (Array.isArray(val) ? val.indexOf(compareWith) !== -1 : !!val[compareWith]);
    };
    // Removes the duplicate elements from the array
    BasicUtility.unique = function (val) {
        return Array.from(new Set(val));
    };
    return BasicUtility;
}());
export { BasicUtility };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzaWMudXRpbGl0eS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1lbnQtc2VjdGlvbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL2Jhc2ljLnV0aWxpdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0lBQUE7SUFtRkEsQ0FBQztJQWpGUSwyQkFBYyxHQUFyQixVQUFzQixRQUFhO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDYixPQUFPLEVBQUUsQ0FBQztTQUNYO1FBQ0QsSUFBTSxHQUFHLEdBQWEsRUFBRSxDQUFDO1FBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztZQUMvQixJQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDMUIsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRTtvQkFDckQsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUMzQjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRU0sMkJBQWMsR0FBckIsVUFBc0IsR0FBUSxFQUFFLElBQVk7O1FBQzFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxLQUFLLE1BQU0sRUFBRTtZQUM1QixPQUFPLEdBQUcsQ0FBQztTQUNaO1FBQ0QsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs7WUFDOUIsS0FBdUIsSUFBQSxVQUFBLFNBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO2dCQUF6QixJQUFNLFFBQVEsa0JBQUE7Z0JBQ2pCLElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQ1IsTUFBTTtpQkFDUDtnQkFDRCxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3JCOzs7Ozs7Ozs7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFTSxpQkFBSSxHQUFYLFVBQVksR0FBVSxFQUFFLE1BQWMsRUFBRSxNQUFjO1FBQ3BELEdBQUcsWUFBTyxHQUFHLENBQUMsQ0FBQztRQUNmLElBQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFCLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDbkIsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRU0sb0JBQU8sR0FBZCxVQUFlLElBQVcsRUFBRSxJQUFXLEVBQUUsSUFBWSxFQUFFLEtBQWM7UUFBckUsaUJBT0M7UUFOQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2pELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLEVBQXRGLENBQXNGLENBQUMsQ0FBQztRQUM1RyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLEVBQXRGLENBQXNGLENBQUMsQ0FBQztRQUM1RyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksSUFBSSxDQUFDLEVBQWhGLENBQWdGLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUgsQ0FBQztJQUVNLHlCQUFZLEdBQW5CLFVBQW9CLENBQUMsRUFBRSxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU0sdUJBQVUsR0FBakI7UUFDRSxJQUFJO1lBQ0EsT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUM7U0FDckM7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDSCxDQUFDO0lBRU0sb0JBQU8sR0FBZCxVQUFlLEdBQVE7UUFDckIsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNSLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFTSwyQkFBYyxHQUFyQixVQUFzQixHQUFRLEVBQUUsV0FBZ0I7UUFDOUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSx5QkFBWSxHQUFuQixVQUFvQixHQUFnQixFQUFFLFdBQWdCO1FBQ3BELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUMxRyxDQUFDO0lBRUQsZ0RBQWdEO0lBQ3pDLG1CQUFNLEdBQWIsVUFBaUIsR0FBUTtRQUN2QixPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBbkZELElBbUZDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJhc2ljVXRpbGl0eSB7XHJcblxyXG4gIHN0YXRpYyBvYmplY3RUb1BhcmFtcyhwYXJhbU9iajogYW55KTogc3RyaW5nIHtcclxuICAgIGlmICghcGFyYW1PYmopIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gICAgY29uc3QgdXJsOiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgT2JqZWN0LmtleXMocGFyYW1PYmopLmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgY29uc3QgdmFsID0gcGFyYW1PYmpba2V5XTtcclxuICAgICAgaWYgKHZhbCAhPSBudWxsKSB7XHJcbiAgICAgICAgaWYgKCh2YWwuZm9yRWFjaCAmJiB2YWwubGVuZ3RoID4gMCkgfHwgKCF2YWwuZm9yRWFjaCkpIHtcclxuICAgICAgICAgIHVybC5wdXNoKGtleSArICc9JyArIHZhbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHJldHVybiB1cmwuZmlsdGVyKHBhcmFtID0+IHBhcmFtLnRyaW0oKS5sZW5ndGggIT09IDApLmpvaW4oJyYnKTtcclxuICB9XHJcblxyXG4gIHN0YXRpYyBwcm9wZXJ0eUdldHRlcihvYmo6IGFueSwgcHJvcDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXByb3AgfHwgcHJvcCA9PT0gJ3RoaXMnKSB7XHJcbiAgICAgIHJldHVybiBvYmo7XHJcbiAgICB9XHJcbiAgICBjb25zdCBwcm9wcyA9IHByb3Auc3BsaXQoJy4nKTtcclxuICAgIGZvciAoY29uc3QgcHJvcGVydHkgb2YgcHJvcHMpIHtcclxuICAgICAgaWYgKCFvYmopIHtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgICBvYmogPSBvYmpbcHJvcGVydHldO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG9iajtcclxuICB9XHJcblxyXG4gIHN0YXRpYyBzd2FwKGFycjogYW55W10sIGluZGV4MTogbnVtYmVyLCBpbmRleDI6IG51bWJlcik6IGFueVtdIHtcclxuICAgIGFyciA9IFsuLi5hcnJdO1xyXG4gICAgY29uc3QgdGVtcCA9IGFycltpbmRleDFdO1xyXG4gICAgYXJyW2luZGV4MV0gPSBhcnJbaW5kZXgyXTtcclxuICAgIGFycltpbmRleDJdID0gdGVtcDtcclxuICAgIHJldHVybiBhcnI7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgY29tcGFyZShhcnIxOiBhbnlbXSwgYXJyMjogYW55W10sIHByb3A6IHN0cmluZywgcHJvcDI/OiBzdHJpbmcpIHtcclxuICAgIGlmICghKGFycjEgJiYgYXJyMiAmJiBhcnIxLmxlbmd0aCA9PSBhcnIyLmxlbmd0aCkpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgYXJyMS5zb3J0KChhLCBiKSA9PiB0aGlzLmNvbXBhcmVWYWx1ZSh0aGlzLnByb3BlcnR5R2V0dGVyKGEsIHByb3ApLCB0aGlzLnByb3BlcnR5R2V0dGVyKGIsIHByb3AyIHx8IHByb3ApKSk7XHJcbiAgICBhcnIyLnNvcnQoKGEsIGIpID0+IHRoaXMuY29tcGFyZVZhbHVlKHRoaXMucHJvcGVydHlHZXR0ZXIoYSwgcHJvcCksIHRoaXMucHJvcGVydHlHZXR0ZXIoYiwgcHJvcDIgfHwgcHJvcCkpKTtcclxuICAgIHJldHVybiBhcnIxLnJlZHVjZSgoYSwgYywgaSkgPT4gYSAmJiB0aGlzLnByb3BlcnR5R2V0dGVyKGMsIHByb3ApID09IHRoaXMucHJvcGVydHlHZXR0ZXIoYXJyMltpXSwgcHJvcDIgfHwgcHJvcCksIHRydWUpO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIGNvbXBhcmVWYWx1ZShhLCBiKSB7XHJcbiAgICByZXR1cm4gYSA9PSBiID8gMCA6IChhID4gYiA/IDEgOiAtMSk7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgaXNJbklmcmFtZSgpOiBib29sZWFuIHtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5zZWxmICE9PSB3aW5kb3cudG9wO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc3RhdGljIGlzRW1wdHkodmFsOiBhbnkpOiBib29sZWFuIHtcclxuICAgIGlmICghdmFsKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHZhbC5sZW5ndGggPT09IDApIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgaXNFbXB0eU9yRXF1YWwodmFsOiBhbnksIGNvbXBhcmVXaXRoOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzRW1wdHkodmFsKSB8fCAoY29tcGFyZVdpdGggPT09IHZhbCk7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgaXNFbXB0eU9ySGFzKHZhbDogYW55W10gfCBhbnksIGNvbXBhcmVXaXRoOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzRW1wdHkodmFsKSB8fCAoQXJyYXkuaXNBcnJheSh2YWwpID8gdmFsLmluZGV4T2YoY29tcGFyZVdpdGgpICE9PSAtMSA6ICEhdmFsW2NvbXBhcmVXaXRoXSk7XHJcbiAgfVxyXG5cclxuICAvLyBSZW1vdmVzIHRoZSBkdXBsaWNhdGUgZWxlbWVudHMgZnJvbSB0aGUgYXJyYXlcclxuICBzdGF0aWMgdW5pcXVlPFQ+KHZhbDogVFtdKSB7XHJcbiAgICByZXR1cm4gQXJyYXkuZnJvbShuZXcgU2V0PFQ+KHZhbCkpO1xyXG4gIH1cclxufVxyXG4iXX0=