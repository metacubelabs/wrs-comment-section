import { __decorate } from "tslib";
/**
 * Update October 22, 2020: Removed the data service layer to avoid passing the config
 * at deeper levels. Ideal solution would have been to provide a configuration in the
 * context instead of manually passing that into the params.
 */
import { HttpClient, HttpClientModule, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Note } from './models/note';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var CommentsService = /** @class */ (function () {
    function CommentsService(handler) {
        this.http = new HttpClient(handler);
    }
    CommentsService.prototype.getNotes = function (_a) {
        var config = _a.config, noteType = _a.noteType, entityId = _a.entityId, clientId = _a.clientId, status = _a.status;
        var headers = this.getHttpOptions();
        if (status == null)
            return this.http.get(config.urlPrefix + 'note/get-note?noteType=' + noteType + '&entityId=' + entityId + '&clientId=' + clientId, headers);
        else
            return this.http.get(config.urlPrefix + 'note/get-note?noteType=' + noteType + '&entityId=' + entityId + '&clientId=' + clientId + '&status=' + status, headers);
    };
    CommentsService.prototype.addComment = function (_a) {
        var config = _a.config, comment = _a.comment, noteType = _a.noteType, rootNote = _a.rootNote, entityId = _a.entityId, clientId = _a.clientId;
        var commentObject = new Note();
        var headers = this.getHttpOptions();
        commentObject.content = comment;
        commentObject.rootNote = rootNote;
        commentObject.entityId = entityId;
        return this.http.post(config.urlPrefix + 'note/add?noteType=' + noteType + '&clientId=' + clientId, commentObject, headers);
    };
    CommentsService.prototype.updateNote = function (_a) {
        var config = _a.config, comment = _a.comment, commentUUID = _a.commentUUID;
        var headers = this.getHttpOptions();
        var commentObject = new Note();
        commentObject.content = comment;
        commentObject.uuid = commentUUID;
        return this.http.post(config.urlPrefix + 'note/update', commentObject, headers);
    };
    CommentsService.prototype.deleteNote = function (_a) {
        var config = _a.config, commentUUID = _a.commentUUID;
        var headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'note/delete?uuid=' + commentUUID, headers);
    };
    CommentsService.prototype.getUserStats = function (_a) {
        var config = _a.config, noteType = _a.noteType, entityId = _a.entityId;
        var headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'note/comment-user-stats'
            + '?noteType=' + noteType + '&entityId=' + entityId, headers);
    };
    CommentsService.prototype.updateLastRead = function (_a) {
        var config = _a.config, noteTypeId = _a.noteTypeId, entityId = _a.entityId, commentUUID = _a.commentUUID;
        var headers = this.getHttpOptions();
        return this.http.post(config.urlPrefix + 'note/last-read?noteUuid=' + commentUUID + '&noteTypeId=' + noteTypeId + '&entityId=' + entityId, {}, headers);
    };
    // createCommentSection({config}: CommentParam): Observable<AccessControl<CommentSection>> {
    //   return this.access.httpPost<CommentSection>(config.urlPrefix + 'section/create');
    // }
    CommentsService.prototype.resolveComment = function (_a) {
        var config = _a.config, noteTypeId = _a.noteTypeId, entityId = _a.entityId, noteUUID = _a.noteUUID;
        var headers = this.getHttpOptions();
        return this.http.post(config.urlPrefix + 'note/resolve?noteTypeId=' + noteTypeId + '&entityId=' + entityId + '&noteUUID=' + noteUUID, {}, headers);
    };
    CommentsService.prototype.getNoteType = function (_a) {
        var config = _a.config, noteType = _a.noteType, clientId = _a.clientId;
        var headers = this.getHttpOptions();
        return this.http.get(config.urlPrefix + 'noteType/get?noteType=' + noteType + '&clientId=' + clientId, headers);
    };
    CommentsService.prototype.getCookie = function (name) {
        var ca = document.cookie.split(';');
        var caLen = ca.length;
        var cookieName = name + "=";
        var c;
        for (var i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    };
    CommentsService.prototype.getHttpOptions = function () {
        var authorization = this.getCookie('access_token');
        var contentType = "application/json";
        var httpHeaderOptions = {
            'Authorization': 'Bearer ' + authorization,
            'Content-Type': contentType
        };
        var httpOptions = {
            headers: new HttpHeaders(httpHeaderOptions)
        };
        return httpOptions;
    };
    CommentsService.ctorParameters = function () { return [
        { type: HttpBackend }
    ]; };
    CommentsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function CommentsService_Factory() { return new CommentsService(i0.ɵɵinject(i1.HttpBackend)); }, token: CommentsService, providedIn: "root" });
    CommentsService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CommentsService);
    return CommentsService;
}());
export { CommentsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1zZWN0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tZW50LXNlY3Rpb24vIiwic291cmNlcyI6WyJsaWIvY29tbWVudC1zZWN0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOzs7O0dBSUc7QUFFSCxPQUFPLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM5RixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTzNDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQXFCckM7SUFLRSx5QkFBWSxPQUFvQjtRQUM5QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRCxrQ0FBUSxHQUFSLFVBQVMsRUFBeUQ7WUFBeEQsa0JBQU0sRUFBQyxzQkFBUSxFQUFDLHNCQUFRLEVBQUMsc0JBQVEsRUFBRSxrQkFBTTtRQUNqRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDcEMsSUFBRyxNQUFNLElBQUksSUFBSTtZQUNmLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQU0sTUFBTSxDQUFDLFNBQVMsR0FBRyx5QkFBeUIsR0FBRyxRQUFRLEdBQUcsWUFBWSxHQUFDLFFBQVEsR0FBRyxZQUFZLEdBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDOztZQUU1SSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLE1BQU0sQ0FBQyxTQUFTLEdBQUcseUJBQXlCLEdBQUcsUUFBUSxHQUFHLFlBQVksR0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFDLFFBQVEsR0FBRyxVQUFVLEdBQUcsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3RLLENBQUM7SUFFRCxvQ0FBVSxHQUFWLFVBQVcsRUFBd0U7WUFBdkUsa0JBQU0sRUFBRSxvQkFBTyxFQUFFLHNCQUFRLEVBQUcsc0JBQVEsRUFBRSxzQkFBUSxFQUFFLHNCQUFRO1FBQ2xFLElBQU0sYUFBYSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDakMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ2hDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ2xDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQU0sTUFBTSxDQUFDLFNBQVMsR0FBRyxvQkFBb0IsR0FBRyxRQUFRLEdBQUUsWUFBWSxHQUFDLFFBQVEsRUFBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDL0gsQ0FBQztJQUVELG9DQUFVLEdBQVYsVUFBVyxFQUE0QjtZQUEzQixrQkFBTSxFQUFDLG9CQUFPLEVBQUMsNEJBQVc7UUFDcEMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQU0sYUFBYSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDakMsYUFBYSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDaEMsYUFBYSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTSxNQUFNLENBQUMsU0FBUyxHQUFHLGFBQWEsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELG9DQUFVLEdBQVYsVUFBVyxFQUFvQjtZQUFuQixrQkFBTSxFQUFDLDRCQUFXO1FBQzVCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLE1BQU0sQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLEdBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFDRCxzQ0FBWSxHQUFaLFVBQWEsRUFBMEM7WUFBekMsa0JBQU0sRUFBRSxzQkFBUSxFQUFFLHNCQUFRO1FBQ3RDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLE1BQU0sQ0FBQyxTQUFTLEdBQUcseUJBQXlCO2NBQ2xFLFlBQVksR0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsd0NBQWMsR0FBZCxVQUFlLEVBQXdEO1lBQXZELGtCQUFNLEVBQUMsMEJBQVUsRUFBRSxzQkFBUSxFQUFFLDRCQUFXO1FBQ3RELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFNLE1BQU0sQ0FBQyxTQUFTLEdBQUcsMEJBQTBCLEdBQUcsV0FBVyxHQUFHLGNBQWMsR0FBQyxVQUFVLEdBQUcsWUFBWSxHQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDNUosQ0FBQztJQUVELDRGQUE0RjtJQUM1RixzRkFBc0Y7SUFDdEYsSUFBSTtJQUVKLHdDQUFjLEdBQWQsVUFBZSxFQUFxRDtZQUFwRCxrQkFBTSxFQUFDLDBCQUFVLEVBQUMsc0JBQVEsRUFBRSxzQkFBUTtRQUNsRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDcEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTSxNQUFNLENBQUMsU0FBUyxHQUFHLDBCQUEwQixHQUFHLFVBQVUsR0FBRSxZQUFZLEdBQUcsUUFBUSxHQUFHLFlBQVksR0FBRyxRQUFRLEVBQUUsRUFBRSxFQUFHLE9BQU8sQ0FBQyxDQUFDO0lBQzFKLENBQUM7SUFFRCxxQ0FBVyxHQUFYLFVBQVksRUFBeUM7WUFBeEMsa0JBQU0sRUFBQyxzQkFBUSxFQUFDLHNCQUFRO1FBQ25DLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLE1BQU0sQ0FBQyxTQUFTLEdBQUcsd0JBQXdCLEdBQUcsUUFBUSxHQUFDLFlBQVksR0FBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDcEgsQ0FBQztJQUVNLG1DQUFTLEdBQWhCLFVBQWlCLElBQVk7UUFDM0IsSUFBSSxFQUFFLEdBQWtCLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25ELElBQUksS0FBSyxHQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUM7UUFDOUIsSUFBSSxVQUFVLEdBQU0sSUFBSSxNQUFHLENBQUM7UUFDNUIsSUFBSSxDQUFTLENBQUM7UUFFZCxLQUFLLElBQUksQ0FBQyxHQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNqRDtTQUNGO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRU8sd0NBQWMsR0FBdEI7UUFDRSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25ELElBQUksV0FBVyxHQUFHLGtCQUFrQixDQUFDO1FBQ3JDLElBQUksaUJBQWlCLEdBQUc7WUFDdEIsZUFBZSxFQUFFLFNBQVMsR0FBSSxhQUFhO1lBQzNDLGNBQWMsRUFBRSxXQUFXO1NBQzVCLENBQUM7UUFDRixJQUFNLFdBQVcsR0FBRztZQUNsQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsaUJBQWlCLENBQUM7U0FDNUMsQ0FBQztRQUNGLE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7O2dCQXBGb0IsV0FBVzs7O0lBTHJCLGVBQWU7UUFIM0IsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLGVBQWUsQ0EyRjNCOzBCQTlIRDtDQThIQyxBQTNGRCxJQTJGQztTQTNGWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIFVwZGF0ZSBPY3RvYmVyIDIyLCAyMDIwOiBSZW1vdmVkIHRoZSBkYXRhIHNlcnZpY2UgbGF5ZXIgdG8gYXZvaWQgcGFzc2luZyB0aGUgY29uZmlnXHJcbiAqIGF0IGRlZXBlciBsZXZlbHMuIElkZWFsIHNvbHV0aW9uIHdvdWxkIGhhdmUgYmVlbiB0byBwcm92aWRlIGEgY29uZmlndXJhdGlvbiBpbiB0aGVcclxuICogY29udGV4dCBpbnN0ZWFkIG9mIG1hbnVhbGx5IHBhc3NpbmcgdGhhdCBpbnRvIHRoZSBwYXJhbXMuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cENsaWVudE1vZHVsZSwgSHR0cEhlYWRlcnMsIEh0dHBCYWNrZW5kIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWNjZXNzQ29udHJvbCB9IGZyb20gJy4vbW9kZWxzL2FjY2Vzcy1jb250cm9sJztcclxuaW1wb3J0IHsgQ29tbWVudFVzZXJTdGF0cyB9IGZyb20gJy4vbW9kZWxzL2NvbW1lbnQtdXNlci1zdGF0cyc7XHJcbmltcG9ydCB7IEFjY2Vzc0NvbnRyb2xVdGlsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvYWNjZXNzLWNvbnRyb2wtdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQmFzaWNVdGlsaXR5IH0gZnJvbSAnLi91dGlsL2Jhc2ljLnV0aWxpdHknO1xyXG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE5vdGUgfSBmcm9tICcuL21vZGVscy9ub3RlJztcclxuXHJcbi8vIEEgZ2VuZXJpYyBpbnRlcmZhY2UgdG8gdGFrZSB0aGUgcGFyYW1ldGVycyBpbiBzZXJ2aWNlXHJcbmV4cG9ydCBpbnRlcmZhY2UgQ29tbWVudFBhcmFtIHtcclxuICBjb25maWc6IHtcclxuICAgIHVybFByZWZpeDogc3RyaW5nXHJcbiAgfTtcclxuICBjb21tZW50VVVJRD86IHN0cmluZztcclxuICBjb21tZW50Pzogc3RyaW5nO1xyXG4gIHJvb3ROb3RlPzogTm90ZTtcclxuICBub3RlVHlwZT86IHN0cmluZztcclxuICBlbnRpdHlJZD86IHN0cmluZztcclxuICBub3RlVHlwZUlkPzogbnVtYmVyO1xyXG4gIG5vdGVVVUlEPzogc3RyaW5nO1xyXG4gIHN0YXR1cz86IHN0cmluZztcclxuICBjbGllbnRJZD86IHN0cmluZztcclxufVxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudHNTZXJ2aWNlIHtcclxuXHJcblxyXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50O1xyXG5cclxuICBjb25zdHJ1Y3RvcihoYW5kbGVyOiBIdHRwQmFja2VuZCkge1xyXG4gICAgdGhpcy5odHRwID0gbmV3IEh0dHBDbGllbnQoaGFuZGxlcik7XHJcbiAgfVxyXG5cclxuICBnZXROb3Rlcyh7Y29uZmlnLG5vdGVUeXBlLGVudGl0eUlkLGNsaWVudElkLCBzdGF0dXN9OiBDb21tZW50UGFyYW0pOiBPYnNlcnZhYmxlPEFjY2Vzc0NvbnRyb2w8YW55Pj4ge1xyXG4gICAgbGV0IGhlYWRlcnMgPSB0aGlzLmdldEh0dHBPcHRpb25zKCk7XHJcbiAgICBpZihzdGF0dXMgPT0gbnVsbClcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55Pihjb25maWcudXJsUHJlZml4ICsgJ25vdGUvZ2V0LW5vdGU/bm90ZVR5cGU9JyArIG5vdGVUeXBlICsgJyZlbnRpdHlJZD0nK2VudGl0eUlkICsgJyZjbGllbnRJZD0nK2NsaWVudElkLCBoZWFkZXJzKTtcclxuICAgIGVsc2VcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55Pihjb25maWcudXJsUHJlZml4ICsgJ25vdGUvZ2V0LW5vdGU/bm90ZVR5cGU9JyArIG5vdGVUeXBlICsgJyZlbnRpdHlJZD0nK2VudGl0eUlkICsgJyZjbGllbnRJZD0nK2NsaWVudElkICsgJyZzdGF0dXM9JyArIHN0YXR1cywgaGVhZGVycyk7XHJcbiAgfVxyXG5cclxuICBhZGRDb21tZW50KHtjb25maWcsIGNvbW1lbnQsIG5vdGVUeXBlICwgcm9vdE5vdGUsIGVudGl0eUlkLCBjbGllbnRJZH06IENvbW1lbnRQYXJhbSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCBjb21tZW50T2JqZWN0ID0gbmV3IE5vdGUoKTtcclxuICAgIGxldCBoZWFkZXJzID0gdGhpcy5nZXRIdHRwT3B0aW9ucygpO1xyXG4gICAgY29tbWVudE9iamVjdC5jb250ZW50ID0gY29tbWVudDtcclxuICAgIGNvbW1lbnRPYmplY3Qucm9vdE5vdGUgPSByb290Tm90ZTtcclxuICAgIGNvbW1lbnRPYmplY3QuZW50aXR5SWQgPSBlbnRpdHlJZDtcclxuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KGNvbmZpZy51cmxQcmVmaXggKyAnbm90ZS9hZGQ/bm90ZVR5cGU9JyArIG5vdGVUeXBlICsnJmNsaWVudElkPScrY2xpZW50SWQsY29tbWVudE9iamVjdCwgaGVhZGVycyk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVOb3RlKHtjb25maWcsY29tbWVudCxjb21tZW50VVVJRH0pIHtcclxuICAgIGxldCBoZWFkZXJzID0gdGhpcy5nZXRIdHRwT3B0aW9ucygpO1xyXG4gICAgY29uc3QgY29tbWVudE9iamVjdCA9IG5ldyBOb3RlKCk7XHJcbiAgICBjb21tZW50T2JqZWN0LmNvbnRlbnQgPSBjb21tZW50O1xyXG4gICAgY29tbWVudE9iamVjdC51dWlkID0gY29tbWVudFVVSUQ7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55Pihjb25maWcudXJsUHJlZml4ICsgJ25vdGUvdXBkYXRlJywgY29tbWVudE9iamVjdCwgaGVhZGVycyk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVOb3RlKHtjb25maWcsY29tbWVudFVVSUR9KSB7XHJcbiAgICBsZXQgaGVhZGVycyA9IHRoaXMuZ2V0SHR0cE9wdGlvbnMoKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oY29uZmlnLnVybFByZWZpeCArICdub3RlL2RlbGV0ZT91dWlkPScrY29tbWVudFVVSUQsIGhlYWRlcnMpO1xyXG4gIH1cclxuICBnZXRVc2VyU3RhdHMoe2NvbmZpZywgbm90ZVR5cGUsIGVudGl0eUlkfTogQ29tbWVudFBhcmFtKTogT2JzZXJ2YWJsZTxBY2Nlc3NDb250cm9sPGFueT4+IHtcclxuICAgIGxldCBoZWFkZXJzID0gdGhpcy5nZXRIdHRwT3B0aW9ucygpO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55Pihjb25maWcudXJsUHJlZml4ICsgJ25vdGUvY29tbWVudC11c2VyLXN0YXRzJ1xyXG4gICAgICArICc/bm90ZVR5cGU9Jytub3RlVHlwZSArICcmZW50aXR5SWQ9JysgZW50aXR5SWQsIGhlYWRlcnMpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlTGFzdFJlYWQoe2NvbmZpZyxub3RlVHlwZUlkLCBlbnRpdHlJZCwgY29tbWVudFVVSUR9OiBDb21tZW50UGFyYW0pOiBPYnNlcnZhYmxlPEFjY2Vzc0NvbnRyb2w8YW55Pj4ge1xyXG4gICAgbGV0IGhlYWRlcnMgPSB0aGlzLmdldEh0dHBPcHRpb25zKCk7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55Pihjb25maWcudXJsUHJlZml4ICsgJ25vdGUvbGFzdC1yZWFkP25vdGVVdWlkPScgKyBjb21tZW50VVVJRCArICcmbm90ZVR5cGVJZD0nK25vdGVUeXBlSWQgKyAnJmVudGl0eUlkPScrIGVudGl0eUlkLCB7fSwgaGVhZGVycyk7XHJcbiAgfVxyXG5cclxuICAvLyBjcmVhdGVDb21tZW50U2VjdGlvbih7Y29uZmlnfTogQ29tbWVudFBhcmFtKTogT2JzZXJ2YWJsZTxBY2Nlc3NDb250cm9sPENvbW1lbnRTZWN0aW9uPj4ge1xyXG4gIC8vICAgcmV0dXJuIHRoaXMuYWNjZXNzLmh0dHBQb3N0PENvbW1lbnRTZWN0aW9uPihjb25maWcudXJsUHJlZml4ICsgJ3NlY3Rpb24vY3JlYXRlJyk7XHJcbiAgLy8gfVxyXG5cclxuICByZXNvbHZlQ29tbWVudCh7Y29uZmlnLG5vdGVUeXBlSWQsZW50aXR5SWQsIG5vdGVVVUlEIH06IENvbW1lbnRQYXJhbSk6IE9ic2VydmFibGU8QWNjZXNzQ29udHJvbDxhbnk+PiB7XHJcbiAgICBsZXQgaGVhZGVycyA9IHRoaXMuZ2V0SHR0cE9wdGlvbnMoKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KGNvbmZpZy51cmxQcmVmaXggKyAnbm90ZS9yZXNvbHZlP25vdGVUeXBlSWQ9JyArIG5vdGVUeXBlSWQgKycmZW50aXR5SWQ9JyArIGVudGl0eUlkICsgJyZub3RlVVVJRD0nICsgbm90ZVVVSUQsIHt9ICwgaGVhZGVycyk7XHJcbiAgfVxyXG5cclxuICBnZXROb3RlVHlwZSh7Y29uZmlnLG5vdGVUeXBlLGNsaWVudElkIH06IENvbW1lbnRQYXJhbSk6IE9ic2VydmFibGU8QWNjZXNzQ29udHJvbDxhbnk+PiB7XHJcbiAgICBsZXQgaGVhZGVycyA9IHRoaXMuZ2V0SHR0cE9wdGlvbnMoKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oY29uZmlnLnVybFByZWZpeCArICdub3RlVHlwZS9nZXQ/bm90ZVR5cGU9JyArIG5vdGVUeXBlKycmY2xpZW50SWQ9JysgY2xpZW50SWQsIGhlYWRlcnMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldENvb2tpZShuYW1lOiBzdHJpbmcpIHtcclxuICAgIGxldCBjYTogQXJyYXk8c3RyaW5nPiA9IGRvY3VtZW50LmNvb2tpZS5zcGxpdCgnOycpO1xyXG4gICAgbGV0IGNhTGVuOiBudW1iZXIgPSBjYS5sZW5ndGg7XHJcbiAgICBsZXQgY29va2llTmFtZSA9IGAke25hbWV9PWA7XHJcbiAgICBsZXQgYzogc3RyaW5nO1xyXG5cclxuICAgIGZvciAobGV0IGk6IG51bWJlciA9IDA7IGkgPCBjYUxlbjsgaSArPSAxKSB7XHJcbiAgICAgIGMgPSBjYVtpXS5yZXBsYWNlKC9eXFxzKy9nLCAnJyk7XHJcbiAgICAgIGlmIChjLmluZGV4T2YoY29va2llTmFtZSkgPT0gMCkge1xyXG4gICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhjb29raWVOYW1lLmxlbmd0aCwgYy5sZW5ndGgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gJyc7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldEh0dHBPcHRpb25zKCkge1xyXG4gICAgbGV0IGF1dGhvcml6YXRpb24gPSB0aGlzLmdldENvb2tpZSgnYWNjZXNzX3Rva2VuJyk7XHJcbiAgICBsZXQgY29udGVudFR5cGUgPSBcImFwcGxpY2F0aW9uL2pzb25cIjtcclxuICAgIGxldCBodHRwSGVhZGVyT3B0aW9ucyA9IHtcclxuICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmVhcmVyICcgKyAgYXV0aG9yaXphdGlvbixcclxuICAgICAgJ0NvbnRlbnQtVHlwZSc6IGNvbnRlbnRUeXBlXHJcbiAgICB9O1xyXG4gICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyhodHRwSGVhZGVyT3B0aW9ucylcclxuICAgIH07XHJcbiAgICByZXR1cm4gaHR0cE9wdGlvbnM7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=