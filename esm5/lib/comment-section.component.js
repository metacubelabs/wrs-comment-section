import { __decorate } from "tslib";
import { AfterContentInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommentsService } from './comment-section.service';
import { OrderPipe } from 'ngx-order-pipe';
import { TimeagoDefaultFormatter } from 'ngx-timeago';
var CommentSectionComponent = /** @class */ (function () {
    function CommentSectionComponent(commentsService, orderService, cdr) {
        this.commentsService = commentsService;
        this.orderService = orderService;
        this.cdr = cdr;
        this.commentContent = '';
        this.frozen = false;
        this.readOnly = false;
        this.loading = true;
        this.noteTypeLoading = true;
        this.commentList = [];
        this.isLastReadUpdated = false;
        this.notes = [];
        this.commentsTree = [];
        this.mentionDenotation = [];
        this.show = false;
        this.readNote = true;
        this.loadCommentUserStats = true;
        this.commentMap = {};
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.mentionClick = new EventEmitter();
        // cbk = debounce(() => this.inview, 300);
        // cbk =  () => this.inview();
        this.showResolved = false;
        this.onChange = function (_) { };
        this.onTouched = function () { };
    }
    CommentSectionComponent_1 = CommentSectionComponent;
    CommentSectionComponent.prototype.ngOnInit = function () {
        // window.addEventListener('scroll',this.cbk);
        // this.getNoteType();
    };
    CommentSectionComponent.prototype.updateCommentContent = function (newVal) {
        this.writeValue(newVal);
    };
    CommentSectionComponent.prototype.ngAfterContentInit = function () {
        this.getNotes();
        this.getNoteType();
    };
    CommentSectionComponent.prototype.getNotes = function () {
        var _this = this;
        this.loading = true;
        this.commentsTree = [];
        var status = 'ACTIVE';
        if (this.showResolved) {
            status = 'RESOLVED';
        }
        this.commentsService.getNotes({ config: this.config, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId, status: status })
            .subscribe(function (accessObj) {
            _this.notes = accessObj.data;
            var commentSection = accessObj.data;
            var sordBy = 'creationDate';
            if (status == 'RESOLVED') {
                sordBy = 'resolvedAt';
            }
            var comments = _this.orderService.transform(commentSection, sordBy);
            _this.commentList = comments;
            comments.forEach(function (comment) {
                _this.commentMap[comment.uuid] = comment;
                comment.replies = [];
            });
            comments.forEach(function (comment) {
                if (comment.rootNote) {
                    _this.commentMap[comment.rootNote.uuid].replies.push(comment);
                }
                else {
                    _this.commentsTree.push(comment);
                }
            });
            _this.loading = false;
            _this.updateLastRead();
        }, function (error) {
            _this.loading = false;
        });
    };
    CommentSectionComponent.prototype.getNoteType = function () {
        var _this = this;
        this.noteTypeLoading = true;
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(function (response) {
            var _a, _b;
            _this.noteTypeData = response === null || response === void 0 ? void 0 : response.data;
            var characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            _this.mentionDenotation = characters.split(',');
            _this.noteTypeLoading = false;
        }, function (error) {
            _this.noteTypeLoading = false;
        });
    };
    CommentSectionComponent.prototype.ngAfterViewChecked = function () {
        // console.log('mention', this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'));
        // Array.from(this.commentSectionWrapper.nativeElement.getElementsByClassName('mention'))
        //   .forEach(elem => {
        //     const mentionChar = (<any>elem).getAttribute('data-denotation-char');
        //     const id = (<any>elem).getAttribute('data-id');
        //     const value = (<any>elem).getAttribute('data-value');
        //     const elemRef = elem;
        //     (<any>elem).onclick = (event) => {
        //       this.mentionClick.emit({mentionChar, id, value, elemRef, event});
        //   };
        //   console.log('listerner set on ', elem);
        // });
    };
    CommentSectionComponent.prototype.ngOnChange = function () {
    };
    CommentSectionComponent.prototype.setStatsOnView = function () {
        // this.loadCommentUserStats = true;
        // if()
        // this.commentsService.getUserStats({config: this.config, noteType : this.noteTypeData.uuid ,entityId : this.entityId})
        //   .subscribe(data => {
        //     this.commentUserStats = data.data;
        //     const commentList = this.commentList.filter(c => c.userId !== this.currentUser.id);
        //     const comment = commentList[commentList.length - this.commentUserStats.unreadCount];
        //     this.loadCommentUserStats = false;
        //     this.updateLastRead();
        //     if (comment) {
        //       comment.firstUnread = true;
        //     }
        //   });
    };
    CommentSectionComponent.prototype.setReplyTo = function (comment) {
        var _this = this;
        comment.replying = true;
        this.cdr.detectChanges();
        setTimeout(function () { return _this.scroll('comment_quill_' + comment.uuid); }, 150);
    };
    CommentSectionComponent.prototype.resolve = function (comment) {
        var _this = this;
        this.commentsService.resolveComment({ config: this.config, noteTypeId: comment.noteType.id, entityId: this.entityId, noteUUID: comment.uuid }).subscribe(function (data) {
            _this.commentsTree = _this.commentsTree.filter(function (note) { return note.uuid != comment.uuid; });
        });
        this.cdr.detectChanges();
        setTimeout(function () { return _this.scroll('comment_quill_' + comment.uuid); }, 150);
    };
    CommentSectionComponent.prototype.pushToCommentList = function (addedComment, comment) {
        this.commentMap[addedComment.uuid] = addedComment;
        this.commentList.push(addedComment);
        addedComment.replies = [];
        if (comment) {
            comment.replying = false;
        }
        if (addedComment.rootNote) {
            this.commentMap[comment.uuid].replies.push(addedComment);
        }
        else {
            this.commentsTree.push(addedComment);
        }
        this.cdr.detectChanges();
        this.scrollToComment(addedComment);
    };
    CommentSectionComponent.prototype.pushWhenEdit = function (editNote, comment) {
        this.commentsTree.forEach(function (comment) {
            if (editNote.uuid == comment.uuid) {
                comment.content = editNote.content;
            }
            else {
                comment.replies.forEach(function (reply) {
                    if (editNote.uuid == reply.uuid)
                        reply.content = editNote.content;
                });
            }
        });
        this.cdr.detectChanges();
    };
    CommentSectionComponent.prototype.deleteToCommentList = function (deletedNote) {
        this.commentsTree = this.commentsTree.filter(function (note) { return note.uuid != deletedNote.uuid; });
        this.commentsTree.forEach(function (comment) {
            comment.replies = comment.replies.filter(function (x) { return x.uuid != deletedNote.uuid; });
        });
    };
    CommentSectionComponent.prototype.cancelReply = function (comment) {
        comment.replying = false;
    };
    CommentSectionComponent.prototype.scrollToComment = function (comment) {
        var _this = this;
        this.scroll(this.getClassName(comment), function () {
            var repliedComment = _this.commentList.filter(function (commentObject) { return comment.uuid == commentObject.uuid; })[0];
            if (repliedComment) {
                _this.selectComment(repliedComment);
            }
        });
    };
    CommentSectionComponent.prototype.scroll = function (className, callback) {
        var classElement = document.getElementsByClassName(className);
        if (classElement.length > 0) {
            classElement[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
            if (callback) {
                callback();
            }
        }
    };
    CommentSectionComponent.prototype.getClassName = function (comment) {
        return 'note_' + comment.uuid;
    };
    CommentSectionComponent.prototype.selectComment = function (comment) {
        comment['selected'] = true;
        setTimeout(function () { return comment['selected'] = false; }, 3000);
    };
    Object.defineProperty(CommentSectionComponent.prototype, "mainCommentBox", {
        get: function () {
            return this.commentBox;
        },
        enumerable: true,
        configurable: true
    });
    CommentSectionComponent.prototype.setFocus = function (editor, box) {
        box.editor.getQuill().focus();
    };
    CommentSectionComponent.prototype.formatDate = function (date_str) {
        var date = new Date(date_str);
        return date.toLocaleString();
    };
    CommentSectionComponent.prototype.onSuggest = function (event) {
        this.suggest.emit(event);
    };
    CommentSectionComponent.prototype.updateLastRead = function () {
        var _this = this;
        if (!this.isLastReadUpdated) {
            var comments = this.commentList.filter(function (c) { return c.userId !== _this.currentUser.id; });
            var lastComment = comments[comments.length - 1];
            if (lastComment) {
                this.commentsService.updateLastRead({ config: this.config, noteTypeId: this.noteTypeData.id, entityId: this.entityId, commentUUID: lastComment.uuid }).subscribe(function (data) {
                    _this.isLastReadUpdated = true;
                });
            }
        }
    };
    Object.defineProperty(CommentSectionComponent.prototype, "value", {
        //   isElementInViewport (el) {
        //     const rect = el.getBoundingClientRect();
        //     return (
        //         rect.top >= 0 &&
        //         rect.left >= 0 &&
        //         rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
        //         rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
        //     );
        // }
        get: function () { return this.commentContent; },
        set: function (v) {
            if (v !== this.commentContent) {
                this.commentContent = v;
                this.onChange(v);
            }
        },
        enumerable: true,
        configurable: true
    });
    CommentSectionComponent.prototype.writeValue = function (value) {
        this.commentContent = value;
        this.onChange(value);
    };
    CommentSectionComponent.prototype.registerOnChange = function (fn) { this.onChange = fn; };
    CommentSectionComponent.prototype.registerOnTouched = function (fn) { this.onTouched = fn; };
    CommentSectionComponent.prototype.ngOnDestroy = function () {
        // window.removeEventListener('scroll', this.cbk);
    };
    CommentSectionComponent.prototype.showResolvedNote = function () {
        this.showResolved = !this.showResolved;
    };
    CommentSectionComponent.prototype.changeSwitch = function () {
        this.showResolved = !this.showResolved;
        this.getNotes();
    };
    CommentSectionComponent.prototype.checkQuill = function (replyQuill) {
    };
    CommentSectionComponent.prototype.strip = function (html) {
        // this.showLessContent(html);
        var doc = new DOMParser().parseFromString(html, 'text/html');
        return doc.body.textContent || "";
    };
    CommentSectionComponent.prototype.deleteNote = function (note) {
        var _this = this;
        this.commentsService.deleteNote({ config: this.config, commentUUID: note.uuid }).subscribe(function (response) {
            var comment = response.data;
            _this.deleteToCommentList(comment);
        });
    };
    CommentSectionComponent.prototype.editNote = function (note) {
        note.updateMode = !note.updateMode;
    };
    CommentSectionComponent.prototype.confirmDelete = function (note) {
        note.confirmDelete = !note.confirmDelete;
    };
    var CommentSectionComponent_1;
    CommentSectionComponent.ctorParameters = function () { return [
        { type: CommentsService },
        { type: OrderPipe },
        { type: ChangeDetectorRef }
    ]; };
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "frozen", void 0);
    __decorate([
        ViewChild('mainCommentBox', { static: false })
    ], CommentSectionComponent.prototype, "commentBox", void 0);
    __decorate([
        ViewChild('commentSectionWrapper', { static: false })
    ], CommentSectionComponent.prototype, "commentSectionWrapper", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "readOnly", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "currentUser", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "noteType", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "entityId", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "clientId", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "suggestions", void 0);
    __decorate([
        Input()
    ], CommentSectionComponent.prototype, "config", void 0);
    __decorate([
        Output()
    ], CommentSectionComponent.prototype, "suggest", void 0);
    __decorate([
        Output()
    ], CommentSectionComponent.prototype, "mentionClick", void 0);
    CommentSectionComponent = CommentSectionComponent_1 = __decorate([
        Component({
            selector: 'wrs-comment-section',
            template: "<div *ngIf = \"loading && noteTypeLoading\">\r\n  Loading notes\r\n</div>\r\n<div *ngIf = \"!loading && !noteTypeLoading\">\r\n<div class=\"resolve-switch text-right text-end d-flex align-items-center justify-content-end mb-2\">\r\n  <span class=\"d-inline-block mr-2\">Show resolved notes</span>\r\n  <label class=\"switch\" data-toggle=\"tooltip\" data-toggle=\"tooltip\" title=\"Show resolved notes\">\r\n    <input type=\"checkbox\" [(ngModel)]=\"showResolved\" (click)=\"changeSwitch()\">\r\n    <span class=\"slider round\"></span>\r\n  </label>\r\n</div>\r\n<div  class=\"comment-box-body mt-2\" *ngIf = \"!readOnly && !showResolved\">\r\n  <wrs-comment-box [config]=\"config\" #mainCommentBox  (sent)=\"pushToCommentList($event)\"\r\n  [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, mainCommentBox)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\" [(ngModel)]=\"commentContent\"></wrs-comment-box>\r\n</div>\r\n</div>\r\n\r\n<ng-template  ngFor let-note [ngForOf]=\"commentsTree\" let-i=\"index\" >\r\n  <div class=\"comment-row mb-2 comment-left\" [ngClass]=\"[\r\n  note?.selected ? 'selected-comment': '',\r\n  true ? 'note_' + note.uuid: ''\r\n]\">\r\n  <div  #commentSectionWrapper class=\"comment-column top-comment pt-2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"note.sender?.imageUrl\" title=\"{{note.sender?.name }} ({{note.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"d-inline-block\" *ngIf = \"!note.updateMode && !note.confirmDelete\">\r\n        <!-- <quill-editor  class=\"read-mode\"  name=\"content\" [(ngModel)]=\"note.content\" [trimOnValidation] = \"true\" [maxLength]=\"maxLength\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"note.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"note.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, note)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"note\" [commentContent] = \"note.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"note.confirmDelete\">\r\n        Deleting this note will permanently delete the entire note trail. Are you sure you want to continue?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(note)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\"btn btn-danger\" (click)= \"deleteNote(note)\">\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == note?.userUuid\">\r\n        <button *ngIf = \"!note.updateMode && note.status == 'ACTIVE' && !readOnly && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"editNote(note)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !note.updateMode && !note.confirmDelete\" type=\"button\" class=\"icon-button edit-delete\"  (click)=\"confirmDelete(note)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n        <!-- <div class=\"show-more-less\" (click)=\"note.show = !note.show\"><a href=\"#\" onclick=\"event.preventDefault();\"> {{ note.show ? 'Show less': 'Show More' }}<i *ngIf=\"!note.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"note.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\" >\r\n        <span>{{note.sender?.name}} <span class=\"comment-info\" timeago [date]=\"note.creationDate\" [live]=\"true\"></span></span>\r\n      </span>\r\n    </span>\r\n  </div>\r\n  <span class=\"comment-actions resolve\">\r\n    <button *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button button-resolve\"  (click)=\"resolve(note)\" data-toggle=\"tooltip\" title=\"Mark as Resolve\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n    <span class=\"d-flex-inline comment-user-info\">\r\n      <span class=\"mr-2\" *ngIf=\"note.status == 'RESOLVED'\">Resolved by: {{note.resolved?.name}} <span class=\"comment-info\" timeago [date]=\"note?.resolvedAt\"></span></span>\r\n    <button *ngIf=\"note.status == 'RESOLVED'\" style=\"color: green;\" type=\"button\" class=\"icon-button button-resolved\"   title=\"Resolved\">\r\n      <i class=\"fas fa-check\"></i>\r\n    </button>\r\n  </span>\r\n  </span>\r\n  <span *ngIf = \"note.replies.length > 2 && !note.showAllReplie\" class=\"show-reply\" (click)=\"note.showAllReplie = !note.showAllReplie\"><a href=\"#\" onclick=\"event.preventDefault();\">{{'Show '+(note.replies.length - 1) +' replies' }}<i  class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i></a></span>\r\n  <div class=\"comment-column reply-comment\" *ngFor = \"let replyNote of note?.replies;let replyIndex=index;\" >\r\n    <span  *ngIf = \"replyIndex == note?.replies.length - 1 || note.showAllReplie || note.replies.length <= 2\">\r\n    <span class=\"user-icon\">\r\n      <img  [src]=\"replyNote.sender?.imageUrl\" title=\"{{replyNote.sender?.name }} ({{replyNote.sender?.emailId}})\" class=\"user-image\" alt=\"user-image\">\r\n    </span>\r\n    <span class=\"comment-content card-body\">\r\n      <div class=\"container-show-more d-inline-block\" *ngIf = \"!replyNote.updateMode && !replyNote.confirmDelete\">\r\n        <!-- <quill-editor #replyQuill class=\"read-mode\"  name=\"content\" [(ngModel)]=\"replyNote.content\" [readOnly]=\"true\" [modules]=\"{toolbar:false}\"></quill-editor> -->\r\n        <quill-view-html class=\"read-mode\" [content]=\"replyNote.content\" theme=\"snow\"></quill-view-html>\r\n      </div>\r\n      <div class=\"comment-box-body pt-2\" *ngIf = \"replyNote.updateMode\" >\r\n        <wrs-comment-box [config]=\"config\" #box   (sent)=\"pushWhenEdit($event, replyNote)\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"   [note] = \"replyNote\" [commentContent] = \"replyNote.content\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n      <div class=\"text-center delete-confirm\" *ngIf = \"replyNote.confirmDelete\">\r\n        Are you sure you want to delete this note?\r\n        <div class=\"text-center mt-2\" >\r\n          <button  type=\"button\"  class=\"btn-text btn-light\" (click)=\"confirmDelete(replyNote)\">\r\n            Cancel\r\n          </button>\r\n          <button  type=\"button\" class=\" btn btn-danger \" (click)= \"deleteNote(replyNote)\" >\r\n            Delete\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <span class=\"edit-delete-button\" *ngIf = \"currentUser?.uuid == replyNote?.userUuid\">\r\n        <button *ngIf = \"!replyNote.updateMode && note.status == 'ACTIVE' && !readOnly && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"editNote(replyNote)\" data-toggle=\"tooltip\" title=\"Edit\">\r\n          <i class=\"fa fa-edit\"></i>\r\n        </button>\r\n        <button *ngIf = \"note.status == 'ACTIVE' && !readOnly && !replyNote.updateMode && !replyNote.confirmDelete\" type=\"button\" class=\"icon-button  edit-delete\"  (click)=\"confirmDelete(replyNote)\" data-toggle=\"tooltip\" title=\"Delete\">\r\n          <i class=\"fa fa-trash\"></i>\r\n        </button>\r\n      </span>\r\n      <!-- <div *ngIf = \"strip(replyNote.content).length > 505\" class=\"show-more-less\" (click)=\"replyNote.show = !replyNote.show\"> <a href=\"#\" onclick=\"event.preventDefault();\">  {{ replyNote.show ? 'Show less': 'Show More' }}<i *ngIf=\"!replyNote.show\" class=\"fa fa-angle-down ml-1\" aria-hidden=\"true\"></i><i *ngIf=\"replyNote.show\" class=\"fa fa-angle-up ml-1\" aria-hidden=\"true\"></i></a></div> -->\r\n        <span class=\"comment-user-info\">\r\n          <span>{{replyNote.sender?.name}} <span class=\"comment-info\" timeago [date]=\"replyNote.creationDate\" [live]=\"true\"></span></span>\r\n        </span>\r\n    </span>\r\n  </span>\r\n  </div>\r\n  <span class=\"comment-actions\">\r\n    <button  *ngIf=\"!note.replying && note.status == 'ACTIVE' && !readOnly\" type=\"button\" class=\"icon-button\"   (click)=\"setReplyTo(note)\" title=\"Reply\">\r\n      <i class=\"fa fa-reply\"></i>\r\n    </button>\r\n  </span>\r\n  <div class=\"comment-text\">\r\n    <div>\r\n      <div>\r\n        <wrs-comment-box [config]=\"config\" #box  *ngIf=\"note.replying\" (sent)=\"pushToCommentList($event, note)\" [rootNote]=\"note\"\r\n        [noteType] = \"noteType\" [entityId] = \"entityId\" [clientId]=\"clientId\"  (init)=\"setFocus($event, box)\" (cancel)=\"cancelReply(note)\" [mentionDenotation] = \"mentionDenotation\" (suggest)=\"onSuggest($event)\"></wrs-comment-box>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n</ng-template>",
            providers: [{
                    provide: NG_VALUE_ACCESSOR,
                    useExisting: forwardRef(function () { return CommentSectionComponent_1; }),
                    multi: true
                }, TimeagoDefaultFormatter],
            styles: [".comment-row{display:flex;position:relative}.comment-row:before{content:\"\";left:17px;height:100%;width:1px;background-color:#ccc;position:absolute}.comment-row .comment-row:before{height:1px;width:26px;top:20px;left:-17px}.comment-row .comment-row .comment-row:before{height:1px;width:70px;top:20px;left:-50px}.comment-left{justify-content:flex-start}.selected-comment>.comment-text>.comment{-webkit-animation-name:highlighted-comment;animation-name:highlighted-comment;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-duration:4s;animation-duration:4s}@-webkit-keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}@keyframes highlighted-comment{from{background-color:rgba(21,112,225,.5)}to{background-color:transparent}}.comment{width:100%;min-width:150px;border-radius:2px;display:inline-block;overflow-x:hidden;padding:2px;position:relative;overflow:visible}.comment-user-info{display:flex;text-overflow:ellipsis;font-size:.8rem;justify-content:flex-end}.comment-user-info.show-resolve{justify-content:flex-start}.comment-right,.comment-right .comment-user-info{flex-direction:row-reverse}.comment-content{padding:5px 0;border-radius:2px}.comment-text{width:100%}.comment-actions{color:#6b778c;font-size:.7em;display:flex}.comment-actions>i{cursor:pointer;margin-right:5px}:host ::ng-deep .ui-dialog{display:flex;flex-direction:column;max-height:100vh;max-width:100%;min-width:150px;min-height:150px}:host ::ng-deep .ui-dialog-content{flex-grow:1}:host ::ng-deep .comment-content img{max-width:500px;height:auto;display:block;background-color:#fff}:host ::ng-deep .comment-content p{margin:5px 0}:host ::ng-deep .comment-section-body{max-height:500px;overflow-y:scroll;padding:5px 10px;border-radius:2px;position:relative}.comment-box-body{margin:1em 0;position:relative}.comment-info{color:grey}:host ::ng-deep quill-editor{display:block}:host ::ng-deep .ql-editor:focus{border:1px solid #80bdff!important}:host ::ng-deep .ng-invalid{border:1px dashed red}:host ::ng-deep .mention{cursor:pointer;color:#fff;border-radius:700px;padding:3px 5px;margin:0 5px}:host ::ng-deep .user-image{border:1px solid #cbcbcb;border-radius:50%;width:30px;background-color:#fff;position:relative;height:30px}:host ::ng-deep [data-denotation-char=\"#\"]{background-color:rgba(140,195,75,.8)}:host ::ng-deep [data-denotation-char=\"@\"]{background-color:rgba(0,122,217,.8)}:host ::ng-deep .ql-mention-list [data-denotation-char=\"@\"]{background-color:#fff}:host ::ng-deep .ql-mention-list [data-denotation-char=\"#\"]{background-color:#fff}.unread-row{display:flex;justify-content:center;font-style:italic}.unread-row span{border-radius:700px;border:1px solid #000;padding:3px 10px;font-size:.8em}.rounded-btn{border:1px solid #007ad9;color:#007ad9;background:0 0;border-radius:30px;margin:0 5px;font-size:13px;padding:5px 10px}.rounded-btn:hover{background-color:#177dfb!important;color:#fff}:host ::ng-deep .button-style{display:inline-block;position:relative;text-decoration:none!important;cursor:pointer;text-align:center;overflow:visible}:host ::ng-deep .ql-container.ql-snow .ql-editor{padding:5px 40px 5px 10px;word-break:break-word}:focus{outline:0!important}wrs-comment-section{display:block;background:#eee;padding:10px}.ql-editor{padding:10px}.comment-box-body{display:flex;flex-direction:row}.comment-box-body wrs-comment-box{flex:1}.comment-box-body .more-option{padding-top:7px;width:30px;display:flex;justify-content:center;font-size:18px}.comment-box-body .more-option i{cursor:pointer}.comment-content .comment-box-body{width:calc(100% - 25px)!important;margin-top:5px;padding-top:0!important}.resolved-comments{padding:0 10px 10px}wrs-comment-box{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;display:block}wrs-comment-box .first-comment-box{flex:1}.comment-row wrs-comment-box{display:block}.comment-row{display:flex;flex-direction:column;border-bottom:0;background-color:rgba(255,255,255,.5);padding:10px;box-shadow:0 0 3px rgba(0,0,0,.1);margin-bottom:15px!important;border-radius:5px}.comment-row:before{display:none}.comment-row .comment-column{display:flex;flex-direction:row;border-top:1px solid #eee}.comment-row .comment-column:last-of-type{padding-bottom:10px}.comment-row .comment-column.reply-comment>span{display:flex;flex:1}.comment-row .comment-column.top-comment{border-top-color:transparent!important}.comment-row .comment-column span.comment-user-info{padding:0 5px 15px}.comment-row .comment-column span.user-icon{margin:0 5px;padding-top:10px}.ql-container.ql-snow{border:1px solid #eee!important;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;width:100%}.comment-box-body .ql-container.ql-snow{box-shadow:none;border-radius:7px!important}.ql-container.ql-snow .ql-editor{padding:5px 10px}.comment-actions{position:absolute;right:0;bottom:8px}.comment-actions.resolve{bottom:unset;top:10px;right:0}.send-button{background:#177dfb!important;padding:5px 10px;border:none;color:#fff}.icon-button{color:#177dfb;margin:0 5px;width:27px;height:27px}.icon-button.edit-delete{color:#1c1d1f}.icon-button:hover{background:#dcecfe;color:#177dfb;border-radius:50%}.icon-button.button-resolve{height:27px;width:27px;font-size:15px;border-radius:50%}.edit-delete-button{display:flex;justify-content:flex-end}.resolve-switch .switch{position:relative;display:inline-block;width:40px;height:20px;margin-bottom:0}.resolve-switch .switch input{opacity:0;width:0;height:0}.resolve-switch .slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;transition:.4s}.resolve-switch .slider:before{position:absolute;content:\"\";height:13px;width:13px;left:4px;bottom:4px;background-color:#fff;transition:.4s}.resolve-switch input:checked+.slider{background-color:#177dfb}.resolve-switch input:focus+.slider{box-shadow:0 0 1px #177dfb}.resolve-switch input:checked+.slider:before{transform:translateX(20px)}.resolve-switch .d-inline-block{font-weight:500;vertical-align:middle}.resolve-switch .slider.round{border-radius:34px}.resolve-switch .slider.round:before{border-radius:50%}:host ::ng-deep .ql-editor.ql-blank::before{left:10px;top:7px}:host ::ng-deep wrs-comment-box .ql-container.ql-snow{box-shadow:none}:host ::ng-deep wrs-comment-box .ql-editor{border-radius:0}:host ::ng-deep .read-mode .ql-container.ql-snow{border:0!important;background:0 0;box-shadow:none;padding:0!important;width:100%}:host ::ng-deep .read-mode .ql-container .ql-editor{padding-left:5px}.comment-user-info span{font-size:11px;font-weight:400}.comment-user-info span .comment-info{color:grey;font-style:italic}.icon-button.button-resolved{height:27px;width:27px;font-size:15px;border-radius:50%;margin-top:-5px}.show-reply{padding-left:43px;font-size:13px;font-weight:400}.show-more-less{font-size:13px;font-weight:400}.delete-confirm{padding:10px;border:1px solid #eee;background:#fff;box-shadow:3px 3px 10px rgba(50,50,50,.15);border-radius:9px;margin-bottom:10px;width:calc(100% - 25px)!important;margin-top:5px}.btn.btn-danger{background-color:#bd2130!important;padding:.3rem 1.4rem;height:31px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.btn-text.btn-light{width:unset;border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}"]
        })
    ], CommentSectionComponent);
    return CommentSectionComponent;
}());
export { CommentSectionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1zZWN0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1lbnQtc2VjdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50LXNlY3Rpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2SixPQUFPLEVBQXdCLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFekUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRTVELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUkzQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFnQnREO0lBb0RFLGlDQUNVLGVBQWdDLEVBQ2hDLFlBQXVCLEVBQ3ZCLEdBQXNCO1FBRnRCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxpQkFBWSxHQUFaLFlBQVksQ0FBVztRQUN2QixRQUFHLEdBQUgsR0FBRyxDQUFtQjtRQXREaEMsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFDWCxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBTWYsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUcxQixZQUFPLEdBQVksSUFBSSxDQUFDO1FBQ3hCLG9CQUFlLEdBQVksSUFBSSxDQUFDO1FBRWhDLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQ3pCLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMxQixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLGlCQUFZLEdBQVcsRUFBRSxDQUFDO1FBQzFCLHNCQUFpQixHQUFVLEVBQUUsQ0FBQztRQUM5QixTQUFJLEdBQVksS0FBSyxDQUFDO1FBQ3RCLGFBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIseUJBQW9CLEdBQVksSUFBSSxDQUFDO1FBV3JDLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFHaEIsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFRakIsWUFBTyxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBR2hELGlCQUFZLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckQsMENBQTBDO1FBQzFDLDhCQUE4QjtRQUM5QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQXFQOUIsYUFBUSxHQUFHLFVBQUMsQ0FBQyxJQUFNLENBQUMsQ0FBQztRQUNyQixjQUFTLEdBQUcsY0FBTyxDQUFDLENBQUM7SUFsUGUsQ0FBQztnQ0F2RDFCLHVCQUF1QjtJQXlEbEMsMENBQVEsR0FBUjtRQUNFLDhDQUE4QztRQUM5QyxzQkFBc0I7SUFDeEIsQ0FBQztJQUVELHNEQUFvQixHQUFwQixVQUFxQixNQUFjO1FBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVELG9EQUFrQixHQUFsQjtRQUNFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELDBDQUFRLEdBQVI7UUFBQSxpQkFzQ0M7UUFyQ0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFFdkIsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDO1FBQ3RCLElBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNwQixNQUFNLEdBQUcsVUFBVSxDQUFDO1NBQ3JCO1FBQ0csSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFDLENBQUM7YUFDN0ksU0FBUyxDQUFDLFVBQUEsU0FBUztZQUNsQixLQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFDNUIsSUFBTSxjQUFjLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztZQUN0QyxJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUM7WUFDNUIsSUFBRyxNQUFNLElBQUksVUFBVSxFQUFFO2dCQUN2QixNQUFNLEdBQUcsWUFBWSxDQUFDO2FBQ3ZCO1lBQ0QsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXJFLEtBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDO1lBQzVCLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUN0QixLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7Z0JBQ3hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBRUgsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87Z0JBQ3RCLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtvQkFDcEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzlEO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNqQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFckIsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBQyxVQUFBLEtBQUs7WUFDTCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCw2Q0FBVyxHQUFYO1FBQUEsaUJBVUM7UUFUQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRyxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRyxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFROztZQUM5SCxLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxJQUFJLENBQUM7WUFDbkMsSUFBTSxVQUFVLGVBQUcsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLElBQUksMENBQUUsY0FBYywwQ0FBRSxpQkFBaUIsQ0FBQztZQUNyRSxLQUFJLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDLEVBQUMsVUFBQSxLQUFLO1lBQ04sS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFDTixDQUFDO0lBQ0Qsb0RBQWtCLEdBQWxCO1FBQ0Usc0dBQXNHO1FBQ3RHLHlGQUF5RjtRQUN6Rix1QkFBdUI7UUFDdkIsNEVBQTRFO1FBQzVFLHNEQUFzRDtRQUN0RCw0REFBNEQ7UUFDNUQsNEJBQTRCO1FBQzVCLHlDQUF5QztRQUN6QywwRUFBMEU7UUFDMUUsT0FBTztRQUNQLDRDQUE0QztRQUM1QyxNQUFNO0lBQ1IsQ0FBQztJQUVELDRDQUFVLEdBQVY7SUFFQSxDQUFDO0lBRUQsZ0RBQWMsR0FBZDtRQUNFLG9DQUFvQztRQUNwQyxPQUFPO1FBQ1Asd0hBQXdIO1FBQ3hILHlCQUF5QjtRQUN6Qix5Q0FBeUM7UUFDekMsMEZBQTBGO1FBQzFGLDJGQUEyRjtRQUMzRix5Q0FBeUM7UUFDekMsNkJBQTZCO1FBQzdCLHFCQUFxQjtRQUNyQixvQ0FBb0M7UUFDcEMsUUFBUTtRQUNSLFFBQVE7SUFDVixDQUFDO0lBRUQsNENBQVUsR0FBVixVQUFXLE9BQWE7UUFBeEIsaUJBSUM7UUFIQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQTVDLENBQTRDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELHlDQUFPLEdBQVAsVUFBUSxPQUFPO1FBQWYsaUJBTUM7UUFMQSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxFQUFFLE1BQU0sRUFBRyxJQUFJLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUcsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFFLFVBQUEsSUFBSTtZQUNqSyxLQUFJLENBQUMsWUFBWSxHQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxFQUF6QixDQUF5QixDQUFDLENBQUM7UUFDcEYsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hCLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQTVDLENBQTRDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELG1EQUFpQixHQUFqQixVQUFrQixZQUFrQixFQUFFLE9BQWM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsWUFBWSxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BDLFlBQVksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksT0FBTyxFQUFFO1lBQ1gsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDMUI7UUFDRCxJQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUU7WUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUMxRDthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDdEM7UUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELDhDQUFZLEdBQVosVUFBYSxRQUFjLEVBQUMsT0FBYztRQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDL0IsSUFBRyxRQUFRLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUU7Z0JBQ2hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQzthQUNwQztpQkFBTTtnQkFDUCxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7b0JBQzNCLElBQUcsUUFBUSxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsSUFBSTt3QkFDOUIsS0FBSyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO2dCQUNuQyxDQUFDLENBQUMsQ0FBQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxxREFBbUIsR0FBbkIsVUFBb0IsV0FBaUI7UUFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxJQUFJLElBQUksV0FBVyxDQUFDLElBQUksRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUM5QixPQUFPLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksSUFBSSxXQUFXLENBQUMsSUFBSSxFQUExQixDQUEwQixDQUFDLENBQUM7UUFDN0UsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkNBQVcsR0FBWCxVQUFZLE9BQU87UUFDakIsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVELGlEQUFlLEdBQWYsVUFBZ0IsT0FBYTtRQUE3QixpQkFPQztRQU5DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN0QyxJQUFNLGNBQWMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLElBQUksYUFBYSxDQUFDLElBQUksRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZHLElBQUksY0FBYyxFQUFFO2dCQUNsQixLQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0NBQU0sR0FBTixVQUFPLFNBQWlCLEVBQUUsUUFBUztRQUNqQyxJQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEUsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQixZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRyxLQUFLLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FBQztZQUN4RSxJQUFJLFFBQVEsRUFBRTtnQkFDWixRQUFRLEVBQUUsQ0FBQzthQUNaO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsOENBQVksR0FBWixVQUFhLE9BQWE7UUFDeEIsT0FBTyxPQUFPLEdBQUUsT0FBTyxDQUFDLElBQUksQ0FBQztJQUMvQixDQUFDO0lBRUQsK0NBQWEsR0FBYixVQUFjLE9BQWE7UUFDekIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixVQUFVLENBQUMsY0FBTSxPQUFBLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLEVBQTNCLENBQTJCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELHNCQUFJLG1EQUFjO2FBQWxCO1lBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBRUQsMENBQVEsR0FBUixVQUFTLE1BQU0sRUFBRSxHQUFHO1FBQ2xCLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELDRDQUFVLEdBQVYsVUFBVyxRQUFnQjtRQUN6QixJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNoQyxPQUFPLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsMkNBQVMsR0FBVCxVQUFVLEtBQVU7UUFDbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVELGdEQUFjLEdBQWQ7UUFBQSxpQkFVQztRQVRDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDM0IsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsTUFBTSxLQUFLLEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxFQUFoQyxDQUFnQyxDQUFDLENBQUM7WUFDaEYsSUFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbEQsSUFBSSxXQUFXLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLEVBQUUsTUFBTSxFQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRyxJQUFJLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO29CQUN0SyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2dCQUMvQixDQUFDLENBQUMsQ0FBQzthQUNIO1NBQ0Y7SUFDSCxDQUFDO0lBY0Qsc0JBQUksMENBQUs7UUFaWCwrQkFBK0I7UUFDL0IsK0NBQStDO1FBRS9DLGVBQWU7UUFDZiwyQkFBMkI7UUFDM0IsNEJBQTRCO1FBQzVCLHNIQUFzSDtRQUN0SCwrR0FBK0c7UUFDL0csU0FBUztRQUNULElBQUk7YUFHRixjQUFzQixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBRW5ELFVBQVUsQ0FBUztZQUNqQixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNsQjtRQUNILENBQUM7OztPQVBrRDtJQVNuRCw0Q0FBVSxHQUFWLFVBQVcsS0FBYTtRQUN0QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFJRCxrREFBZ0IsR0FBaEIsVUFBaUIsRUFBb0IsSUFBVSxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEUsbURBQWlCLEdBQWpCLFVBQWtCLEVBQWMsSUFBVSxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFFaEUsNkNBQVcsR0FBWDtRQUNFLGtEQUFrRDtJQUNwRCxDQUFDO0lBRUQsa0RBQWdCLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLFlBQVksR0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDMUMsQ0FBQztJQUVELDhDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUN2QyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVELDRDQUFVLEdBQVYsVUFBVyxVQUFVO0lBQ3JCLENBQUM7SUFFRCx1Q0FBSyxHQUFMLFVBQU0sSUFBSTtRQUNSLDhCQUE4QjtRQUM5QixJQUFJLEdBQUcsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDN0QsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7SUFFckMsQ0FBQztJQUdELDRDQUFVLEdBQVYsVUFBVyxJQUFVO1FBQXJCLGlCQUtDO1FBSkEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUM5RixJQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQzlCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCwwQ0FBUSxHQUFSLFVBQVMsSUFBVTtRQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsK0NBQWEsR0FBYixVQUFjLElBQVU7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0MsQ0FBQzs7O2dCQTdSNEIsZUFBZTtnQkFDbEIsU0FBUztnQkFDbEIsaUJBQWlCOztJQXJEdkI7UUFBUixLQUFLLEVBQUU7MkRBQWdCO0lBR3hCO1FBREMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQyxDQUFDOytEQUNiO0lBRXFCO1FBQXBELFNBQVMsQ0FBQyx1QkFBdUIsRUFBRSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQzswRUFBbUM7SUFDOUU7UUFBUixLQUFLLEVBQUU7NkRBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFO2dFQUFZO0lBZ0JwQjtRQURDLEtBQUssRUFBRTs2REFDUztJQUdqQjtRQURDLEtBQUssRUFBRTs2REFDUztJQUdqQjtRQURDLEtBQUssRUFBRTs2REFDUztJQUtqQjtRQURDLEtBQUssRUFBRTtnRUFDUztJQUdqQjtRQURDLEtBQUssRUFBRTsyREFDYztJQUt0QjtRQURDLE1BQU0sRUFBRTs0REFDdUM7SUFHaEQ7UUFEQyxNQUFNLEVBQUU7aUVBQzRDO0lBL0MxQyx1QkFBdUI7UUFWbkMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQiwrd1NBQStDO1lBRS9DLFNBQVMsRUFBRSxDQUFDO29CQUNWLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLHlCQUF1QixFQUF2QixDQUF1QixDQUFDO29CQUN0RCxLQUFLLEVBQUUsSUFBSTtpQkFDWixFQUFDLHVCQUF1QixDQUFDOztTQUMzQixDQUFDO09BQ1csdUJBQXVCLENBb1ZuQztJQUFELDhCQUFDO0NBQUEsQUFwVkQsSUFvVkM7U0FwVlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJDb250ZW50SW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBmb3J3YXJkUmVmLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENvbW1lbnRCb3hDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnQtYm94L2NvbW1lbnQtYm94LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbW1lbnRzU2VydmljZSB9IGZyb20gJy4vY29tbWVudC1zZWN0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21tZW50VXNlclN0YXRzIH0gZnJvbSAnLi9tb2RlbHMvY29tbWVudC11c2VyLXN0YXRzJztcclxuaW1wb3J0IHsgT3JkZXJQaXBlIH0gZnJvbSAnbmd4LW9yZGVyLXBpcGUnO1xyXG5pbXBvcnQgeyBjb25maWcgfSBmcm9tICdwcm9jZXNzJztcclxuaW1wb3J0ICBkZWJvdW5jZSAgZnJvbSAnbG9kYXNoLWVzL2RlYm91bmNlJztcclxuaW1wb3J0IHsgTm90ZSB9IGZyb20gJy4vbW9kZWxzL25vdGUnO1xyXG5pbXBvcnQgeyBUaW1lYWdvRGVmYXVsdEZvcm1hdHRlciB9IGZyb20gJ25neC10aW1lYWdvJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQ29tbWVudENvbmZpZyB7XHJcbiAgdXJsUHJlZml4OiBzdHJpbmc7ICAgIC8vIHRoZSBwcmVmaXggd2lsbCBiZSBhcHBlbmRlZCBiZWZvcmUgYWxsIHRoZSBjb21tZW50IGVuZHBvaW50cy5cclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICd3cnMtY29tbWVudC1zZWN0aW9uJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudC1zZWN0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50LXNlY3Rpb24uY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFt7XHJcbiAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IENvbW1lbnRTZWN0aW9uQ29tcG9uZW50KSxcclxuICAgIG11bHRpOiB0cnVlXHJcbiAgfSxUaW1lYWdvRGVmYXVsdEZvcm1hdHRlcl1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRTZWN0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlckNvbnRlbnRJbml0LCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcbiAgY29tbWVudENvbnRlbnQgPSAnJztcclxuICBASW5wdXQoKSBmcm96ZW4gPSBmYWxzZTtcclxuXHJcbiAgQFZpZXdDaGlsZCgnbWFpbkNvbW1lbnRCb3gnLCB7c3RhdGljOiBmYWxzZX0pXHJcbiAgY29tbWVudEJveDogQ29tbWVudEJveENvbXBvbmVudDtcclxuXHJcbiAgQFZpZXdDaGlsZCgnY29tbWVudFNlY3Rpb25XcmFwcGVyJywge3N0YXRpYzogZmFsc2V9KSBjb21tZW50U2VjdGlvbldyYXBwZXI6IEVsZW1lbnRSZWY7XHJcbiAgQElucHV0KCkgcmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBjdXJyZW50VXNlclxyXG4gIGRpc3BsYXlDb21tZW50OiBOb3RlO1xyXG4gIGxvYWRpbmc6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIG5vdGVUeXBlTG9hZGluZzogYm9vbGVhbiA9IHRydWU7XHJcbiAgY29tbWVudFVzZXJTdGF0czogQ29tbWVudFVzZXJTdGF0cztcclxuICBjb21tZW50TGlzdDogTm90ZVtdID0gW107XHJcbiAgaXNMYXN0UmVhZFVwZGF0ZWQgPSBmYWxzZTtcclxuICBub3RlczogTm90ZVtdID0gW107XHJcbiAgY29tbWVudHNUcmVlOiBOb3RlW10gPSBbXTtcclxuICBtZW50aW9uRGVub3RhdGlvbjogYW55W10gPSBbXTtcclxuICBzaG93OiBib29sZWFuID0gZmFsc2U7XHJcbiAgcmVhZE5vdGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIG5vdGVUeXBlRGF0YTogYW55O1xyXG4gIGxvYWRDb21tZW50VXNlclN0YXRzOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgQElucHV0KClcclxuICBub3RlVHlwZTogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIGVudGl0eUlkOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgY2xpZW50SWQ6IHN0cmluZztcclxuXHJcbiAgY29tbWVudE1hcCA9IHt9O1xyXG5cclxuICBASW5wdXQoKVxyXG4gIHN1Z2dlc3Rpb25zID0gW107XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgY29uZmlnOiBDb21tZW50Q29uZmlnOyAgLy8gQ29tbWVudCBjb25maWcuIFJlcXVpcmVkLlxyXG5cclxuXHJcblxyXG4gIEBPdXRwdXQoKVxyXG4gIHN1Z2dlc3Q6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBAT3V0cHV0KClcclxuICBtZW50aW9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAvLyBjYmsgPSBkZWJvdW5jZSgoKSA9PiB0aGlzLmludmlldywgMzAwKTtcclxuICAvLyBjYmsgPSAgKCkgPT4gdGhpcy5pbnZpZXcoKTtcclxuICBzaG93UmVzb2x2ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIG9yZGVyU2VydmljZTogT3JkZXJQaXBlLFxyXG4gICAgcHJpdmF0ZSBjZHI6IENoYW5nZURldGVjdG9yUmVmKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICAvLyB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJyx0aGlzLmNiayk7XHJcbiAgICAvLyB0aGlzLmdldE5vdGVUeXBlKCk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVDb21tZW50Q29udGVudChuZXdWYWw6IHN0cmluZykge1xyXG4gICAgdGhpcy53cml0ZVZhbHVlKG5ld1ZhbCk7XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XHJcbiAgICB0aGlzLmdldE5vdGVzKCk7XHJcbiAgICB0aGlzLmdldE5vdGVUeXBlKCk7XHJcbiAgfVxyXG5cclxuICBnZXROb3RlcygpIHtcclxuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcbiAgICB0aGlzLmNvbW1lbnRzVHJlZSA9IFtdO1xyXG5cclxuICAgIGxldCBzdGF0dXMgPSAnQUNUSVZFJztcclxuICAgIGlmKHRoaXMuc2hvd1Jlc29sdmVkKSB7XHJcbiAgICAgIHN0YXR1cyA9ICdSRVNPTFZFRCc7XHJcbiAgICB9XHJcbiAgICAgICAgdGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0Tm90ZXMoe2NvbmZpZzogdGhpcy5jb25maWcsIG5vdGVUeXBlOiB0aGlzLm5vdGVUeXBlLCBlbnRpdHlJZDogdGhpcy5lbnRpdHlJZCwgY2xpZW50SWQgOiB0aGlzLmNsaWVudElkLCBzdGF0dXM6IHN0YXR1c30pXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKGFjY2Vzc09iaiA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm90ZXMgPSBhY2Nlc3NPYmouZGF0YTtcclxuICAgICAgICAgICAgY29uc3QgY29tbWVudFNlY3Rpb24gPSBhY2Nlc3NPYmouZGF0YTtcclxuICAgICAgICAgICAgbGV0IHNvcmRCeSA9ICdjcmVhdGlvbkRhdGUnO1xyXG4gICAgICAgICAgICBpZihzdGF0dXMgPT0gJ1JFU09MVkVEJykge1xyXG4gICAgICAgICAgICAgIHNvcmRCeSA9ICdyZXNvbHZlZEF0JztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBjb21tZW50cyA9IHRoaXMub3JkZXJTZXJ2aWNlLnRyYW5zZm9ybShjb21tZW50U2VjdGlvbiwgc29yZEJ5KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudExpc3QgPSBjb21tZW50cztcclxuICAgICAgICAgICAgY29tbWVudHMuZm9yRWFjaChjb21tZW50ID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRNYXBbY29tbWVudC51dWlkXSA9IGNvbW1lbnQ7XHJcbiAgICAgICAgICAgICAgY29tbWVudC5yZXBsaWVzID0gW107XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgY29tbWVudHMuZm9yRWFjaChjb21tZW50ID0+IHtcclxuICAgICAgICAgICAgICBpZiAoY29tbWVudC5yb290Tm90ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb21tZW50TWFwW2NvbW1lbnQucm9vdE5vdGUudXVpZF0ucmVwbGllcy5wdXNoKGNvbW1lbnQpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRzVHJlZS5wdXNoKGNvbW1lbnQpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGFzdFJlYWQoKTtcclxuICAgICAgICAgIH0sZXJyb3IgPT57XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXROb3RlVHlwZSgpIHtcclxuICAgIHRoaXMubm90ZVR5cGVMb2FkaW5nID0gdHJ1ZTtcclxuICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmdldE5vdGVUeXBlKHsgY29uZmlnIDogdGhpcy5jb25maWcgLG5vdGVUeXBlIDogdGhpcy5ub3RlVHlwZSwgY2xpZW50SWQgOiB0aGlzLmNsaWVudElkfSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5ub3RlVHlwZURhdGEgPSByZXNwb25zZT8uZGF0YTtcclxuICAgICAgY29uc3QgY2hhcmFjdGVycyA9IHJlc3BvbnNlPy5kYXRhPy5ub3RlVHlwZUNvbmZpZz8ubWVudGlvbkNoYXJhY3RlcnM7XHJcbiAgICAgIHRoaXMubWVudGlvbkRlbm90YXRpb24gPSBjaGFyYWN0ZXJzLnNwbGl0KCcsJyk7XHJcbiAgICAgIHRoaXMubm90ZVR5cGVMb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgfSxlcnJvciA9PntcclxuICAgICAgdGhpcy5ub3RlVHlwZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICB9KTtcclxuICB9XHJcbiAgbmdBZnRlclZpZXdDaGVja2VkKCkge1xyXG4gICAgLy8gY29uc29sZS5sb2coJ21lbnRpb24nLCB0aGlzLmNvbW1lbnRTZWN0aW9uV3JhcHBlci5uYXRpdmVFbGVtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ21lbnRpb24nKSk7XHJcbiAgICAvLyBBcnJheS5mcm9tKHRoaXMuY29tbWVudFNlY3Rpb25XcmFwcGVyLm5hdGl2ZUVsZW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnbWVudGlvbicpKVxyXG4gICAgLy8gICAuZm9yRWFjaChlbGVtID0+IHtcclxuICAgIC8vICAgICBjb25zdCBtZW50aW9uQ2hhciA9ICg8YW55PmVsZW0pLmdldEF0dHJpYnV0ZSgnZGF0YS1kZW5vdGF0aW9uLWNoYXInKTtcclxuICAgIC8vICAgICBjb25zdCBpZCA9ICg8YW55PmVsZW0pLmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpO1xyXG4gICAgLy8gICAgIGNvbnN0IHZhbHVlID0gKDxhbnk+ZWxlbSkuZ2V0QXR0cmlidXRlKCdkYXRhLXZhbHVlJyk7XHJcbiAgICAvLyAgICAgY29uc3QgZWxlbVJlZiA9IGVsZW07XHJcbiAgICAvLyAgICAgKDxhbnk+ZWxlbSkub25jbGljayA9IChldmVudCkgPT4ge1xyXG4gICAgLy8gICAgICAgdGhpcy5tZW50aW9uQ2xpY2suZW1pdCh7bWVudGlvbkNoYXIsIGlkLCB2YWx1ZSwgZWxlbVJlZiwgZXZlbnR9KTtcclxuICAgIC8vICAgfTtcclxuICAgIC8vICAgY29uc29sZS5sb2coJ2xpc3Rlcm5lciBzZXQgb24gJywgZWxlbSk7XHJcbiAgICAvLyB9KTtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2UoKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgc2V0U3RhdHNPblZpZXcoKSB7XHJcbiAgICAvLyB0aGlzLmxvYWRDb21tZW50VXNlclN0YXRzID0gdHJ1ZTtcclxuICAgIC8vIGlmKClcclxuICAgIC8vIHRoaXMuY29tbWVudHNTZXJ2aWNlLmdldFVzZXJTdGF0cyh7Y29uZmlnOiB0aGlzLmNvbmZpZywgbm90ZVR5cGUgOiB0aGlzLm5vdGVUeXBlRGF0YS51dWlkICxlbnRpdHlJZCA6IHRoaXMuZW50aXR5SWR9KVxyXG4gICAgLy8gICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgLy8gICAgIHRoaXMuY29tbWVudFVzZXJTdGF0cyA9IGRhdGEuZGF0YTtcclxuICAgIC8vICAgICBjb25zdCBjb21tZW50TGlzdCA9IHRoaXMuY29tbWVudExpc3QuZmlsdGVyKGMgPT4gYy51c2VySWQgIT09IHRoaXMuY3VycmVudFVzZXIuaWQpO1xyXG4gICAgLy8gICAgIGNvbnN0IGNvbW1lbnQgPSBjb21tZW50TGlzdFtjb21tZW50TGlzdC5sZW5ndGggLSB0aGlzLmNvbW1lbnRVc2VyU3RhdHMudW5yZWFkQ291bnRdO1xyXG4gICAgLy8gICAgIHRoaXMubG9hZENvbW1lbnRVc2VyU3RhdHMgPSBmYWxzZTtcclxuICAgIC8vICAgICB0aGlzLnVwZGF0ZUxhc3RSZWFkKCk7XHJcbiAgICAvLyAgICAgaWYgKGNvbW1lbnQpIHtcclxuICAgIC8vICAgICAgIGNvbW1lbnQuZmlyc3RVbnJlYWQgPSB0cnVlO1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRSZXBseVRvKGNvbW1lbnQ6IE5vdGUpIHtcclxuICAgIGNvbW1lbnQucmVwbHlpbmcgPSB0cnVlO1xyXG4gICAgdGhpcy5jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnNjcm9sbCgnY29tbWVudF9xdWlsbF8nICsgY29tbWVudC51dWlkKSwgMTUwKTtcclxuICB9XHJcblxyXG4gIHJlc29sdmUoY29tbWVudCkge1xyXG4gICB0aGlzLmNvbW1lbnRzU2VydmljZS5yZXNvbHZlQ29tbWVudCh7IGNvbmZpZyA6IHRoaXMuY29uZmlnLCBub3RlVHlwZUlkIDogY29tbWVudC5ub3RlVHlwZS5pZCAsZW50aXR5SWQgOiB0aGlzLmVudGl0eUlkLCBub3RlVVVJRCA6IGNvbW1lbnQudXVpZCB9KS5zdWJzY3JpYmUoIGRhdGEgPT4ge1xyXG4gICAgdGhpcy5jb21tZW50c1RyZWUgPSAgdGhpcy5jb21tZW50c1RyZWUuZmlsdGVyKChub3RlKSA9PiBub3RlLnV1aWQgIT0gY29tbWVudC51dWlkKTtcclxuICAgfSk7XHJcbiAgIHRoaXMuY2RyLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5zY3JvbGwoJ2NvbW1lbnRfcXVpbGxfJyArIGNvbW1lbnQudXVpZCksIDE1MCk7XHJcbiAgfVxyXG5cclxuICBwdXNoVG9Db21tZW50TGlzdChhZGRlZENvbW1lbnQ6IE5vdGUsIGNvbW1lbnQ/OiBOb3RlKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRNYXBbYWRkZWRDb21tZW50LnV1aWRdID0gYWRkZWRDb21tZW50O1xyXG4gICAgdGhpcy5jb21tZW50TGlzdC5wdXNoKGFkZGVkQ29tbWVudCk7XHJcbiAgICBhZGRlZENvbW1lbnQucmVwbGllcyA9IFtdO1xyXG4gICAgaWYgKGNvbW1lbnQpIHtcclxuICAgICAgY29tbWVudC5yZXBseWluZyA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKGFkZGVkQ29tbWVudC5yb290Tm90ZSkge1xyXG4gICAgICB0aGlzLmNvbW1lbnRNYXBbY29tbWVudC51dWlkXS5yZXBsaWVzLnB1c2goYWRkZWRDb21tZW50KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY29tbWVudHNUcmVlLnB1c2goYWRkZWRDb21tZW50KTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLnNjcm9sbFRvQ29tbWVudChhZGRlZENvbW1lbnQpO1xyXG4gIH1cclxuXHJcbiAgcHVzaFdoZW5FZGl0KGVkaXROb3RlOiBOb3RlLGNvbW1lbnQ/OiBOb3RlKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzVHJlZS5mb3JFYWNoKGNvbW1lbnQgPT57XHJcbiAgICAgIGlmKGVkaXROb3RlLnV1aWQgPT0gY29tbWVudC51dWlkKSB7XHJcbiAgICAgICAgY29tbWVudC5jb250ZW50ID0gZWRpdE5vdGUuY29udGVudDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgY29tbWVudC5yZXBsaWVzLmZvckVhY2gocmVwbHkgPT57XHJcbiAgICAgICAgaWYoZWRpdE5vdGUudXVpZCA9PSByZXBseS51dWlkKVxyXG4gICAgICAgIHJlcGx5LmNvbnRlbnQgPSBlZGl0Tm90ZS5jb250ZW50O1xyXG4gICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgdGhpcy5jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlVG9Db21tZW50TGlzdChkZWxldGVkTm90ZTogTm90ZSkge1xyXG4gICAgdGhpcy5jb21tZW50c1RyZWUgPSB0aGlzLmNvbW1lbnRzVHJlZS5maWx0ZXIobm90ZSA9PiBub3RlLnV1aWQgIT0gZGVsZXRlZE5vdGUudXVpZCk7XHJcbiAgICB0aGlzLmNvbW1lbnRzVHJlZS5mb3JFYWNoKGNvbW1lbnQgPT57XHJcbiAgICAgICBjb21tZW50LnJlcGxpZXMgPSBjb21tZW50LnJlcGxpZXMuZmlsdGVyKHggPT4geC51dWlkICE9IGRlbGV0ZWROb3RlLnV1aWQpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxSZXBseShjb21tZW50KSB7XHJcbiAgICBjb21tZW50LnJlcGx5aW5nID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBzY3JvbGxUb0NvbW1lbnQoY29tbWVudDogTm90ZSkge1xyXG4gICAgdGhpcy5zY3JvbGwodGhpcy5nZXRDbGFzc05hbWUoY29tbWVudCksICgpID0+IHtcclxuICAgICAgY29uc3QgcmVwbGllZENvbW1lbnQgPSB0aGlzLmNvbW1lbnRMaXN0LmZpbHRlcihjb21tZW50T2JqZWN0ID0+IGNvbW1lbnQudXVpZCA9PSBjb21tZW50T2JqZWN0LnV1aWQpWzBdO1xyXG4gICAgICBpZiAocmVwbGllZENvbW1lbnQpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdENvbW1lbnQocmVwbGllZENvbW1lbnQpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNjcm9sbChjbGFzc05hbWU6IHN0cmluZywgY2FsbGJhY2s/KSB7XHJcbiAgICBjb25zdCBjbGFzc0VsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKGNsYXNzTmFtZSk7XHJcbiAgICBpZiAoY2xhc3NFbGVtZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgY2xhc3NFbGVtZW50WzBdLnNjcm9sbEludG9WaWV3KHsgYmVoYXZpb3I6ICdzbW9vdGgnICwgYmxvY2s6ICdjZW50ZXInfSk7XHJcbiAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldENsYXNzTmFtZShjb21tZW50OiBOb3RlKTogc3RyaW5nIHtcclxuICAgIHJldHVybiAnbm90ZV8nKyBjb21tZW50LnV1aWQ7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RDb21tZW50KGNvbW1lbnQ6IE5vdGUpIHtcclxuICAgIGNvbW1lbnRbJ3NlbGVjdGVkJ10gPSB0cnVlO1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiBjb21tZW50WydzZWxlY3RlZCddID0gZmFsc2UsIDMwMDApO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG1haW5Db21tZW50Qm94KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY29tbWVudEJveDtcclxuICB9XHJcblxyXG4gIHNldEZvY3VzKGVkaXRvciwgYm94KSB7XHJcbiAgICBib3guZWRpdG9yLmdldFF1aWxsKCkuZm9jdXMoKTtcclxuICB9XHJcblxyXG4gIGZvcm1hdERhdGUoZGF0ZV9zdHI6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBkYXRlID0gbmV3IERhdGUoZGF0ZV9zdHIpO1xyXG4gICAgcmV0dXJuIGRhdGUudG9Mb2NhbGVTdHJpbmcoKTtcclxuICB9XHJcblxyXG4gIG9uU3VnZ2VzdChldmVudDogYW55KSB7XHJcbiAgICB0aGlzLnN1Z2dlc3QuZW1pdChldmVudCk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVMYXN0UmVhZCgpIHtcclxuICAgIGlmICghdGhpcy5pc0xhc3RSZWFkVXBkYXRlZCkge1xyXG4gICAgICBjb25zdCBjb21tZW50cyA9IHRoaXMuY29tbWVudExpc3QuZmlsdGVyKGMgPT4gYy51c2VySWQgIT09IHRoaXMuY3VycmVudFVzZXIuaWQpO1xyXG4gICAgICBjb25zdCBsYXN0Q29tbWVudCA9IGNvbW1lbnRzW2NvbW1lbnRzLmxlbmd0aCAtIDFdO1xyXG4gICAgICBpZiAobGFzdENvbW1lbnQpIHtcclxuICAgICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLnVwZGF0ZUxhc3RSZWFkKHsgY29uZmlnIDogdGhpcy5jb25maWcsIG5vdGVUeXBlSWQgOiB0aGlzLm5vdGVUeXBlRGF0YS5pZCAsZW50aXR5SWQgOiB0aGlzLmVudGl0eUlkLCBjb21tZW50VVVJRDogbGFzdENvbW1lbnQudXVpZH0pLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICB0aGlzLmlzTGFzdFJlYWRVcGRhdGVkID0gdHJ1ZTtcclxuICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuLy8gICBpc0VsZW1lbnRJblZpZXdwb3J0IChlbCkge1xyXG4vLyAgICAgY29uc3QgcmVjdCA9IGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG5cclxuLy8gICAgIHJldHVybiAoXHJcbi8vICAgICAgICAgcmVjdC50b3AgPj0gMCAmJlxyXG4vLyAgICAgICAgIHJlY3QubGVmdCA+PSAwICYmXHJcbi8vICAgICAgICAgcmVjdC5ib3R0b20gPD0gKHdpbmRvdy5pbm5lckhlaWdodCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0KSAmJiAvKiBvciAkKHdpbmRvdykuaGVpZ2h0KCkgKi9cclxuLy8gICAgICAgICByZWN0LnJpZ2h0IDw9ICh3aW5kb3cuaW5uZXJXaWR0aCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgpIC8qIG9yICQod2luZG93KS53aWR0aCgpICovXHJcbi8vICAgICApO1xyXG4vLyB9XHJcblxyXG5cclxuICBnZXQgdmFsdWUoKTogc3RyaW5nIHsgcmV0dXJuIHRoaXMuY29tbWVudENvbnRlbnQ7IH1cclxuXHJcbiAgc2V0IHZhbHVlKHY6IHN0cmluZykge1xyXG4gICAgaWYgKHYgIT09IHRoaXMuY29tbWVudENvbnRlbnQpIHtcclxuICAgICAgdGhpcy5jb21tZW50Q29udGVudCA9IHY7XHJcbiAgICAgIHRoaXMub25DaGFuZ2Uodik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuY29tbWVudENvbnRlbnQgPSB2YWx1ZTtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2UgPSAoXykgPT4ge307XHJcbiAgb25Ub3VjaGVkID0gKCkgPT4ge307XHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbjogKF86IGFueSkgPT4gdm9pZCk6IHZvaWQgeyB0aGlzLm9uQ2hhbmdlID0gZm47IH1cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbjogKCkgPT4gdm9pZCk6IHZvaWQgeyB0aGlzLm9uVG91Y2hlZCA9IGZuOyB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgLy8gd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuY2JrKTtcclxuICB9XHJcblxyXG4gIHNob3dSZXNvbHZlZE5vdGUoKSB7XHJcbiAgICB0aGlzLnNob3dSZXNvbHZlZCA9ICAhdGhpcy5zaG93UmVzb2x2ZWQ7XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VTd2l0Y2goKSB7XHJcbiAgICB0aGlzLnNob3dSZXNvbHZlZCA9ICF0aGlzLnNob3dSZXNvbHZlZDtcclxuICAgIHRoaXMuZ2V0Tm90ZXMoKTtcclxuICB9XHJcblxyXG4gIGNoZWNrUXVpbGwocmVwbHlRdWlsbCkge1xyXG4gIH1cclxuXHJcbiAgc3RyaXAoaHRtbCl7XHJcbiAgICAvLyB0aGlzLnNob3dMZXNzQ29udGVudChodG1sKTtcclxuICAgIGxldCBkb2MgPSBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKGh0bWwsICd0ZXh0L2h0bWwnKTtcclxuICAgIHJldHVybiBkb2MuYm9keS50ZXh0Q29udGVudCB8fCBcIlwiO1xyXG5cclxuIH1cclxuXHJcblxyXG4gZGVsZXRlTm90ZShub3RlOiBOb3RlKSB7XHJcbiAgdGhpcy5jb21tZW50c1NlcnZpY2UuZGVsZXRlTm90ZSh7Y29uZmlnOiB0aGlzLmNvbmZpZyxjb21tZW50VVVJRDogbm90ZS51dWlkfSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+e1xyXG4gICAgY29uc3QgY29tbWVudCA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICB0aGlzLmRlbGV0ZVRvQ29tbWVudExpc3QoY29tbWVudCk7XHJcbiAgfSk7XHJcbiB9XHJcblxyXG4gZWRpdE5vdGUobm90ZTogTm90ZSkge1xyXG4gIG5vdGUudXBkYXRlTW9kZSA9ICFub3RlLnVwZGF0ZU1vZGU7XHJcbn1cclxuXHJcbmNvbmZpcm1EZWxldGUobm90ZTogTm90ZSkge1xyXG4gIG5vdGUuY29uZmlybURlbGV0ZSA9ICFub3RlLmNvbmZpcm1EZWxldGU7XHJcbn1cclxuXHJcbn1cclxuIl19