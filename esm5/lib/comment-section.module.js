import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommentSectionComponent } from './comment-section.component';
import { CommentBoxComponent } from './comment-box/comment-box.component';
// import { NgInviewModule } from 'angular-inport';
import { QuillModule } from 'ngx-quill';
import { TimeagoModule } from 'ngx-timeago';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrderModule } from 'ngx-order-pipe';
var CommentSectionModule = /** @class */ (function () {
    function CommentSectionModule() {
    }
    CommentSectionModule = __decorate([
        NgModule({
            declarations: [CommentSectionComponent, CommentBoxComponent],
            imports: [
                CommonModule,
                FormsModule,
                TimeagoModule.forRoot(),
                QuillModule.forRoot(),
                OrderModule
                // NgInviewModule
            ],
            exports: [CommentSectionComponent],
        })
    ], CommentSectionModule);
    return CommentSectionModule;
}());
export { CommentSectionModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1zZWN0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1lbnQtc2VjdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50LXNlY3Rpb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUN0RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUMxRSxtREFBbUQ7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN4QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzVDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBZ0I3QztJQUFBO0lBRUEsQ0FBQztJQUZZLG9CQUFvQjtRQVpoQyxRQUFRLENBQUM7WUFDUixZQUFZLEVBQUUsQ0FBQyx1QkFBdUIsRUFBRSxtQkFBbUIsQ0FBQztZQUM1RCxPQUFPLEVBQUU7Z0JBQ1AsWUFBWTtnQkFDWixXQUFXO2dCQUNYLGFBQWEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZCLFdBQVcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3JCLFdBQVc7Z0JBQ1gsaUJBQWlCO2FBQ2xCO1lBQ0QsT0FBTyxFQUFFLENBQUMsdUJBQXVCLENBQUM7U0FDbkMsQ0FBQztPQUNXLG9CQUFvQixDQUVoQztJQUFELDJCQUFDO0NBQUEsQUFGRCxJQUVDO1NBRlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbWVudFNlY3Rpb25Db21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnQtc2VjdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50Qm94Q29tcG9uZW50IH0gZnJvbSAnLi9jb21tZW50LWJveC9jb21tZW50LWJveC5jb21wb25lbnQnO1xyXG4vLyBpbXBvcnQgeyBOZ0ludmlld01vZHVsZSB9IGZyb20gJ2FuZ3VsYXItaW5wb3J0JztcclxuaW1wb3J0IHsgUXVpbGxNb2R1bGUgfSBmcm9tICduZ3gtcXVpbGwnO1xyXG5pbXBvcnQgeyBUaW1lYWdvTW9kdWxlIH0gZnJvbSAnbmd4LXRpbWVhZ28nO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgT3JkZXJNb2R1bGUgfSBmcm9tICduZ3gtb3JkZXItcGlwZSc7XHJcbmltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbQ29tbWVudFNlY3Rpb25Db21wb25lbnQsIENvbW1lbnRCb3hDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgVGltZWFnb01vZHVsZS5mb3JSb290KCksXHJcbiAgICBRdWlsbE1vZHVsZS5mb3JSb290KCksXHJcbiAgICBPcmRlck1vZHVsZVxyXG4gICAgLy8gTmdJbnZpZXdNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtDb21tZW50U2VjdGlvbkNvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50U2VjdGlvbk1vZHVsZSB7XHJcblxyXG59XHJcbiJdfQ==