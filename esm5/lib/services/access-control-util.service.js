import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AccessControl } from '../models/access-control';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var AccessControlUtilService = /** @class */ (function () {
    function AccessControlUtilService(http) {
        this.http = http;
    }
    AccessControlUtilService.prototype.httpGet = function (url, params) {
        return this.http.get(url, { params: params })
            .pipe(map(function (obj) { return new AccessControl(obj); }));
    };
    AccessControlUtilService.prototype.httpPost = function (url, params) {
        return this.http.post(url, params)
            .pipe(map(function (obj) { return new AccessControl(obj); }));
    };
    AccessControlUtilService.prototype.httpPut = function (url, params) {
        return this.http.put(url, params)
            .pipe(map(function (obj) { return new AccessControl(obj); }));
    };
    AccessControlUtilService.prototype.httpDelete = function (url, params) {
        return this.http.delete(url, { params: params })
            .pipe(map(function (obj) { return new AccessControl(obj); }));
    };
    AccessControlUtilService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    AccessControlUtilService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AccessControlUtilService_Factory() { return new AccessControlUtilService(i0.ɵɵinject(i1.HttpClient)); }, token: AccessControlUtilService, providedIn: "root" });
    AccessControlUtilService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AccessControlUtilService);
    return AccessControlUtilService;
}());
export { AccessControlUtilService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wtdXRpbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbWVudC1zZWN0aW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2FjY2Vzcy1jb250cm9sLXV0aWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXJDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7O0FBS3pEO0lBRUUsa0NBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFBSSxDQUFDO0lBRXpDLDBDQUFPLEdBQVAsVUFBVyxHQUFXLEVBQUUsTUFBWTtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFtQixHQUFHLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQyxDQUFDO2FBQ2xELElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLGFBQWEsQ0FBSSxHQUFHLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDJDQUFRLEdBQVIsVUFBWSxHQUFXLEVBQUUsTUFBWTtRQUNuQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFtQixHQUFHLEVBQUUsTUFBTSxDQUFDO2FBQ2pELElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLGFBQWEsQ0FBSSxHQUFHLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDBDQUFPLEdBQVAsVUFBVyxHQUFXLEVBQUUsTUFBWTtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFtQixHQUFHLEVBQUUsTUFBTSxDQUFDO2FBQ2hELElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLGFBQWEsQ0FBSSxHQUFHLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDZDQUFVLEdBQVYsVUFBYyxHQUFXLEVBQUUsTUFBWTtRQUNyQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFtQixHQUFHLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQyxDQUFDO2FBQ3JELElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLGFBQWEsQ0FBSSxHQUFHLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Z0JBcEJ5QixVQUFVOzs7SUFGekIsd0JBQXdCO1FBSHBDLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyx3QkFBd0IsQ0F1QnBDO21DQWhDRDtDQWdDQyxBQXZCRCxJQXVCQztTQXZCWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBY2Nlc3NDb250cm9sIH0gZnJvbSAnLi4vbW9kZWxzL2FjY2Vzcy1jb250cm9sJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEFjY2Vzc0NvbnRyb2xVdGlsU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XHJcblxyXG4gIGh0dHBHZXQ8VD4odXJsOiBzdHJpbmcsIHBhcmFtcz86IGFueSk6IE9ic2VydmFibGU8QWNjZXNzQ29udHJvbDxUPj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8QWNjZXNzQ29udHJvbDxUPj4odXJsLCB7cGFyYW1zfSlcclxuICAgICAgLnBpcGUobWFwKG9iaiA9PiBuZXcgQWNjZXNzQ29udHJvbDxUPihvYmopKSk7XHJcbiAgfVxyXG5cclxuICBodHRwUG9zdDxUPih1cmw6IHN0cmluZywgcGFyYW1zPzogYW55KTogT2JzZXJ2YWJsZTxBY2Nlc3NDb250cm9sPFQ+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8QWNjZXNzQ29udHJvbDxUPj4odXJsLCBwYXJhbXMpXHJcbiAgICAgIC5waXBlKG1hcChvYmogPT4gbmV3IEFjY2Vzc0NvbnRyb2w8VD4ob2JqKSkpO1xyXG4gIH1cclxuXHJcbiAgaHR0cFB1dDxUPih1cmw6IHN0cmluZywgcGFyYW1zPzogYW55KTogT2JzZXJ2YWJsZTxBY2Nlc3NDb250cm9sPFQ+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLnB1dDxBY2Nlc3NDb250cm9sPFQ+Pih1cmwsIHBhcmFtcylcclxuICAgICAgLnBpcGUobWFwKG9iaiA9PiBuZXcgQWNjZXNzQ29udHJvbDxUPihvYmopKSk7XHJcbiAgfVxyXG5cclxuICBodHRwRGVsZXRlPFQ+KHVybDogc3RyaW5nLCBwYXJhbXM/OiBhbnkpOiBPYnNlcnZhYmxlPEFjY2Vzc0NvbnRyb2w8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlPEFjY2Vzc0NvbnRyb2w8VD4+KHVybCwge3BhcmFtc30pXHJcbiAgICAgIC5waXBlKG1hcChvYmogPT4gbmV3IEFjY2Vzc0NvbnRyb2w8VD4ob2JqKSkpO1xyXG4gIH1cclxufVxyXG4iXX0=