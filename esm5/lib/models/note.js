var Note = /** @class */ (function () {
    function Note() {
        this.updateMode = false;
        this.confirmDelete = false;
    }
    return Note;
}());
export { Note };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1lbnQtc2VjdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbm90ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUFBO1FBc0JFLGVBQVUsR0FBYSxLQUFLLENBQUM7UUFDN0Isa0JBQWEsR0FBYSxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUFELFdBQUM7QUFBRCxDQUFDLEFBeEJELElBd0JDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE5vdGUge1xyXG4gIHV1aWQ/OiBzdHJpbmc7XHJcbiAgb3JnSWQ/OiBudW1iZXI7XHJcbiAgb3JnVXVpZD86IHN0cmluZztcclxuICBjb250ZW50Pzogc3RyaW5nO1xyXG4gIGFsbENvbnRlbnQ/OiBzdHJpbmc7XHJcbiAgdXNlcklkPzogbnVtYmVyO1xyXG4gIHVzZXJVdWlkPzogc3RyaW5nO1xyXG4gIHJvb3ROb3RlPzogTm90ZTtcclxuICBlbnRpdHlJZD86IHN0cmluZztcclxuICBjcmVhdGVkQnk/OiBudW1iZXI7XHJcbiAgY3JlYXRlZERhdGU/OiBzdHJpbmc7XHJcbiAgbGFzdE1vZGlmaWVkQnk/OiBudW1iZXI7XHJcbiAgbGFzdE1vZGlmaWVkRGF0ZT86IHN0cmluZztcclxuICByZXNvbHZlZEJ5PzogbnVtYmVyO1xyXG4gIHJlc29sdmVkQXQ/OiBzdHJpbmc7XHJcbiAgc2VuZGVyPzogYW55O1xyXG4gIHJlcGxpZXM/OiBOb3RlW107XHJcbiAgZmlyc3RVbnJlYWQ/OiBib29sZWFuO1xyXG4gIHJlcGx5aW5nPzogYm9vbGVhbjtcclxuICBzaG93PzogYm9vbGVhbjtcclxuICBzaG93QWxsUmVwbGllPzogYm9vbGVhbjtcclxuICB1cGRhdGVNb2RlPzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGNvbmZpcm1EZWxldGU/OiBib29sZWFuID0gZmFsc2U7XHJcbn1cclxuIl19