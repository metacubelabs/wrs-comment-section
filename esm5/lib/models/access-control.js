var AccessControl = /** @class */ (function () {
    function AccessControl(obj) {
        this.data = obj && obj.data;
        this.permissions = obj && obj.permissions;
    }
    Object.defineProperty(AccessControl.prototype, "read", {
        get: function () {
            return this.permissions ? this.permissions[0].split(',').includes('read') : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccessControl.prototype, "write", {
        get: function () {
            return this.permissions ? this.permissions[0].split(',').includes('write') : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccessControl.prototype, "delete", {
        get: function () {
            return this.permissions ? this.permissions[0].split(',').includes('delete') : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccessControl.prototype, "edit", {
        get: function () {
            return this.permissions ? this.permissions[0].split(',').includes('edit') : false;
        },
        enumerable: true,
        configurable: true
    });
    AccessControl.prototype.hasPermissionFor = function (access) {
        return this.permissions ? this.permissions[0].split(',').includes(access) : false;
    };
    return AccessControl;
}());
export { AccessControl };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tZW50LXNlY3Rpb24vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2FjY2Vzcy1jb250cm9sLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBSUksdUJBQVksR0FBUTtRQUNoQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLENBQUM7SUFDOUMsQ0FBQztJQUVELHNCQUFJLCtCQUFJO2FBQVI7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3RGLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0NBQUs7YUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDdkYsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpQ0FBTTthQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN4RixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLCtCQUFJO2FBQVI7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3RGLENBQUM7OztPQUFBO0lBRUQsd0NBQWdCLEdBQWhCLFVBQWlCLE1BQWM7UUFDM0IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN0RixDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBNUJELElBNEJDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEFjY2Vzc0NvbnRyb2w8VD4ge1xyXG4gICAgZGF0YTogVDtcclxuICAgIHBlcm1pc3Npb25zOiBzdHJpbmdbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo6IGFueSkge1xyXG4gICAgICAgIHRoaXMuZGF0YSA9IG9iaiAmJiBvYmouZGF0YTtcclxuICAgICAgICB0aGlzLnBlcm1pc3Npb25zID0gb2JqICYmIG9iai5wZXJtaXNzaW9ucztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcmVhZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wZXJtaXNzaW9ucyA/IHRoaXMucGVybWlzc2lvbnNbMF0uc3BsaXQoJywnKS5pbmNsdWRlcygncmVhZCcpIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHdyaXRlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBlcm1pc3Npb25zID8gdGhpcy5wZXJtaXNzaW9uc1swXS5zcGxpdCgnLCcpLmluY2x1ZGVzKCd3cml0ZScpIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlbGV0ZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wZXJtaXNzaW9ucyA/IHRoaXMucGVybWlzc2lvbnNbMF0uc3BsaXQoJywnKS5pbmNsdWRlcygnZGVsZXRlJykgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZWRpdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wZXJtaXNzaW9ucyA/IHRoaXMucGVybWlzc2lvbnNbMF0uc3BsaXQoJywnKS5pbmNsdWRlcygnZWRpdCcpIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzUGVybWlzc2lvbkZvcihhY2Nlc3M6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBlcm1pc3Npb25zID8gdGhpcy5wZXJtaXNzaW9uc1swXS5zcGxpdCgnLCcpLmluY2x1ZGVzKGFjY2VzcykgOiBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=