import { __decorate } from "tslib";
import { Component, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommentsService } from '../comment-section.service';
import { QuillEditorComponent } from 'ngx-quill';
import { finalize } from 'rxjs/operators';
import 'quill-mention';
var CommentBoxComponent = /** @class */ (function () {
    function CommentBoxComponent(commentsService) {
        this.commentsService = commentsService;
        this.commentContent = '';
        this.mentionDenotation = [];
        this.sent = new EventEmitter();
        this.cancel = new EventEmitter();
        this.init = new EventEmitter();
        this.onContentChange = new EventEmitter();
        this.suggestions = [];
        this.suggest = new EventEmitter();
        this.modules = null;
        this.user = JSON.parse(localStorage.getItem('user'));
        this.formAction = false;
        this.onChange = function (_) { };
        this.onTouched = function () { };
    }
    CommentBoxComponent_1 = CommentBoxComponent;
    CommentBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getNoteType();
        this.modules = {
            mention: {
                allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
                // TODO: User has to provide the denotation characters
                mentionDenotationChars: this.mentionDenotation,
                onSelect: function (item, insertItem) {
                    var editor = _this.editor.quillEditor;
                    insertItem(item);
                    // necessary because quill-mention triggers changes as 'api' instead of 'user'
                    editor.insertText(editor.getLength() - 1, '', 'user');
                },
                source: function (searchTerm, renderList, mentionChar) {
                    var event = {
                        query: searchTerm,
                        mentionChar: mentionChar,
                        renderList: renderList
                    };
                    // TODO: Document it for users
                    _this.suggest.emit(event);
                },
                // TODO: Provide a map of denotation characters and functions
                renderItem: function (item, searchTerm) {
                    return item.value;
                }
            },
            toolbar: []
        };
    };
    CommentBoxComponent.prototype.ngAfterViewInit = function () {
        // this.user['name'] = this.user['full_name'];
    };
    CommentBoxComponent.prototype.getNoteType = function () {
        var _this = this;
        this.commentsService.getNoteType({ config: this.config, noteType: this.noteType, clientId: this.clientId }).subscribe(function (response) {
            var _a, _b;
            var characters = (_b = (_a = response === null || response === void 0 ? void 0 : response.data) === null || _a === void 0 ? void 0 : _a.noteTypeConfig) === null || _b === void 0 ? void 0 : _b.mentionCharacters;
            _this.mentionDenotation = characters.split(',');
        });
    };
    CommentBoxComponent.prototype.updateContent = function () {
        this.writeValue(this.commentContent);
    };
    CommentBoxComponent.prototype.doComment = function () {
        var _this = this;
        if (!this.commentContent.trim()) {
            return false;
        }
        this.formAction = true;
        if (this.rootNote) {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, rootNote: { uuid: this.rootNote.uuid }, clientId: this.clientId })
                .pipe(finalize(function () { return _this.resetState(); }))
                .subscribe(function (accessObj) {
                var comment = accessObj.data;
                _this.sent.emit(comment);
            });
        }
        else {
            this.commentsService.addComment({ config: this.config, comment: this.commentContent, noteType: this.noteType, entityId: this.entityId, clientId: this.clientId })
                .pipe(finalize(function () { return _this.resetState(); }))
                .subscribe(function (accessObj) {
                var comment = accessObj.data;
                comment['user'] = _this.user;
                _this.sent.emit(comment);
            });
        }
    };
    CommentBoxComponent.prototype.cancelComment = function () {
        this.cancel.emit(true);
    };
    CommentBoxComponent.prototype.resetState = function () {
        this.commentContent = null;
        this.formAction = false;
    };
    CommentBoxComponent.prototype.inititalized = function (editor) {
        this.init.emit(editor);
    };
    Object.defineProperty(CommentBoxComponent.prototype, "value", {
        get: function () {
            return this.commentContent;
        },
        set: function (v) {
            if (v !== this.commentContent) {
                this.commentContent = v;
                this.onChange(v);
            }
        },
        enumerable: true,
        configurable: true
    });
    CommentBoxComponent.prototype.writeValue = function (value) {
        this.commentContent = value;
        this.onContentChange.emit(this.commentContent);
        this.onChange(value);
    };
    CommentBoxComponent.prototype.updateNote = function (note) {
        var _this = this;
        this.commentsService.updateNote({ config: this.config, comment: this.commentContent, commentUUID: note.uuid }).subscribe(function (response) {
            var comment = response.data;
            note.updateMode = !note.updateMode;
            _this.sent.emit(comment);
        });
    };
    CommentBoxComponent.prototype.editNote = function (note) {
        note.updateMode = !note.updateMode;
    };
    CommentBoxComponent.prototype.registerOnChange = function (fn) { this.onChange = fn; };
    CommentBoxComponent.prototype.registerOnTouched = function (fn) { this.onTouched = fn; };
    var CommentBoxComponent_1;
    CommentBoxComponent.ctorParameters = function () { return [
        { type: CommentsService }
    ]; };
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "commentContent", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "note", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "rootNote", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "noteType", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "entityId", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "clientId", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "mentionDenotation", void 0);
    __decorate([
        ViewChild(QuillEditorComponent, { static: false })
    ], CommentBoxComponent.prototype, "editor", void 0);
    __decorate([
        Output()
    ], CommentBoxComponent.prototype, "sent", void 0);
    __decorate([
        Output()
    ], CommentBoxComponent.prototype, "cancel", void 0);
    __decorate([
        Output()
    ], CommentBoxComponent.prototype, "init", void 0);
    __decorate([
        Output()
    ], CommentBoxComponent.prototype, "onContentChange", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "suggestions", void 0);
    __decorate([
        Input()
    ], CommentBoxComponent.prototype, "config", void 0);
    __decorate([
        Output()
    ], CommentBoxComponent.prototype, "suggest", void 0);
    CommentBoxComponent = CommentBoxComponent_1 = __decorate([
        Component({
            selector: 'wrs-comment-box',
            template: "<div class=\"first-comment-box\">\r\n  <quill-editor  #editor\r\n    [(ngModel)]=\"commentContent\" name=\"commentContent\" [style]=\"{'height':'150px'}\" [disabled]=\"formAction\"\r\n    placeholder=\"Enter note\" (onInit)=\"inititalized($event)\" [modules]=\"modules\"\r\n    (onContentChanged)=\"updateContent()\"></quill-editor>\r\n</div>\r\n<div class=\"text-right text-end mt-2\">\r\n  <button  type=\"button\" class=\"icon-button btn-light\" *ngIf=\"rootNote\"\r\n    (click)=\"cancelComment()\" [disabled]=\"formAction\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"rootNote\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n\r\n  <button  type=\"button\" class=\"send-button\" *ngIf=\"!rootNote && !note?.updateMode\" (click)=\"doComment()\"\r\n    [disabled]=\"formAction\" >\r\n    Note\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>\r\n<div class=\"text-right text-end mt-2\" *ngIf = \"note?.updateMode\">\r\n  <button  type=\"button\" (click) = \"editNote(note)\" class=\"icon-button btn-light\">\r\n    <!-- <i class=\"fas fa-times mr-1\"></i>Close -->\r\n    Cancel\r\n  </button>\r\n  <button  type=\"button\" class=\"send-button\" (click) = \"updateNote(note)\">\r\n    save\r\n    <i class=\"fab fa-telegram-plane mr-1\"></i>\r\n  </button>\r\n</div>",
            providers: [{
                    provide: NG_VALUE_ACCESSOR,
                    useExisting: forwardRef(function () { return CommentBoxComponent_1; }),
                    multi: true
                }],
            styles: [":host ::ng-deep .ql-toolbar.ql-snow{display:none}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-top:1px solid #c8c8c8}:host ::ng-deep .ql-editor{min-height:50px}:host ::ng-deep .ui-send-comment{background:#177dfb!important;border-color:#177dfb!important;border-radius:26px;font-size:14px;margin:5px;transition:.3s;color:#fff;padding:5px 10px}:host ::ng-deep .ui-send-comment:hover{background:0 0!important;border-color:#177dfb!important;color:#177dfb!important}:host ::ng-deep .ui-send-comment-cancel{background:#177dfb!important}:host ::ng-deep .ql-mention-list-container{width:270px;border:1px solid #f0f0f0;border-radius:4px;background-color:#fff;box-shadow:0 2px 12px 0 rgba(30,30,30,.08);z-index:9001;max-height:150px;overflow-y:scroll}:host ::ng-deep .ql-mention-list{list-style:none;margin:0;padding:0;overflow:hidden}:host ::ng-deep .ql-mention-list-item{cursor:pointer;height:40px;line-height:40px;font-size:14px;padding:0 20px;border-bottom:1px solid #e4e4e4;vertical-align:middle}:host ::ng-deep .ql-mention-list-item.selected{background-color:#d3e1eb;text-decoration:none}:host ::ng-deep .mention{height:24px;width:65px;border-radius:6px;background-color:#177dfb;padding:3px 0;margin-right:2px}:host ::ng-deep .mention>span{margin:0 3px}:host ::ng-deep .ql-toolbar.ql-snow+.ql-container.ql-snow{border-radius:5px}.send-button{background:#177dfb!important;padding:7px 10px;border:none;color:#fff;border-radius:3px;margin-left:10px}.icon-button{border:none;font-size:14px;background:0 0;cursor:pointer}.icon-button.btn-light{border:0;font-size:14px;background:#e7e7e7;cursor:pointer;padding:7px 10px;border:none;color:#212529;border-radius:3px;margin-left:10px;background-color:transparent!important}.icon-button.button-resolve{background:#177dfb;color:#fff;position:absolute;bottom:30px;right:12px;border-radius:0 14px 14px 0;height:40px}"]
        })
    ], CommentBoxComponent);
    return CommentBoxComponent;
}());
export { CommentBoxComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1ib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbWVudC1zZWN0aW9uLyIsInNvdXJjZXMiOlsibGliL2NvbW1lbnQtYm94L2NvbW1lbnQtYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFpQixTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNySCxPQUFPLEVBQXdCLGlCQUFpQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFekUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzdELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUNqRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUMsT0FBTyxlQUFlLENBQUM7QUFhdkI7SUFpQ0UsNkJBQW9CLGVBQWdDO1FBQWhDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQS9CM0MsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFNcEIsc0JBQWlCLEdBQVUsRUFBRSxDQUFDO1FBSTdCLFNBQUksR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDO1FBQ2hDLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBQ3JDLFNBQUksR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9CLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUdwRCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQU1qQixZQUFPLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFaEQsWUFBTyxHQUFHLElBQUksQ0FBQztRQUVmLFNBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUVoRCxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBb0huQixhQUFRLEdBQUcsVUFBQyxDQUFDLElBQU0sQ0FBQyxDQUFDO1FBQ3JCLGNBQVMsR0FBRyxjQUFPLENBQUMsQ0FBQztJQWxIbUMsQ0FBQzs0QkFqQzlDLG1CQUFtQjtJQW1DOUIsc0NBQVEsR0FBUjtRQUFBLGlCQTZCQztRQTVCQyxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRztZQUNiLE9BQU8sRUFBRTtnQkFDUCxZQUFZLEVBQUUscUJBQXFCO2dCQUNuQyxzREFBc0Q7Z0JBQ3RELHNCQUFzQixFQUFFLElBQUksQ0FBQyxpQkFBaUI7Z0JBQ2xELFFBQVEsRUFBRSxVQUFDLElBQUksRUFBRSxVQUFVO29CQUN6QixJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQW9CLENBQUM7b0JBQ2hELFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDakIsOEVBQThFO29CQUM5RSxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN4RCxDQUFDO2dCQUNELE1BQU0sRUFBRSxVQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVztvQkFDMUMsSUFBTSxLQUFLLEdBQUc7d0JBQ1osS0FBSyxFQUFFLFVBQVU7d0JBQ2pCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixVQUFVLEVBQUUsVUFBVTtxQkFDdkIsQ0FBQztvQkFDRiw4QkFBOEI7b0JBQzlCLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMzQixDQUFDO2dCQUNELDZEQUE2RDtnQkFDN0QsVUFBVSxFQUFFLFVBQUMsSUFBSSxFQUFFLFVBQVU7b0JBQzNCLE9BQVEsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDckIsQ0FBQzthQUNGO1lBQ0QsT0FBTyxFQUFFLEVBQUU7U0FDVixDQUFBO0lBQ0QsQ0FBQztJQUNELDZDQUFlLEdBQWY7UUFDRSw4Q0FBOEM7SUFDaEQsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFBQSxpQkFLQztRQUpDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUMsUUFBUSxFQUFHLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7O1lBQzdILElBQU0sVUFBVSxlQUFHLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxJQUFJLDBDQUFFLGNBQWMsMENBQUUsaUJBQWlCLENBQUM7WUFDckUsS0FBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDTixDQUFDO0lBRUQsMkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx1Q0FBUyxHQUFUO1FBQUEsaUJBc0JDO1FBckJDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQy9CLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUV2QixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRyxRQUFRLEVBQUcsRUFBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNwTSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxFQUFFLEVBQWpCLENBQWlCLENBQUMsQ0FBQztpQkFDdkMsU0FBUyxDQUFDLFVBQUEsU0FBUztnQkFDbEIsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDL0IsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUM1SixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxFQUFFLEVBQWpCLENBQWlCLENBQUMsQ0FBQztpQkFDdkMsU0FBUyxDQUFDLFVBQUEsU0FBUztnQkFDbEIsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDL0IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsMkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCx3Q0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVELDBDQUFZLEdBQVosVUFBYSxNQUFNO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxzQkFBSSxzQ0FBSzthQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzdCLENBQUM7YUFFRCxVQUFVLENBQVM7WUFDakIsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEI7UUFDSCxDQUFDOzs7T0FQQTtJQVNELHdDQUFVLEdBQVYsVUFBVyxLQUFhO1FBQ3RCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsSUFBVTtRQUFyQixpQkFNRTtRQUxBLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDNUgsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNuQyxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxzQ0FBUSxHQUFSLFVBQVMsSUFBVTtRQUNoQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN0QyxDQUFDO0lBSUYsOENBQWdCLEdBQWhCLFVBQWlCLEVBQW9CLElBQVUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3BFLCtDQUFpQixHQUFqQixVQUFrQixFQUFjLElBQVUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7Z0JBcEgzQixlQUFlOztJQS9CM0M7UUFBUixLQUFLLEVBQUU7K0RBQXFCO0lBQ3BCO1FBQVIsS0FBSyxFQUFFO3FEQUFZO0lBQ1g7UUFBUixLQUFLLEVBQUU7eURBQWdCO0lBQ2Y7UUFBUixLQUFLLEVBQUU7eURBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFO3lEQUFrQjtJQUNqQjtRQUFSLEtBQUssRUFBRTt5REFBa0I7SUFDakI7UUFBUixLQUFLLEVBQUU7a0VBQStCO0lBQ1c7UUFBakQsU0FBUyxDQUFDLG9CQUFvQixFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQyxDQUFDO3VEQUE4QjtJQUdyRTtRQUFULE1BQU0sRUFBRTtxREFBaUM7SUFDaEM7UUFBVCxNQUFNLEVBQUU7dURBQXNDO0lBQ3JDO1FBQVQsTUFBTSxFQUFFO3FEQUFnQztJQUMvQjtRQUFULE1BQU0sRUFBRTtnRUFBMkM7SUFHcEQ7UUFEQyxLQUFLLEVBQUU7NERBQ1M7SUFHakI7UUFEQyxLQUFLLEVBQUU7dURBQ2M7SUFHdEI7UUFEQyxNQUFNLEVBQUU7d0RBQ3VDO0lBeEJyQyxtQkFBbUI7UUFWL0IsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixpK0NBQTJDO1lBRTNDLFNBQVMsRUFBRSxDQUFDO29CQUNWLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLHFCQUFtQixFQUFuQixDQUFtQixDQUFDO29CQUNsRCxLQUFLLEVBQUUsSUFBSTtpQkFDWixDQUFDOztTQUNILENBQUM7T0FDVyxtQkFBbUIsQ0FzSi9CO0lBQUQsMEJBQUM7Q0FBQSxBQXRKRCxJQXNKQztTQXRKWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBDb21tZW50Q29uZmlnIH0gZnJvbSAnLi4vY29tbWVudC1zZWN0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbW1lbnRzU2VydmljZSB9IGZyb20gJy4uL2NvbW1lbnQtc2VjdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUXVpbGxFZGl0b3JDb21wb25lbnQgfSBmcm9tICduZ3gtcXVpbGwnO1xyXG5pbXBvcnQgeyBmaW5hbGl6ZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IFF1aWxsIGZyb20gJ3F1aWxsJztcclxuaW1wb3J0ICdxdWlsbC1tZW50aW9uJztcclxuaW1wb3J0IHsgTm90ZSB9IGZyb20gJy4uL21vZGVscy9ub3RlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnd3JzLWNvbW1lbnQtYm94JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudC1ib3guY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1lbnQtYm94LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbe1xyXG4gICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBDb21tZW50Qm94Q29tcG9uZW50KSxcclxuICAgIG11bHRpOiB0cnVlXHJcbiAgfV1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRCb3hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuXHJcbiAgQElucHV0KCkgY29tbWVudENvbnRlbnQgPSAnJztcclxuICBASW5wdXQoKSBub3RlOiBOb3RlO1xyXG4gIEBJbnB1dCgpIHJvb3ROb3RlOiBOb3RlO1xyXG4gIEBJbnB1dCgpIG5vdGVUeXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgZW50aXR5SWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBjbGllbnRJZDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lbnRpb25EZW5vdGF0aW9uOiBhbnlbXSA9IFtdO1xyXG4gIEBWaWV3Q2hpbGQoUXVpbGxFZGl0b3JDb21wb25lbnQsIHtzdGF0aWM6IGZhbHNlfSkgZWRpdG9yOiBRdWlsbEVkaXRvckNvbXBvbmVudDtcclxuXHJcblxyXG4gIEBPdXRwdXQoKSBzZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxOb3RlPigpO1xyXG4gIEBPdXRwdXQoKSBjYW5jZWwgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcbiAgQE91dHB1dCgpIGluaXQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgb25Db250ZW50Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc3VnZ2VzdGlvbnMgPSBbXTtcclxuXHJcbiAgQElucHV0KClcclxuICBjb25maWc6IENvbW1lbnRDb25maWc7XHJcblxyXG4gIEBPdXRwdXQoKVxyXG4gIHN1Z2dlc3Q6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBtb2R1bGVzID0gbnVsbDtcclxuXHJcbiAgdXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXInKSk7XHJcblxyXG4gIGZvcm1BY3Rpb24gPSBmYWxzZTtcclxuICBtZW50aW9uQ2hhcnM6IGFueTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb21tZW50c1NlcnZpY2U6IENvbW1lbnRzU2VydmljZSkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy8gdGhpcy5nZXROb3RlVHlwZSgpO1xyXG4gICAgdGhpcy5tb2R1bGVzID0ge1xyXG4gICAgICBtZW50aW9uOiB7XHJcbiAgICAgICAgYWxsb3dlZENoYXJzOiAvXltBLVphLXpcXHPDhcOEw5bDpcOkw7ZdKiQvLFxyXG4gICAgICAgIC8vIFRPRE86IFVzZXIgaGFzIHRvIHByb3ZpZGUgdGhlIGRlbm90YXRpb24gY2hhcmFjdGVyc1xyXG4gICAgICAgIG1lbnRpb25EZW5vdGF0aW9uQ2hhcnM6IHRoaXMubWVudGlvbkRlbm90YXRpb24sXHJcbiAgICBvblNlbGVjdDogKGl0ZW0sIGluc2VydEl0ZW0pID0+IHtcclxuICAgICAgY29uc3QgZWRpdG9yID0gdGhpcy5lZGl0b3IucXVpbGxFZGl0b3IgYXMgUXVpbGw7XHJcbiAgICAgIGluc2VydEl0ZW0oaXRlbSk7XHJcbiAgICAgIC8vIG5lY2Vzc2FyeSBiZWNhdXNlIHF1aWxsLW1lbnRpb24gdHJpZ2dlcnMgY2hhbmdlcyBhcyAnYXBpJyBpbnN0ZWFkIG9mICd1c2VyJ1xyXG4gICAgICBlZGl0b3IuaW5zZXJ0VGV4dChlZGl0b3IuZ2V0TGVuZ3RoKCkgLSAxLCAnJywgJ3VzZXInKTtcclxuICAgIH0sXHJcbiAgICBzb3VyY2U6IChzZWFyY2hUZXJtLCByZW5kZXJMaXN0LCBtZW50aW9uQ2hhcikgPT4ge1xyXG4gICAgICBjb25zdCBldmVudCA9IHtcclxuICAgICAgICBxdWVyeTogc2VhcmNoVGVybSxcclxuICAgICAgICBtZW50aW9uQ2hhcjogbWVudGlvbkNoYXIsXHJcbiAgICAgICAgcmVuZGVyTGlzdDogcmVuZGVyTGlzdFxyXG4gICAgICB9O1xyXG4gICAgICAvLyBUT0RPOiBEb2N1bWVudCBpdCBmb3IgdXNlcnNcclxuICAgICAgdGhpcy5zdWdnZXN0LmVtaXQoZXZlbnQpO1xyXG4gICAgfSxcclxuICAgIC8vIFRPRE86IFByb3ZpZGUgYSBtYXAgb2YgZGVub3RhdGlvbiBjaGFyYWN0ZXJzIGFuZCBmdW5jdGlvbnNcclxuICAgIHJlbmRlckl0ZW06IChpdGVtLCBzZWFyY2hUZXJtKSA9PiB7XHJcbiAgICAgIHJldHVybiAgaXRlbS52YWx1ZTtcclxuICAgIH1cclxuICB9LFxyXG4gIHRvb2xiYXI6IFtdXHJcbiAgfVxyXG4gIH1cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICAvLyB0aGlzLnVzZXJbJ25hbWUnXSA9IHRoaXMudXNlclsnZnVsbF9uYW1lJ107XHJcbiAgfVxyXG5cclxuICBnZXROb3RlVHlwZSgpIHtcclxuICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmdldE5vdGVUeXBlKHsgY29uZmlnIDogdGhpcy5jb25maWcgLG5vdGVUeXBlIDogdGhpcy5ub3RlVHlwZSxjbGllbnRJZCA6IHRoaXMuY2xpZW50SWR9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBjb25zdCBjaGFyYWN0ZXJzID0gcmVzcG9uc2U/LmRhdGE/Lm5vdGVUeXBlQ29uZmlnPy5tZW50aW9uQ2hhcmFjdGVycztcclxuICAgICAgdGhpcy5tZW50aW9uRGVub3RhdGlvbiA9IGNoYXJhY3RlcnMuc3BsaXQoJywnKTtcclxuICAgICB9KTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZUNvbnRlbnQoKSB7XHJcbiAgICB0aGlzLndyaXRlVmFsdWUodGhpcy5jb21tZW50Q29udGVudCk7XHJcbiAgfVxyXG5cclxuICBkb0NvbW1lbnQoKSB7XHJcbiAgICBpZiAoIXRoaXMuY29tbWVudENvbnRlbnQudHJpbSgpKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHRoaXMuZm9ybUFjdGlvbiA9IHRydWU7XHJcblxyXG4gICAgaWYgKHRoaXMucm9vdE5vdGUpIHtcclxuICAgICAgdGhpcy5jb21tZW50c1NlcnZpY2UuYWRkQ29tbWVudCh7Y29uZmlnOiB0aGlzLmNvbmZpZyxjb21tZW50OiB0aGlzLmNvbW1lbnRDb250ZW50LCBub3RlVHlwZTogdGhpcy5ub3RlVHlwZSwgZW50aXR5SWQ6IHRoaXMuZW50aXR5SWQgLCByb290Tm90ZSA6IHt1dWlkOiB0aGlzLnJvb3ROb3RlLnV1aWR9LCBjbGllbnRJZDogdGhpcy5jbGllbnRJZCB9KVxyXG4gICAgICAgIC5waXBlKGZpbmFsaXplKCgpID0+IHRoaXMucmVzZXRTdGF0ZSgpKSlcclxuICAgICAgICAuc3Vic2NyaWJlKGFjY2Vzc09iaiA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjb21tZW50ID0gYWNjZXNzT2JqLmRhdGE7XHJcbiAgICAgICAgICB0aGlzLnNlbnQuZW1pdChjb21tZW50KTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmFkZENvbW1lbnQoe2NvbmZpZzogdGhpcy5jb25maWcsY29tbWVudDogdGhpcy5jb21tZW50Q29udGVudCwgbm90ZVR5cGU6IHRoaXMubm90ZVR5cGUsIGVudGl0eUlkOiB0aGlzLmVudGl0eUlkLCBjbGllbnRJZDogdGhpcy5jbGllbnRJZCB9KVxyXG4gICAgICAgIC5waXBlKGZpbmFsaXplKCgpID0+IHRoaXMucmVzZXRTdGF0ZSgpKSlcclxuICAgICAgICAuc3Vic2NyaWJlKGFjY2Vzc09iaiA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjb21tZW50ID0gYWNjZXNzT2JqLmRhdGE7XHJcbiAgICAgICAgICBjb21tZW50Wyd1c2VyJ10gPSB0aGlzLnVzZXI7XHJcbiAgICAgICAgICB0aGlzLnNlbnQuZW1pdChjb21tZW50KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNhbmNlbENvbW1lbnQoKSB7XHJcbiAgICB0aGlzLmNhbmNlbC5lbWl0KHRydWUpO1xyXG4gIH1cclxuXHJcbiAgcmVzZXRTdGF0ZSgpIHtcclxuICAgIHRoaXMuY29tbWVudENvbnRlbnQgPSBudWxsO1xyXG4gICAgdGhpcy5mb3JtQWN0aW9uID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBpbml0aXRhbGl6ZWQoZWRpdG9yKSB7XHJcbiAgICB0aGlzLmluaXQuZW1pdChlZGl0b3IpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHZhbHVlKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5jb21tZW50Q29udGVudDtcclxuICB9XHJcblxyXG4gIHNldCB2YWx1ZSh2OiBzdHJpbmcpIHtcclxuICAgIGlmICh2ICE9PSB0aGlzLmNvbW1lbnRDb250ZW50KSB7XHJcbiAgICAgIHRoaXMuY29tbWVudENvbnRlbnQgPSB2O1xyXG4gICAgICB0aGlzLm9uQ2hhbmdlKHYpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgd3JpdGVWYWx1ZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRDb250ZW50ID0gdmFsdWU7XHJcbiAgICB0aGlzLm9uQ29udGVudENoYW5nZS5lbWl0KHRoaXMuY29tbWVudENvbnRlbnQpO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVOb3RlKG5vdGU6IE5vdGUpIHtcclxuICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLnVwZGF0ZU5vdGUoe2NvbmZpZzogdGhpcy5jb25maWcsY29tbWVudDogdGhpcy5jb21tZW50Q29udGVudCwgY29tbWVudFVVSUQ6IG5vdGUudXVpZH0pLnN1YnNjcmliZShyZXNwb25zZSA9PntcclxuICAgICAgY29uc3QgY29tbWVudCA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgIG5vdGUudXBkYXRlTW9kZSA9ICFub3RlLnVwZGF0ZU1vZGU7XHJcbiAgICAgIHRoaXMuc2VudC5lbWl0KGNvbW1lbnQpO1xyXG4gICAgfSk7XHJcbiAgIH1cclxuXHJcbiAgIGVkaXROb3RlKG5vdGU6IE5vdGUpIHtcclxuICAgICAgbm90ZS51cGRhdGVNb2RlID0gIW5vdGUudXBkYXRlTW9kZTtcclxuICAgfVxyXG5cclxuICBvbkNoYW5nZSA9IChfKSA9PiB7fTtcclxuICBvblRvdWNoZWQgPSAoKSA9PiB7fTtcclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiAoXzogYW55KSA9PiB2b2lkKTogdm9pZCB7IHRoaXMub25DaGFuZ2UgPSBmbjsgfVxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuOiAoKSA9PiB2b2lkKTogdm9pZCB7IHRoaXMub25Ub3VjaGVkID0gZm47IH1cclxufVxyXG4iXX0=